package com.wwa.study.dataStrcture.recursion;

import org.springframework.util.StringUtils;

/**

 * @description 八皇后问题
 * @date Created in 2021/6/16
 * @modified By：
 */
public class Queue8 {
    //定义一个max 表示共有多少个皇后
    int max = 8;
    // 定义数组 array ,保存皇后放置位置的结果，比如 arr ={0,4,7,5,2,6,1,3} ,数组的下表索引为行，数组元素的值为列所在的位置
    int[] array = new int[max];
    static  int count = 0;
    static int judgeCount = 0;
    public static void main(String[] args) {
        Queue8 queue8 = new Queue8();
        queue8.check(0);
        System.out.printf("八皇后问题一共有 %d种解法;一种判断冲突%d次",count,judgeCount);  //92,15720
    }


    //编写一个方法，放置 第n 个皇后
    // 特别注意：check 是 每一次递归时，进入到 check 中都有 for(int i=0; i<max; i++), 因此 会有回溯
    private void check(int n ){
        if(n == max){   // n=8, 其实 8个皇后就已然放好了。
            print();
            return;
        }

        //依次 放入皇后，并判断是否冲突
        for (int i = 0; i < max; i++) {
            // 先把当前这个皇后n，放到 该行 第1列
            array[n] =i;
            // 判断当前放置 第n个皇后到i 列时，是否冲突
            if(judge(n)){  // 不冲突
                // 接着 放 n+1 个皇后，即开始递归
                check(n+1);

            }
            // 如果冲突，就继续执行array[n] =i; 即将第n个皇后，放置在本行的 后移的 一个位置
        }
    }

    /**
     *     //查看当我们放置第n个皇后，就去检测该皇后是否和前面已经摆放的皇后冲突。
     * @param n 表示第n个皇后
     * @return
     */
    private boolean judge(int n ){
        judgeCount++;
        for (int i = 0; i < n; i++) {
            //说明
            // 若 array[i] == array[n] 则意味着它俩在同一列,表示 判断第n个皇后是否和前面的n-1个皇后 在同一列
            //Math.abs(n-i) == Math.abs(array[n] - array[i]) 表示 判断第n个皇后是否和 第i个皇后 是否在同一斜线
            // n=1 {表示第2个皇后【0，1】} 放置在第二行，和对应的值array[1] 放置在第二列{0，1}
            //Math.abs(n-i) = Math.abs(1-0) ；Math.abs(array[n=1] - array[i=0])
            //3. 判断是否在同一行，没必要，因为n是行数每次都在递增
            if(array[i] == array[n] || Math.abs(n-i) == Math.abs(array[n] - array[i]) ){
                return false;
            }
        }
        return true;
    }

    //写一个方法，可以将皇后摆放的位置输出
    private void print(){
        for (int i = 0; i<array.length; i++){
            System.out.printf(array[i]+" ");
        }
         for (int i = 0; i<array.length; i++){
             System.out.printf("将第%d 个皇后 放置在 第%d行 第%d列的位置上;",i+1,i+1,array[i]+1);
        }
        System.out.println( "破解成功 ");
        count++;
    }
}
