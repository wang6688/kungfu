package com.wwa.study.dataStrcture.graph;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**

 * @date Created in 2021/7/1
  */
public class Graph {

    private List<String> vertexList; // 存储顶点集合
    private int[][] edges;  // 存储图对应的 邻接矩阵
    private int numOfEdges;  // 表示边的数目
    // 定义给数组 boolean[],记录某个节点是否被访问
    private boolean [] hasVisited;
    public  Graph(int nodeNum ){
        // 初始化矩阵 和vertexList
        edges = new int[nodeNum][nodeNum];
        vertexList = new ArrayList<>(nodeNum);
        numOfEdges = 0;
    }

    //插入节点
    public void insertVertex(String vertex){
        vertexList.add(vertex);
    }

    /** 添加边
     * @param v1 表示点的 下标 即第几个顶点 ”A“-”B“  ”A“->0 "B"->1
     * @param v2 第二个顶点对应的下标
     * @param weight 表示
     */
    public void insertEdge(int v1,int v2, int weight){
        edges[v1][v2]  = weight;
        edges[v2][v1] = weight;
        numOfEdges++;
    }

    /**
     * 图中常用的方法
     * @return 返回节点的个数
     */
    public int getNumOfVertex(){
        return vertexList.size();
    }

    /**
     * @return 得到边的数目
     */
    public int getNumOfEdges(){
        return numOfEdges;
    }

    /**
     * @param i 下标
     * @return 返回参数 节点i 对应的数据 0->"A"  1->"B"  2->"C"
     */
    public String getValueByIndex(int i){
        return vertexList.get(i);
    }

    /**
     * @param v1 节点
     * @param v2 节点
     * @return 返回v1 和v2 对应那条边的 权值
     */
    public int getWeight(int v1, int v2){
        return edges[v1][v2];
    }

    /**
     * 显示图对应 的矩阵
     */
    public void showGraph(){
        int idx = 0;
        System.out.println("   "+String.join(", ",vertexList));
        for (int[] link : edges) {
            System.out.println(vertexList.get(idx++) +":"+Arrays.toString(link));
        }
    }

    /** 得到第一个邻接节点的下标w
     * @param index
     * @return 如果存在就返回对应的下标，否则返回-1
     */
    public int getFirstNeighbor(int index){
        for(int j = 0; j<vertexList.size();j++){
            if(edges[index][j] >0){
                return j;
            }
        }
        return -1;
    }

    /** 根据前一个邻接节点的下标来获取下一个邻接节点
     * @param v1
     * @param v2
     * @return
     */
    public int getNextNeighbor(int v1,int v2){
        for (int j = v2+1; j<vertexList.size();j++){
            if(edges[v1][j] >0){
                return j;
            }
        }
        return -1;
    }

    /** 深度优先 遍历算法
     * 1）访问初始节点v，并标记节点v 为已访问。
     *  2）查找节点v的第一个邻接节点w。
     *  3）若w存在，则继续执行第4步，如果w不存在，则回到第1步，将从 v的下一个节点继续。
     *  4）若w未被访问，对w 进行深度优先遍历递归（即把w当作另一个v，然后进行步骤1-2-3）。
     *  5）查找节点v的w邻接节点的下一个邻接节点，转到步骤3.
     * @param hasVisited
     * @param i 第一次是0
     */
    private void dfs(boolean[] hasVisited,int i){
        // 首先我们访问该节点，输出
        System.out.print(getValueByIndex(i)+"->");
        // 将节点设置为 已经访问
        hasVisited[i] = true;
        // 查找节点i 的第一个邻接节点w
        int w = getFirstNeighbor(i);
        while(w != -1){  // 说明有
            if(!hasVisited[w]){
                dfs(hasVisited,w);
            }
            // 如果w节点已经被访问过
            w = getNextNeighbor(i,w);

        }
    }

    /**
     * 对 dfs 进行重载，遍历所有的节点，并进行dfs
     */
    public void dfs(){
        hasVisited = new boolean[vertexList.size()];

        // 遍历 所有的节点，进行dfs[回溯]
        for (int i = 0; i < getNumOfVertex(); i++) {
            if(!hasVisited[i]){
                dfs(hasVisited,i);
            }
        }
    }

    /**
     *  广度优先遍历算法步骤
     *  1）访问初始节点 v 并标记节点v为已访问。
     *  2）节点v 入队列。
     *  3）当队列非空时，继续执行，否则算法结束。
     *  4）出队列，取得队列头节点u。
     *  5）查找节点u的第一个邻接节点w。
     *  6） 若节点u的邻接节点w 不存在，则转到步骤3；否则循环执行以下三个步骤：
     *     6.1 若节点 w 尚未被访问，则访问节点w 并标记为已访问。
     *     6.2 节点w 入队列。
     *     6.3 查找节点u的 继w邻接节点后的下一个邻接节点w，转到步骤6.
     * 对一个节点进行 广度优先遍历的方法
     */
    private void broadFirstSearch(boolean [] hasVisited,int i) {
        int u; // 表示头节点对应下标
        int w; // 邻接节点w
        // 队列，记录节点访问的顺序
        LinkedList<Integer> queue = new LinkedList<Integer>();
        //访问节点，输出节点信息
        System.out.print(getValueByIndex(i) + "=>");
        // 标记已访问
        hasVisited[i] = true;
        // 将节点 加入队列
        queue.addLast(i);

        while (!queue.isEmpty()) {
            u = queue.removeFirst();
            // 得到第一个邻接节点的 下标w
            w = getFirstNeighbor(u);
            while (w != -1) {  //找到
                // 是否访问过
                if (!hasVisited[w]) {
                    System.out.print(getValueByIndex(w) + "=>");
                    // 标记已经访问
                    hasVisited[w] = true;
                    // 入队
                    queue.addLast(w);

                }
                // 以 u 为前驱点，找w 后面的下一个 邻接节点
                w = getNextNeighbor(u, w);   // 体现出了广度优先
            }
        }
    }
    public void bfs(){
        hasVisited = new boolean[vertexList.size()];
        for (int i = 0; i < getNumOfVertex(); i++) {
            if(!hasVisited[i]){
                broadFirstSearch(hasVisited,i);
            }
        }
    }

    public static void main(String[] args) {
//        study();
        example();
     }


    public static void study( ) {
        // 测试 图 是否创建ok
        int vertexNum = 5;  // 节点 的个数
        String []vertexes=  {"A","B","C","D","E"};
        // 创建图对象
        Graph graph = new Graph(vertexNum);
        //循环的添加顶点
        for (String vertex : vertexes) {
            graph.insertVertex(vertex);

        }
        //添加边  A-B A-C B-C B-D B-E
        graph.insertEdge(0,1,1);   //A-B
        graph.insertEdge(0,2,1);   //A-C
        graph.insertEdge(1,2,1);   //B-C
        graph.insertEdge(1,3,1);   //B-D
        graph.insertEdge(1,4,1);   // B-E

        // 显示 邻接矩阵
        graph.showGraph();

        // 测试 ，深度遍历是否ok
        System.out.println("深度优先遍历：");
        graph.dfs();   //A->B->C->D->E->
        System.out.println("广度优先遍历：");
        graph.bfs();   //A->B->C->D->E->
    }

    public static void example( ) {
        // 测试 图 是否创建ok
        String []vertexes=  {"1","2","3","4","5","6","7","8"};
        int vertexNum = vertexes.length;  // 节点 的个数

        // 创建图对象
        Graph graph = new Graph(vertexNum);
        //循环的添加顶点
        for (String vertex : vertexes) {
            graph.insertVertex(vertex);

        }
        //添加边  A-B A-C B-C B-D B-E
        graph.insertEdge(0,1,1);
        graph.insertEdge(0,2,1);
        graph.insertEdge(1,3,1);
         graph.insertEdge(1,4,1);
        graph.insertEdge(3,7,1);
        graph.insertEdge(4,7,1);
        graph.insertEdge(2,5,1);
        graph.insertEdge(2,6,1);
        graph.insertEdge(5,6,1);

        // 显示 邻接矩阵
        graph.showGraph();

        // 测试 ，深度遍历是否ok
        System.out.println("深度优先遍历：");
        graph.dfs();   //A->B->C->D->E->
        System.out.println("广度优先遍历：");
        graph.bfs();   //A->B->C->D->E->
    }
}
