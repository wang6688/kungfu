package com.wwa.study.dataStrcture.stack;

import sun.plugin2.util.SystemUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 *  逆波兰计算器，任务如下；
 * 1） 输入一个逆波兰表达式（后缀表达式），使用 栈，计算其结果
 * 2） 支持 小括号 和多位数 整数，因为这里我们主要讲的是数据结构，因此计算器进行简化，只支持对整数的计算。
 * 3） 思路分析
 * 4） 代码完成
  * @description  逆波兰表达式
 * @date Created in 2021/6/10
 * @modified By：
 */
public class PolandNotation {
    public static void main(String[] args) {
            //完成将一个 中缀表达式 转成后缀表达式的功能
            //说明
//       1. 中缀表达式 1+((2+3) X 4)-5 =》后缀表达式 ，将s2出栈 -5+*4+321 =》 1 2 3 + 4 * + 5 -
//        2. 因为直接 str 进行操作不方便，因此先将 "1+((2+3)X4)-5" =》中缀的表达式转成称 对应的List
        // 即 “1+((2+3) X 4)-5 ”=》 ArrayList [1,+,(,(,2,+,3,),*,4,),-,5]

        String expression = "1+((2+3)*4)-5";
        List<String> infixExpressionList = toInfixExpressionList(expression);
        System.out.println("中缀表达式对应的List = "+infixExpressionList);   //ArrayList [1,+,(,(,2,+,3,),*,4,),-,5]
        List<String> suffixExpressionList = parseSuffixExpressionList(infixExpressionList);
        System.out.println("后缀表达式对应的List="+suffixExpressionList);
        System.out.printf("expression=%d\n",calculate(suffixExpressionList));
        //定义给逆波兰表达式
        // (3 + 4 ） X 5 - 6 => 3 4 + 5 X 6 -  = 29
        // (30 + 4 ） X 5 - 6 => 30 4 + 5 X 6 -  = 29
        // 4 * 5 - 8 + 60 + 8 /2  => 4 5 * 8 - 60 + 8  2 / +  =76
        // 说明为了方便，逆 波兰表达式的数字和符号使用空格 隔开
        String  suffixExpression = "4 5 * 8 - 60 + 8 2 / +";
        //思路
        //1. 先讲 ”3 4 + 5 X 6 - “ =》 放到 ArrayList中
        //2. 将 ArrayList 传递给一个方法，遍历 ArrayList 配合栈完成计算
        List<String> rpnList = getListString(suffixExpression);
        System.out.println("rpnList="+rpnList);
        int res = calculate(rpnList);
        System.out.println("计算的结果是="+res);
    }

    /**
     * 将1个逆 波兰表达式，依次 将数据和 运算符 放入到 ArrayList 中
     * @param suffixExpression
     * @return
     */
    private static List<String> getListString(String suffixExpression) {
        // 将 suffixExpression 分割
        return  Arrays.asList(suffixExpression.split(" "));

    }


    /**
     *  完成对 逆 （后缀）波兰表达式的运算
     * 1） 从左至右扫描，将3 和4 压入堆栈；
     * 2） 遇到 + 运算符， 因此弹出4 和3 （4为栈顶元素，3为次顶元素），计算出 3+ 4 的值，得7，再将7 入栈；
     * 3） 将5 入栈；
     * 4） 接下来 是 X 运算符，因此弹出 5 和7，计算出 7 X 5 = 35，将35 入栈；
     * 5） 将6 入栈；
     * 6） 最后是- 运算符，计算出35-6的值，即29，由此得出最终结果。
      * @return
     */
    public static int calculate(List<String> ls){
        // 创建给栈，只需要一个栈即可
        Stack<String> stack = new Stack<String>();
        // 遍历 ls
        for(String item : ls){
            //这里使用 正则表达式 来取出数字
            if(item.matches("\\d+")){  //匹配的是多位数
                //入栈
                stack.push(item);
            }else {   //运算符
                // pop 出两个数，并运算， 再入栈
                int num2 = Integer.parseInt(stack.pop());
                int num1 = Integer.parseInt(stack.pop());
                int res = 0;
                switch (item){
                    case "+" : res = num1 + num2; break;
                    case "-" : res = num1 - num2; break;
                    case "*" : res = num1 * num2; break;
                    case "/" : res = num1 / num2; break;
                    default: throw new RuntimeException("运算符有误");
                }
                // 把运算符 入栈
                stack.push(res + "");
            }

        }
        //最后留在stack 中的数据就是运算结果
        return  Integer.parseInt(stack.pop());
    }

    /**
     * 将中缀表达式 转换成 以 数字和 符号为 单位的 集合
     * @param s
     * @return
     */
    public static List<String> toInfixExpressionList(String s ){
        //定义一个List，存放中缀表达式对应的内容。
        ArrayList<String> ls = new ArrayList<>();
        int i = 0; // 这似乎是一个指针，用于遍历中缀表达式字符串
        String str ; //对多位数的拼接
        char c ;// 每遍历一个字符，就放入到c
        do{
            // 如果 c 是一个非数字，我需要加入到 ls
            if((c=s.charAt(i)) < 48 || (c=s.charAt(i))>57){
                ls.add(""+c);
                i++; //i  需要后移
            }else{
                // 如果是一个数， 需要考虑多位数
                str = "";// 先将 str 设成""  '0'[48] ->'9'[57]
                while(i<s.length() && (c=s.charAt(i))>48 && (c=s.charAt(i))<=57){
                    str += c; // 拼接
                    i++;
                }
                ls.add(str);
            }
        }while (i < s.length());
        return ls;
    }


    /**
     * 即 ArrayList [1,+,(,(,2,+,3,),*,4,),-,5] =》ArrayList[1,2,3,+,4,*,+,5,-]
     *  方法： 将得到的中缀表达式 对应的List => 后缀表达式对应的List
     * @param ls
     * @return
     */
    public  static List<String> parseSuffixExpressionList(List<String> ls){
        //  定义 两个栈
        Stack<String> opStack = new Stack<String>(); // 符号栈
        //说明： 因为 phaseStack 这个栈，在整个转换过程中，没有pop操作，而且后面我们还需要逆序输出。
        // 因此比较麻烦，这里我们就不用 Stack<String> 直接使用 List<String> phaseStack;
        List<String > phaseStack = new ArrayList<>();// 存储中间结果的 phaseStack；
        // 遍历
        for(String item: ls){
            // 如果是 一个数，入栈
            if(item.matches("\\d+")){
                phaseStack.add(item);
            }else if(item.equals("(")){
                opStack.add(item);
            }else if(item.equals(")")){
                //  如果是右括号“）”，则依次弹出 opStack 栈顶的运算符，并压入 phaseStack，直到遇到左括号为止，此时将这一对括号匹配上就将其丢弃。
//            注：小括号 不为运算符，所以不存在与 运算符进行比较
                while(!"(".equals(opStack.peek())){
                    phaseStack.add(opStack.pop());
                }
                opStack.pop();// 消除一对小括号
            }else{
                // 当  item 的优先级 小于等于 opStack 栈顶运算符，将 opStack 栈顶 的运算符弹出并加入到 phaseStack  中，
                //再次转到4.1  与 OpStack 中新的 栈顶运算符相比较
                //  问题： 需要 一个比较优先级高低的方法
                while(!opStack.isEmpty()&& Operation.getLevel(item) <= Operation.getLevel(opStack.peek())){
                    phaseStack.add(opStack.pop());
                }
                // 还需要将item 压入栈中
                opStack.push(item);
            }

        }
        // 将 s1 中剩余的 运算符 依次弹出 并加入 phaseStack中
        while(!opStack.isEmpty()){
            phaseStack.add(opStack.pop());
        }
        return phaseStack;   //注意因为 是存放到list，因此按顺序输出 就是 对应的 后缀表达式（逆波兰式） 对应的list
    }

}

/**
 * 编写一个类 ，可以返回一个运算符 对应的优先级
 */
class Operation{

        private static  int ADD = 1 ;
        private static  int SUB =1;
    private static  int MUL =1;
    private static  int DIV =1;

    public static  int getLevel(String operation){
        int result = 0;
        switch (operation){
            case "+":  result = ADD;break;
            case "-":  result = SUB;break;
            case "*":  result = MUL;break;
            case "/":  result = DIV;break;
            default:
                System.out.println("不存在该运算符");
        }
        return  result;
    }
}
