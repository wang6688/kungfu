package com.wwa.study.dataStrcture.stack;

import com.sun.xml.internal.stream.StaxErrorReporter;
import sun.plugin2.util.SystemUtil;

import java.util.Scanner;

/**
 *栈的快速入门
 * 1）用数组模拟栈的使用，由于栈是一种有序列表，当然可以使用数组的结构来储存栈的数据内容，下面我们就用数组模拟栈的出栈，入栈等操作。
 * 2）实现思路分析，并画出示意图
 * 实现 栈的 思路分析
 * 1、 使用数组来模拟栈
 * 2、定义一个top 来标识栈顶， 初始化为-1；
 * 3、入栈的操作，当有数据加入到栈时，top++; stack[top]=data;
 * 4、出栈的操作，int value = stack[top]; top--; return value;
 *
 * 练习，用 单链表 实现 栈
  * @description
 * @date Created in 2021/6/8
 *
 */
public class ArrayStackDemo {
    public static void main(String[] args) {
        //创建 一个栈对象
        ArrayStack stack = new ArrayStack(4);
        String key = "";
        boolean loop = true; //控制是否 退出菜单
        Scanner scanner = new Scanner(System.in);
        while(loop){
            System.out.printf("1.show: 显示栈中的数据\t");
            System.out.printf("2.exit: 退出程序\t");
            System.out.printf("3.push:  添加数据到栈（入栈）\t");
            System.out.printf("4.pop: 从栈中取出数据（出战）\t");
            System.out.printf(" 请选择");
            key = scanner.next();
            switch (key){
                case "show":
                    stack.list();
                    break;
                case "push":
                    System.out.println("请输入一个数：");
                    int value = scanner.nextInt();
                    stack.push(value);
                    break;
                case "pop":
                    try{
                        int res = stack.pop();
                        System.out.printf("出战的数据是：%d\n",res);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case "exit":
                    scanner.close();
                    loop=false;
                    break;
                default:break;
            }
        }
        System.out.println("程序结束");
    }


}


//定义一个ArrayStack 表示栈

class ArrayStack{
    private int maxSize; // 栈的大小
    private int[] stack; // 数组，数组模拟栈，数据就放在该数组
    private int top = -1; // top 表示栈顶， 初始化为-1.

    // 构造器
    public ArrayStack(int maxSize){
        this.maxSize = maxSize;
        stack = new int[this.maxSize];
    }

    //栈满
    public boolean isFull(){
//        if(top== maxSize){
//            return true;
//        }
        return top>= maxSize-1;
    }

    //栈空
    public boolean isEmpty(){
        return  -1 == top;
    }

    //入栈-push
    public void push(int value){
        //先判断栈是否满
        if(isFull()){
            System.err.println("栈已满，无法入栈！");
            return;
        }
        stack[++top] = value;
    }
    //出战-pop,将栈顶的数据返回，同时top--
    public int pop(){
        //先判断 栈 是否空。
        if(isEmpty()){
            throw new RuntimeException("栈中无数据，不能执行出栈！");
        }
        return stack[top--];
    }
    //显示栈的情况[遍历栈],遍历时，需要从栈顶 开始

    public void list(){
        if(!isEmpty()){
            for(int i = top ;i>-1;i--){
                System.out.printf("stack[%d] = %d",i,stack[i]);
            }
         }else {
            System.out.println("栈中无数据！");
        }
    }
}