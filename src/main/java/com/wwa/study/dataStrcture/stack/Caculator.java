package com.wwa.study.dataStrcture.stack;

import org.springframework.util.NumberUtils;

import java.util.Scanner;

/**
 * 使用 栈完成计算一个表达式的结果
 * 7*2*2-5+1-5+3-4=?
 * 3+2*6-2 = ?
 * 思路：
 * 定义一个 操作数栈numStack（存放操作数） 和 一个操作符栈operStack（存放运算发）
 * 1.通过一个 index值（索引）来遍历 我们的表达式
 * 2.如果我们发现 index 指向的值是一个数字，就直接放入操作数栈
 * 3.如果发现扫描到是一个符号，就分如下情况。
 * 3.1 如果发现当前的符号栈为空，就直接入栈
 * 3.2 如果符号栈有操作符，就进行比较**，如果当前的操作符的优先级小于或者等于栈中的操作符**，就需要从数栈中pop出两个数，再从符号栈中pop出一个符号，进行运算，将得到结果，放入操作数栈，
 * 然后将当前的操作符 入符号栈，如果当前的操作符的优先级大于栈中的操作符，就直接入符号栈。
 * 4.当表达式扫描完毕，就顺序的从 操作数栈 和符号栈中 pop出相应的数和符号，并运行。
 * 5. 最后再数栈中只有一个数字，就是表达式的结果。
 *
 * @description  中缀表达式 计算器
 * @date Created in 2021/6/8
 */
public class Caculator {
    public static void main(String[] args) {
        //3+2*6-2
        //7*2*2-5+1-5+3-4
        //70+2*6-4
        String expression = "7*2*2-5+1-5+3-4";
        //创建两个栈 ,（操作数栈 和 操作符栈）
        ArrayStack2 numStack = new ArrayStack2(10);
        ArrayStack2 operStack = new ArrayStack2(10);
        // 定义需要的相关变量
         int num1 = 0;
        int num2 = 0;
        int oper = 0;
        int result = 0;
        char[] expCharAry = expression.toCharArray();
        for (int i = 0; i <expCharAry.length; i++) {
            char ch = expCharAry[i];
            if (operStack.isOper(ch)) {  //如果是运算符
                if (operStack.isEmpty()) {
                    //操作符栈 为空直接入栈
                    operStack.push(ch);

                } else {
                    //如果符号栈 有操作符，就进行比较，如果当前的操作符 的优先级 小于或者等于 栈中的操作符，就需要从数栈中pop 出两个数，
                    //在从符号栈中pop出一个符号，进行运算，将得到结果，入数栈，然后将当前的操作符入符号栈
                    if (operStack.priority(ch) <= operStack.priority(operStack.peak())) {
                        num1 = numStack.pop();
                        num2 = numStack.pop();
                        oper = operStack.pop();
                        result = operStack.calc(num1, num2, (char) oper);
                        numStack.push(result);
                        operStack.push(ch);
                    } else {
                        //如果 当前的操作符的优先级大于栈中的操作符，就直接入符号栈
                        operStack.push(ch);

                    }
                }
            }else{
                // 如果是 操作数，则直接入数栈
//                numStack.push( ch-48); //ascii 码字符序值
                // 当处理 多位数时，不能发现是一个数字就 立即入栈，因为他可能是多位数
                //在处理数时，需要向expression的表达式的后面再多看一位，如果是数就继续多看，直到看到的是符号的时候才入数字栈
                //因此需要定义一个字符串变量，用于拼接
                String keepNum = "";
                int tempIdx = i;
                while(tempIdx<expCharAry.length && !operStack.isOper(expCharAry[tempIdx])){
                    keepNum += expCharAry[tempIdx++];

                }
                i=i+(tempIdx-i-1);
                numStack.push( Integer.parseInt(keepNum)); //ascii 码字符序值

            }
        }
        //当表达式 扫描完毕，就顺序的从数栈 和符号栈中pop出相应的数和符号，并运行
        //如果符号栈为 空，则计算到最后的结果， 数栈中只有一个数字结果
        while(!operStack.isEmpty()){
            num1 = numStack.pop();
            num2 = numStack.pop();
            oper = operStack.pop();
            result = operStack.calc(num1, num2, (char) oper);
            numStack.push(result);
        }
        System.out.printf("表达式%s = %d",expression,result);
    }


}


//定义一个ArrayStack2 表示栈,相对于 ArrayStack 需要扩展功能

class ArrayStack2 {
    private int maxSize; // 栈的大小
    private int[] stack; // 数组，数组模拟栈，数据就放在该数组
    private int top = -1; // top 表示栈顶， 初始化为-1.

    // 构造器
    public ArrayStack2(int maxSize) {
        this.maxSize = maxSize;
        stack = new int[this.maxSize];
    }

    //栈满
    public boolean isFull() {
//        if(top== maxSize){
//            return true;
//        }
        return top >= maxSize - 1;
    }

    //栈空
    public boolean isEmpty() {
        return -1 == top;
    }

    //偷看 栈顶的值
    public int peak() {
        return stack[top];
    }

    //入栈-push
    public void push(int value) {
        //先判断栈是否满
        if (isFull()) {
            System.err.println("栈已满，无法入栈！");
            return;
        }
        stack[++top] = value;
    }

    //出战-pop,将栈顶的数据返回，同时top--
    public int pop() {
        //先判断 栈 是否空。
        if (isEmpty()) {
            throw new RuntimeException("栈中无数据，不能执行出栈！");
        }
        return stack[top--];
    }
    //显示栈的情况[遍历栈],遍历时，需要从栈顶 开始

    public void list() {
        if (!isEmpty()) {
            for (int i = top; i > -1; i--) {
                System.out.printf("stack[%d] = %d", i, stack[i]);
            }
        } else {
            System.out.println("栈中无数据！");
        }
    }

    //返回运算符的优先级，优先级是程序员来确定，优先级使用数字表示。
    //数字越大 ，则优先级就越高。
    public int priority(int oper) {
        int level = -1;
        switch (oper) {
            case '*':
            case '/':
                level = 1;
                break;
            case '+':
            case '-':
                level = 0;
                break;

        }
        return level;
    }

    //判断 是不是一个运算符。
    public boolean isOper(char val) {
        boolean isOper = false;
        switch (val) {
            case '+':
            case '-':
            case '*':
            case '/':
                isOper = true;
                break;
        }
        return isOper;
    }

    //计算方法
    public int calc(int num1, int num2, char oper) {
        int result = 0;
        switch (oper) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num2 - num1;
                break; //注意顺序
            case '*':
                result = num2 * num1;
                break;
            case '/':
                result = num2 / num1;
                break; //注意顺序

        }
        return result;
    }
}