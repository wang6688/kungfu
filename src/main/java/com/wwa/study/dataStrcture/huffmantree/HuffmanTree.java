package com.wwa.study.dataStrcture.huffmantree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @date Created in 2021/6/23
 */
public class HuffmanTree {

    public static void main(String[] args) {
        int arr[] = {13, 7, 8, 3, 29, 6, 1};
        Node huffmanRoot = createHuffmanTree(arr);

        preOrder(huffmanRoot);
    }

    // 编写前序遍历的方法
    public static void preOrder(Node root) {
        if(root!=null){
            root.preOrder();
        }else{
            System.out.println(" 是空树，不能遍历");
        }
    }

    /**
     * 创建赫夫曼树的方法。
     * @param arr 需要创建哈夫曼树的数组
     * @return 创建好的赫夫曼树的root节点
     */
    public static Node createHuffmanTree(int[] arr) {

        //第一步为了操作方便
        //1. 遍历arr数组
        //2. 将arr 的每个元素构建成一个Node
        //3. 将Node 放入到ArrayList 中
        List<Node> nodes = new ArrayList<Node>();
        for (int value : arr) {
            nodes.add(new Node(value));
        }

        //  处理的过程是一个循环的过程
        while (nodes.size() > 1) {
            // 排序从小到到，使用Collections.sort()方法，因为Node接口实现了Comparable 接口所以可以调用该方法进行排序
            Collections.sort(nodes);
            System.out.println("nodes =" + nodes);

            // 取出根节点权值最小的两颗二叉树
            //(1) 取出权值 最小的节点（二叉树）
            Node leftNode = nodes.get(0);
            //(2) 取出 权值 第二小的节点（二叉树）
            Node rightNode = nodes.get(1);
            //(3)构建一颗新的二叉树
            Node parent = new Node(leftNode.value + rightNode.value);
            parent.left = leftNode;
            parent.right = rightNode;

            // (4) 从ArrayList 删除 处理过的二叉树
            nodes.remove(leftNode);
            nodes.remove(rightNode);
            // (5) 将parent 加入到 Nodes
            nodes.add(parent);

//            System.out.println("第一次处理后:"+nodes);
        }
        // 返回哈夫曼树的root节点
        return nodes.get(0);
    }


}

//节点类
// 为了 让Node 对象持续排序 Collections集合排序 ，让Node 实现 Comparable  接口
class Node implements Comparable<Node> {
    int value; // 节点权值
    char c; // 字符
    Node left; // 指向左子节点
    Node right;  //指向右子节点

    //前序遍历
    public void preOrder(){
        System.out.println(this);
        if(this.left!=null){
            this.left.preOrder();
        }
        if (this.right != null) {

            this.right.preOrder();
        }
    }

    public Node(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node [value = " + value + "]";
    }

    @Override
    public int compareTo(Node o) {
        // 表示从小到大排序
        return this.value - o.value;
//        return -(this.value-o.value);  从大到小排序

    }
}
