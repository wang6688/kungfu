package com.wwa.study.dataStrcture.huffmantree;


import ch.qos.logback.classic.Logger;

import java.io.*;
import java.util.*;

/**

  * @date Created in 2021/6/24
  */
public class HuffmanCode {
    public static void main(String[] args) {

        testProcessString();
        // 注： 如果两个方法同时执行的话，后面的解压文件时 会有问题，造成解压后的文件无法打开，所以要分两个方法进行测试
//        testProcessFile();
    }

    public static void testProcessString( ) {
                String content = "i like like like java do you like a java";
        byte[] contentBytes = content.getBytes();
        System.out.println("原字符串的长度是："+contentBytes.length); //40
      /*  //分步过程
        List<NodeForHFCode> nodes = getNodes(contentBytes);
        System.out.println("nodes="+nodes);

        // 测试一把，创建的二叉树
        System.out.println("赫夫曼树:");
        NodeForHFCode huffmanTreeRoot = createHuffmanTree(nodes);
        System.out.println("前序遍历");
        huffmanTreeRoot.preOrder();
        // 测试是否生成了对应的哈夫曼编码
        Map<Byte, String> huffmanCodes = getCodes(huffmanTreeRoot);
        System.out.println("~生成的哈夫曼编码表："+huffmanCodes);

        //测试
        byte[] huffmanCodeBytes = zip(contentBytes, huffmanCodes);
        System.out.println("huffmanCodeBytes:"+Arrays.toString(huffmanCodeBytes));  //17
*/
        byte[] huffmanCodesBytes = huffmanZip(contentBytes);
        System.out.println("压缩后的结果是:"+Arrays.toString(huffmanCodesBytes)+"长度是:"+huffmanCodesBytes.length);
//        System.out.println(new String(huffmanCodesBytes));
        // 发送 huffmanCodeBytes 数组

        //测试byteToBitString 方法
//        byteToBitString(false,(byte)1);
        byte[] sourceChars = decode(huffmanCodes, huffmanCodesBytes);
        System.out.println("原来的字符串="+new String(sourceChars));
    }
    public static void testProcessFile() {
        String srcFile = "D:\\test\\sun.bmp";
        String destFile = "d:\\test\\sum.zip";
        zipFile(srcFile,destFile);
        System.out.println("压缩文件ok");
        String zipFile = "d:\\test\\sum.zip";
        String afterUnzipFile = "D:\\test\\unzip_sun.bmp";
        unZipFile(zipFile,afterUnzipFile);
        System.out.println("解压文件ok");
    }
    /**
     * @param bytes 原始的字符串对应的 字节数组
     * @return 是经过 赫夫曼编码处理后的字节数组（压缩后的数组）
     */
    //使用一个方法，将前面的方法封装起来，便于我们调用
    private  static byte[] huffmanZip(byte[] bytes){
        List<NodeForHFCode> nodes = getNodes(bytes);
        // 根据nodes 创建huffman树
        NodeForHFCode huffmanTreeRoot = createHuffmanTree(nodes);
        // 对应 的赫夫曼编码(根据赫夫曼树)
        Map<Byte,String> huffmanCodes = getCodes(huffmanTreeRoot);
        //根据生成的赫夫曼编码，压缩得到压缩后的赫夫曼编码字节数组
        byte[] huffmanCodeBytes = zip(bytes, huffmanCodes);
        return huffmanCodeBytes;
    }

    private static byte[] decode(Map<Byte,String> huffmanCodes,byte[] huffmanBytes){
        //1. 先得到huffmanBytes 对应的二进制的字符串，形式101010010111...
        StringBuilder stringBuilder = new StringBuilder();
        // 将byte数组转成 二进制的字符串
        for (int i = 0; i < huffmanBytes.length; i++) {
            byte huffmanByte = huffmanBytes[i];
            // 判断是不是最后一个字节
            boolean flag = (i== huffmanBytes.length-1);
            stringBuilder.append(byteToBitString(!flag,huffmanByte));

        }
//        System.out.println("赫夫曼字节数组对应的二进制字符串="+stringBuilder.toString());
        // 把字符串 按照指定的赫夫曼编码 进行解码
        // 把赫夫曼编码表进行调换，因为要进行反向查询97->100 100->'a'
        HashMap<String, Byte> map = new HashMap<>();
        for (Map.Entry<Byte, String> entry : huffmanCodes.entrySet()) {
            map.put(entry.getValue(),entry.getKey());
        }
//        System.out.println("map="+map);
        // 创建集合 存放byte
        List<Byte> list = new ArrayList<>();
        //i 可以理解成就是索引扫描 stringBuilder
        for (int i = 0; i < stringBuilder.length();) {
            int count =1 ; // 小的计数器
            boolean flag = true;
            Byte b = null;
            while(flag){
                // 1010100010111...
                // 递增 的取出key 1
                String key = stringBuilder.substring(i,i+count);// i 不动，让count 移动，指定匹配到一个字符。
                b = map.get(key);
                if(null==b){ // 说明没有匹配到对应字符
                    count++;
                }else{
                    // 匹配到
                    flag = false;
                }
            }
            list.add(b);
            i += count;
        }
        // 当for 循环结束后 ，list中就存放了所有的字符 ”i like like like java do you like a java“
        // 把list 中的数据放入到 byte[] 并返回
        byte[] charBytes = new byte[list.size()];
        for (int i = 0; i < list.size(); i++) {
            charBytes[i] = list.get(i);
        }
        return charBytes;
     }




    /**
     * 将一个byte 转成一个二进制的字符串
     * @param b 传入的byte
     * * @param flag 标志 是否需要补高位，如果是true表示需要补高位，如果false则不用 ,如果是最后一个字节（可能不足8位）则无需补高位
     * @return 是该b 对应的二进制的字符串，（注意是按补码返回）
     */
    //完成数据的解压
    //思路：
    // 1. 将 huffmanCodeBytes[-88, -65, -56, -65, -56, -65, -55, 77, -57, 6, -24, -14, -117, -4, -60, -90, 28]
    //重写先转成赫夫曼编码对应的二进制字符串10101000101111111100100010111111110010001..."
    //2. 将赫夫曼编码对应的二进制字符串10101000101111111100100010111111110010001... 对照赫夫曼编码=>"i like like like java do you like a java"
    private static String byteToBitString(boolean flag,byte b){
        // 使用变量保存b
        int temp = b; // 将b 转成int
        // 如果参数b 是正数的话，我们还需要在高位进行补充
        if(flag){
            temp |=256;  // 按位或256=1 0000 0000 | 0000 0001 =》1 0000 0001
        }
        String str = Integer.toBinaryString(temp);// 返回的是 temp对应的二进制的补码
//        System.out.println("str="+str);
        if(flag){
            return str.substring(str.length()-8);
        }else {
            return str;
        }
    }
    /**
     * @param bytes 原始的字符串对应的byte[]
     * @param huffmanCodes 生成的赫夫曼编码map
     * @return 返回赫夫曼编码处理后的byte[]
     * example:         String content = "i like like like java do you like a java"; =>  byte[] contentBytes = content.getBytes();
     * 返回的是字符串“1010100010111111110010001011111111001000101111111100100101001101110001110000011011101000111100101000101111111100110001001010011011100"=>
     * 对应的 byte[] huffmanCodeBytes, 即8位对应一个byte，放到huffmanCodeBytes.
     * huffmanCodeBytes[0] = 10101000(补码)=>byte [推导 10101000=>10101000 -1 => 10100111(反码)=>11011000 = -88]
     * huffmanCodeBytes[1] = -88
     */
    //编写一个方法，将字符串对应的byte[]数组，通过生成的赫夫曼编码表，返回一个赫夫曼编码 压缩后的 byte[]
    private static byte[] zip(byte[] bytes,Map<Byte,String> huffmanCodes){
        //1. 利用 huffmanCodes 将 bytes 转称 赫夫曼编码对应的字符串。
        StringBuilder stringBuilder =new StringBuilder();
        // 遍历bytes 数组
        for (byte b : bytes) {
            stringBuilder.append(huffmanCodes.get(b));
        }
        System.out.println("测试 stringbuilder~~~="+stringBuilder.toString());
        // 将"101010001011111... ”转换称byte[]
        // 统计返回byte[] huffmanCodeBytes 长度
        int len;
        // 一行代码 int len = (stringBuilder.length()+7)/8; 等同于下面的if-else
        if(stringBuilder.length()%8==0){
            len = stringBuilder.length()/8;
        }else{
            len = stringBuilder.length()/8+1;
        }
        // 创建 存储压缩后的byte 数组
        byte[] huffmanCodeBytes = new byte[len];
       int index = 0;// 记录是第几个byte
        for (int i =0;i<stringBuilder.length();i+=8){  // 因为是每8位对应一个byte，所以步长+8
            String strByte;
            if(i +8 > stringBuilder.length()){
                    strByte = stringBuilder.substring(i);
            }else{
                strByte = stringBuilder.substring(i,i+8);
            }
            // 将strByte 转成一个byte，放入到 huffmanCodeBytes
            huffmanCodeBytes[index]  = (byte)Integer.parseInt(strByte,2);
            index ++;
        }
        return huffmanCodeBytes;
    }

    /**该方法对由霍夫曼编码进行压缩的文件进行解压。
     * @param zipFile 待解压的文件路径
     * @param destFile 解压文件后输出的路径
     */
    public static  void unZipFile(String zipFile,String destFile){
         // 定义文件输入流
        InputStream is = null;
        // 定义一个对象输入流
        ObjectInputStream objectInputStream = null;
        // 定义文件的输出流
        OutputStream os = null;
        try {
            is = new FileInputStream(zipFile);
            // 创建一个和 is关联的对象输入流
              objectInputStream = new ObjectInputStream(is);
              // 读取 byte 数组 huffmanBytes
            byte[] huffmanBytes = (byte[]) objectInputStream.readObject();
            // 读取赫夫曼编码表
            Map<Byte, String> huffmanCodes = (Map<Byte, String>) objectInputStream.readObject();
            //解码
            byte[] bytes = decode(huffmanCodes, huffmanBytes);
            // 将bytes 数组写入到目标文件
           os = new FileOutputStream(destFile);
           // 写入数据到destFile 文件
            os.write(bytes);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            try {
                is.close();
                objectInputStream.close();
                os.close();
            }catch (Exception e2){
                System.out.println(e2.getMessage());
            }
        }
    }

    /** 该方法对一个文件进行压缩
     * @param srcFile 待压缩文件的全路径
     * @param destFile 压缩后输出的文件路径
     */
    public static void zipFile(String srcFile,String destFile){
        // 创建输出流
        // 创建文件的输入流。
        FileInputStream is = null;
        OutputStream os = null;
        ObjectOutputStream objectOutputStream=null;
        try{
              is = new FileInputStream(srcFile);
              // 创建一个何 源文件大小一样的byte[]
            byte[] bytes = new byte[is.available()];
            // 读取文件
            is.read(bytes);
            // 直接对源文件压缩
            byte[] huffmanBytes = huffmanZip(bytes);
            // 创建文件的输出流，存放压缩文件
              os = new FileOutputStream(destFile);
              //创建一个和我呢见输出流关联的ObjectOutputStream
              objectOutputStream = new ObjectOutputStream(os);
              // 把赫夫曼编码后的字节输入写入压缩文件
            objectOutputStream.writeObject(huffmanBytes);
            // 这里以对象流的方式写入 赫夫曼编码，是为了以后我们恢复源文件时使用
            // 一定要把赫夫曼编码 写入压缩文件，不然以后就恢复不了了 。
            objectOutputStream.writeObject(huffmanCodes);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            try {
                is.close();
                objectOutputStream.close();
                os.close();
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }

    // 生成赫夫曼树对应的赫夫曼编码表
    // 思路：
    // 1. 将赫夫曼编码表 存放在Map<Byte,String>形式
    static Map<Byte,String> huffmanCodes =new HashMap<>();
    // 32=' '->01 97='a'->100 100->11000 等等【形式】
    //2. 在生成 赫夫曼编码表时，需要去拼接路径，定义StringBuilder 存储某个叶子节点的路径
    static StringBuilder strBuilder = new StringBuilder();

    // 重载getCodes
    private static  Map<Byte,String> getCodes(NodeForHFCode root){
        if(root == null){
            return null;
        }
        //处理root的左子树
        getCodes(root.left,"0",strBuilder);
        //处理 root的右子树
        getCodes(root.right,"1",strBuilder);
        return huffmanCodes;
    }
    /**
     * 功能： 将传入的node节点的所有叶子节点的赫夫曼编码得到，并放入到 huffmanCodes集合。
     * @param node 传入节点
     * @param code 路径：左子节点 是0，右子节点是1
     * @param path 用于拼接路径
     */
    private static void getCodes(NodeForHFCode node, String code, StringBuilder path){
        StringBuilder pathBuilder = new StringBuilder(path);
        // 将code 加入pathBuilder
        pathBuilder.append(code);
        if(node !=null){  // 如果node ==null  不处理
            // 判断当前node 是叶子节点还是非叶子节点。
            if(node.date == null){ // 非叶子节点
                // 递归处理
                // 向左递归
                getCodes(node.left,"0",pathBuilder);
                // 向右递归
                getCodes(node.right,"1",pathBuilder);
            }else{  // 说明是一个叶子节点
                // 就表示 找到某个叶子节点的最后
                huffmanCodes.put(node.date,pathBuilder.toString());
            }

        }
    }

    // 前序遍历的方法
    public   void preOrder (NodeForHFCode root) {
        if(root != null){
            root.preOrder();
        }else{
            System.out.println("赫夫曼树为空");

        }
    }
    /**
     * @param bytes 接收 字节数组
     * @return 返回的就是List 形式[Node[date=97,weight=5],Node[data=32,weight=9]...],
     */
    private static List<NodeForHFCode> getNodes(byte[] bytes){
        //1 .创建一个ArrayList
        ArrayList<NodeForHFCode> nodes = new ArrayList<>();
        // 遍历bytes，统计 每一个byte 出现的次数 ->map[key,value]
        Map<Byte,Integer> counts = new HashMap<>();
        for (byte b : bytes) {
            Integer count = counts.get(b);
            if(count == null){  // Map 还没有这个字符数据，第1次。
                counts.put(b,1);
            }else{
                counts.put(b,count+1);
            }
        }

        // 把每一个键值对换称 一个Node对象并加入到 Nodes 集合。
        // 遍历map
        for (Map.Entry<Byte, Integer> entry : counts.entrySet()) {
            nodes.add(new NodeForHFCode(entry.getKey(),entry.getValue()));
        }
        return nodes;
    }

    // 可以通过List 创建 的赫夫曼树
    private static NodeForHFCode createHuffmanTree(List<NodeForHFCode> nodes){
        while(nodes.size()>1){
            // 排序，从小到大
            Collections.sort(nodes);
            // 取出第一颗最小的二叉树
            NodeForHFCode leftNode = nodes.get(0);
            // 取出 第二课最小的二叉树
            NodeForHFCode rightNode = nodes.get(1);
            // 创建一颗新的二叉树，它的根节点没有data，只有权值
            NodeForHFCode parent = new NodeForHFCode(null,leftNode.weight+rightNode.weight);
            parent.left = leftNode;
            parent.right = rightNode;

            // 将已经处理的两棵二叉树从nodes 删除
            nodes.remove(leftNode);
            nodes.remove(rightNode);
            // 将新的二叉树 ，加入到nodes
            nodes.add(parent);

        }
        // nodes 最后的节点，就是 赫夫曼树的根节点
        return nodes.get(0);
        }
}


// 创建Node，待数据和权值
class  NodeForHFCode implements Comparable<NodeForHFCode> {
    Byte date; // 存放数据字符本身，比如'a'=>97 ' '=>32
    int weight; // 权值，表示字符出现的次数。
    NodeForHFCode left; //
    NodeForHFCode right; //

    public NodeForHFCode(Byte date, int weight) {
        this.date = date;
        this.weight = weight;
    }

    @Override
    public int compareTo(NodeForHFCode o) {
        return this.weight-o.weight;
    }

    @Override
    public String toString() {
        return "Node{" +
                "date=" + date +
                ", weight=" + weight +
                '}';
    }

    //前序遍历
    public void preOrder(){
        System.out.println(this);
        if(null!=left){
            this.left.preOrder();
        }
        if(null!=right){
            this.right.preOrder();
        }
    }


}
