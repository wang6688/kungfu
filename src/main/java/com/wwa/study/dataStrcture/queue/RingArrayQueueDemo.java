package com.wwa.study.dataStrcture.queue;


import java.util.Scanner;

/**
 *  数组模拟 环形队列
 * @author clark
 * @date 2021/3/18
 * @desc   思路如下：
 * 1. front 变量的含义做一个调整：front就指向队列的第一个元素，也就是说arr[front] 就是队列的第一个元素front的初始值=0
 * 2. rear 变量的含义做一个调整： rear指向队列的最后一个元素的后一个位置，因为希望空出一个空间做为约定!!!!， rear的初始值=0
 * 3.当队列满时，条件是(rear+1)%maxSize == front【满】
 * 4.队列为空的条件是 ,rear==front 【空】
 * 5.当我们这样分析，队列中有效的数据的个数 [rear+maxSize-front)%maxSize   //rear=1 ,front =0 ps:一般来说只要在队列的设计中使用到数据的循环引用，一般都是用此算法
 * @return
 */
public class RingArrayQueueDemo {
    //
    public static void main(String[] args) {
        //空出一个空间做为约定!!!!!
        int arrange = 1;
        RingArrayQueue arrayQueue = new RingArrayQueue(3+arrange);
        char key = ' ';// 接收用户输入
        Scanner scanner = new Scanner(System.in);
        boolean loop = true;
        //输出一个菜单
        while (loop){
            System.out.println("s(show): 显示队列");
            System.out.println("e(exit): 退出程序");
            System.out.println("a(add) : 添加数据到队列");
            System.out.println("g(get): 从队列取出数据");
            System.out.println("h(head):  查看队列头的数据");
            key = scanner.next().charAt(0);// 接收一个字符
            switch (key){
                case  's' :
                    arrayQueue.showQueue();
                    break;
                case 'a':
                    System.out.println("输出一个数");
                    arrayQueue.addQueue(scanner.nextInt());
                    break;
                case 'g': //取出数据
                    try {
                        int res = arrayQueue.getQueue();
                        System.out.printf("取出的数据是 %d\n",res);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'h'://查看队列头的数据
                    try {
                        int res = arrayQueue.headQueue();
                        System.out.printf("队列头的数据是%d\n",res);
                    }catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'e':// 退出
                    scanner.close();
                    break;
                default:break;
            }
        }
        System.out.println("程序退出");
    }
}


// 使用数组模拟队列-编写一个ArrayQueue类
class RingArrayQueue {
    /*** 表示数组的最大容量*/
    private int maxSize;
    /*** 队列头*/
    private int front;

    /**
     * 队列尾
     */
    private int rear;
    //该数据用于存放数据，模拟队列
    private int[] arr;

    /**
     * 创建队列的构造器
     */
    public RingArrayQueue(int arrMaxSize) {
        maxSize = arrMaxSize;
        arr = new int[maxSize];
        // 指向队列头部，分析出front是指向队列头的一个位置
        front = 0;
        //指向队列尾部，指向队列尾的数据(最后一个元素的后一个位置)
        rear = 0;
    }

    //判断队列是否满
    public boolean isFull() {
        return front == (rear+1)%maxSize;
    }
    //判断队列是否为空
    public  boolean isEmpty(){
        return rear==front;
    }
    //添加数据到队列
    public void addQueue(int num){
        //判断队列是否满
        if(isFull()){
            System.err.println("队列已满不能加入数据");
            return;
        }
        //直接将数据加入
        arr[rear] = num;
        //将rear后移，这里必须考虑取模
        rear = (rear+1)%maxSize;
    }
    //获取队列的数据，让数据出队
    public int getQueue(){
        // 判断是否为空
        if(isEmpty()){
            //通过抛出异常处理
            throw new RuntimeException("队列里无数据，不能获取");
        }
        //front后移
        //  这里需要分析除front是指向队列的第一个元素
        //1. 先把front对应的值保存到一个临时变量
        int value =arr[front];
        //2. 将front 后移，考虑取模,防止出现数组下标越界的问题
        front = (front+1) % maxSize;
        //3.将临时保存的变量返回。
        return value;
    }
    //显示队列的所有数据
    public void showQueue(){
        //遍历
        if(isEmpty()){
            System.out.println("队列为空，没有数据");
            return;
        }
        //思路： 从front开始遍历，遍历多少个元素（队列中有效值的个数）
        for (int i = front; i < front+getValidSize(); i++) {
            System.out.printf("arr[%d]=%d\n",i%maxSize,arr[i%maxSize]);
        }
    }
    //显示队列的头数据，注意不是取出数据 peek
    public int headQueue(){
        //判断
        if(isEmpty()){
             throw new RuntimeException("队列为空，没有数据!~~~");
        }
        return arr[front];
    }
    //求出当前队列有效数据的个数
    public int getValidSize(){
        return (rear+maxSize-front)%maxSize;
    }
}
