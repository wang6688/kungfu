package com.wwa.study.dataStrcture.queue;

/**
 * @param
 * @author clark
 * @date 2021/3/17
 * @desc 模拟队列
 * @return
 */
public class SimulationQueue {

    /**
     * 队列是一个有序列表，可以用数组或是链表来实现。
     * 遵循先入先出的原则。即：先存入队列的数据，要先取出，后存入的要后取出

     数组模拟队列：
     队列本身是有序列表，若使用数组的结构来存储队列的数据，则队列数组的声明入下图，其中maxSize是该队列的最大容量
     因为队列的输出、输入时分别从前后端来处理，因此需要两个变量front及rear分别记录队列前后端的下标，front会随着数据输出而改变，而rear则是随着数据输入而改变，
     当我们将数据存入队列时成为“addQueue”，addQueue的处理需要有两个步骤：思路分析
     1） 将尾指针往后移：rear+1, 当front == rear【空】
     2) 若尾指针rear小于队列最大下标maxSize-1, 则将数据存入rear所指的数组元素中，否则无法存入数据。rear==maxSize01【队列满】
     *
     使用场景： 银行有4个窗口办理业务，若客户人数较少时则 4个窗口叫号同时 办理即可。若客户人数大于4 时则 客户 先坐在等待区（队列），等到叫号。
     * */
}
