package com.wwa.study.dataStrcture.queue;


import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;
import sun.plugin2.util.SystemUtil;

import java.util.Queue;
import java.util.Scanner;

/**
 *  纯属组模拟 队列
 * @author clark
 * @date 2021/3/18
 * @desc  存在问题，队列只能用一次， 当队列满并清空后 ，仍然还不能再放入数据
 * @return
 */
public class ArrayQueueDemo {
    //
    public static void main(String[] args) {
        ArrayQueue arrayQueue = new ArrayQueue(3);
        char key = ' ';// 接收用户输入
        Scanner scanner = new Scanner(System.in);
        boolean loop = true;
        //输出一个菜单
        while (loop){
            System.out.println("s(show): 显示队列");
            System.out.println("e(exit): 退出程序");
            System.out.println("a(add) : 添加数据到队列");
            System.out.println("g(get): 从队列取出数据");
            System.out.println("h(head):  查看队列头的数据");
            key = scanner.next().charAt(0);// 接收一个字符
            switch (key){
                case  's' :
                    arrayQueue.showQueue();
                    break;
                case 'a':
                    System.out.println("输出一个数");
                    arrayQueue.addQueue(scanner.nextInt());
                    break;
                case 'g': //取出数据
                    try {
                        int res = arrayQueue.getQueue();
                        System.out.printf("取出的数据是 %d\n",res);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'h'://查看队列头的数据
                    try {
                        int res = arrayQueue.headQueue();
                        System.out.printf("队列头的数据是%d\n",res);
                    }catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'e':// 退出
                    scanner.close();
                    break;
                default:break;
            }
        }
        System.out.println("程序退出");
    }
}



// 使用数组模拟队列-编写一个ArrayQueue类
class ArrayQueue {
    /*** 表示数组的最大容量*/
    private int maxSize;
    /*** 队列头*/
    private int front;

    /**
     * 队列尾
     */
    private int rear;
    //该数据用于存放数据，模拟队列
    private int[] arr;

    /**
     * 创建队列的构造器
     */
    public ArrayQueue(int arrMaxSize) {
        maxSize = arrMaxSize;
        arr = new int[maxSize];
        // 指向队列头部，分析出front是指向队列头的前一个位置
        front = -1;
        //只想队列尾部，指向队列尾的数据(即就是队列最后一个数据)
        rear = -1;
    }

    //判断队列是否满
    public boolean isFull() {
        return rear == maxSize - 1;
    }
    //判断队列是否为空
    public  boolean isEmpty(){
        return rear==front;
    }
    //添加数据到队列
    public void addQueue(int num){
        //判断队列是否满
        if(isFull()){
            System.out.println("队列已满不能加入数据");
            return;
        }
        // 让rear 后移
        arr[++rear] = num;
    }
    //获取队列的数据，让数据出队
    public int getQueue(){
        // 判断是否为空
        if(isEmpty()){
            //通过抛出异常处理
            throw new RuntimeException("队列里无数据，不能获取");
        }
        //front后移
        front++;
        return arr[front];
    }
    //显示队列的所有数据
    public void showQueue(){
        //遍历
        if(isEmpty()){
            System.out.println("队列为空，没有数据");
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.printf("arr[%d]=%d\n",i,arr[i]);
        }
    }
    //显示队列的头数据，注意不是取出数据 peek
    public int headQueue(){
        //判断
        if(isEmpty()){
             throw new RuntimeException("队列为空，没有数据!~~~");
        }
        // +1 是因为front是指向队列头的前一个位置，而并非指向队列头
        return arr[front+1];
    }
}
