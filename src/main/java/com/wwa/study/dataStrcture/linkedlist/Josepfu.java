package com.wwa.study.dataStrcture.linkedlist;

import lombok.Data;

/**
 * 约瑟夫问题：
 *  设编号为1,2,...n 的n个人围坐一圈，约定编号为k（1<=k<=n）的人从1 开始报数，数到m的那个人出列，它的下一位又从1开始报数，数到m的那个人又出列，依次类推，直到所有人出列为止，由此产生一个出队编号的序列。
 *  n=5 ，即有5个人
 *  k=1,从第一个人开始报数
 *  m =2 ,数2下。
 *  其出圈的顺序为  2->4->1->5->3
 *
 *  提示：
 *  用一个不带头节点的循环链表来处理Josephu 问题： 先构成一个有n个节点的单循环链表，然后由k 节点起从1 开始计数，计到 m 时，对应节点从链表中删除，然后再从被删除节点的下一个节点又从1开始计数，直到最后一个节点从链表中删除算法结束。
 * @author clark
 * @date 2021/3/25
 * @desc 单向环形链表
 * @return
 */
public class Josepfu {

    public static void main(String[] args) {
        CircleSingleLinkedList circleSingleLinkedList = new CircleSingleLinkedList();
        circleSingleLinkedList.addBoy(5);
        circleSingleLinkedList.showBoy();

        //测试一把 小孩出圈顺序 是否正确
        circleSingleLinkedList.countBoy(1,2,5);
    }

    /** 构建 一个单向的环形链表的思路
     * 1. 先创建第一个节点，让first指向该节点，并形成环形。
     * 2. 后面当我们每创建一个新的节点，就把该节点，加入到已有的环形链表中即可。
     *
     * 遍历环形链表
     * 1. 先让一个辅助指针（变量）curBoy, 指向fist节点
     * 2.  然后通过一个while循环遍历 该环形链表即可 curBoy.next  = first 结束
     * */



}

/**
 * 单向环形链表
 */
class CircleSingleLinkedList{
    //创建一个first节点，没有编号
    private Boy first = new Boy(-1);
    //添加小孩节点， 构成一个环形链表
    public void addBoy(int nums){
        if(nums<1){
            System.err.println("nums  的值不正确");
            return;
        }

        Boy curBoy = null;  // 辅助指针，帮助构建环形链表
        //创建循环链表
        for (int i = 1; i <= nums; i++) {
            Boy boy = new Boy(i);
            //根据编号创建小孩节点
            if(1 == i){  //如果是第一个小孩
                first = first.next = boy;   //构成环
                curBoy  =first;  //让curBoy指向第一个小孩
            }else{
                curBoy.next = boy;
                boy.next = first;    //构成环
                curBoy = boy;  //curBoy 向后偏移

            }
        }
    }

    //遍历当前的环形链表
    public void showBoy(  ){
     //判断链表是否为空
     if(first==null){
         System.err.println("m没有有任何小孩");

     }
     //因为first 指针后面还要用到它，所以不能动，要增加临时指针来进行遍历
        Boy curBoy = first;
     while (curBoy.next!=first){
         System.out.printf("小孩的编号为:%s\n",curBoy.no);
         curBoy = curBoy.next;  //curBoy 后移
     }
     //while循环结束 ，但停在了最后一个编号处，从而最后一个编号还没有输出
        System.out.printf("小孩的编号为:%s\n",curBoy.no);

    }

    /** 根据用户的输入，计算出小孩出圈的顺序
     *  n=5 ，即有5个人
     *  k=1,从第一个人开始报数
     *  m =2 ,数2下。
     *  其出队列的顺序为  2->4->1->5->3
     * @param startNo  表示从第几个小孩开始数数
     * @param countNum  表示数几下
     * @param nums  表示最初有多少小孩在圈中
     */
    public void countBoy(int startNo,int countNum,int nums){
        //先对数据进行校验
        if(first ==null || startNo <1 || startNo>nums){
            System.out.println("参数输入有误，清 重新输入");
            return;
        }
        //1.创建辅助指针helper(该指针实现应该指向环形链表的最后一个节点 )帮助完成小孩出圈
        Boy helper = first;
        //当 该while循环结束，则helper 就指向了最后一个节点了
        while(helper.next!=first){
            helper = helper.next;
        }

        //小孩报数前，先让first和helper 移动k-1次
        for (int i = 0; i < startNo - 1; i++) {
            first = first.next;
            helper = helper.next;
        }
        //2. 当小孩报数时，让first 和helper 指针同时 移动m-1次，然后出圈
        //这里是一个循环操作，直到圈中只有一个节点
        while(true){
            if(helper ==first){
                //说明圈中只有一个节点
                break;
            }
            // 让 fist 和 helper 指针同时 移动 countNum -1,然后出圈
            for(int j = 0; j< countNum-1; j++){
                first = first.next;
                helper = helper.next;
            }
            //这时 fist 指向的系欸但，就是要出圈的小孩节点
            System.out.printf("小孩 %d出圈\n",first.no);
            //这时 将 fist 指向的小孩节点出圈
            first = first.next;

            helper.next = first;
        }
        System.out.printf("最后留在圈中的小孩编号是%d\n",first.no);
        //3. 这时就可以将first指向的小孩节点出圈

    }
}


/**
 * Boy类 表示一个节点
 */

class Boy{
    protected int no; //编号
    protected Boy next; //指向下一个节点 ，默认null

    public Boy(int no) {
        this.no = no;
    }
}
