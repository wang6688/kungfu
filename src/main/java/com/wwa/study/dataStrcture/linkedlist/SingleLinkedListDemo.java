package com.wwa.study.dataStrcture.linkedlist;

import java.util.Stack;

/**
 * @param
 * @author clark
 * @date 2021/3/18
 * @desc 添加（创建）
 * 1. 先创建一个head头节点，作用就是表示单链表的头
 * 2. 后面我们每添加一个节点，就直接加入到链表的最后
 * 遍历：
 * 1.通过一个辅助遍历，帮助遍历整个链表
 * @return
 */

public class SingleLinkedListDemo {
    public static void main(String[] args) {
        HeroNode hero1 = new HeroNode(1, "宋将", "及时雨");
        HeroNode hero2 = new HeroNode(2, "卢俊义", "玉麒麟");
        HeroNode hero3 = new HeroNode(3, "吴用", "智多星");
        HeroNode hero4 = new HeroNode(4, "林冲", "豹子头");

        SingleLinkedList singleLinkedList = new SingleLinkedList();
        //1. 普通无序加入
//        singleLinkedList.add(songjiang);
//        singleLinkedList.add(lujunyi);
//        singleLinkedList.add(wuyong);
//        singleLinkedList.add(linchong);
        //2. 加入按照编号的顺序
        singleLinkedList.addByOrder(hero1);
        singleLinkedList.addByOrder(hero4);
        singleLinkedList.addByOrder(hero3);
        singleLinkedList.addByOrder(hero2);
        HeroNode heroUpdate = new HeroNode(2, "卢俊义2", "玉麒麟2");
        singleLinkedList.myupdate(heroUpdate);

//        singleLinkedList.delete(2);
//        singleLinkedList.delete(1);

        singleLinkedList.list();

        System.err.println("当前链表的长度是："+SingleLinkedList.getLength(singleLinkedList.getHead()));
//        System.err.println("倒数第k个节点是："+SingleLinkedList.findLastIndexNode(singleLinkedList.getHead(), 4));
//        SingleLinkedList.reverseList(singleLinkedList.getHead());
//        System.err.println("逆序后的链表是:");
//        singleLinkedList.list();
        SingleLinkedList.reversePrint(singleLinkedList.getHead());

    }

}

//定义SingleLinkedList
class SingleLinkedList{
    //初始化头节点，头节点不放具体的数据
    private HeroNode head = new HeroNode(0,"","");
    //添加节点到单向链表
    //思路，当不考虑编号顺序时
    //1. 找到当前节点的最后节点
    //2.将最后这个节点的next指向新的节点
    public void add(HeroNode heroNode){
        //因为head节点不能动，因此我们需要一个辅助遍历temp
         HeroNode temp =new HeroNode(0, "", "");
         temp =head;
        //遍历链表，找到最后
        while(true){
            //找到链表的最后
            if(null == temp.next){
                break;
            }
            //如果没有找到子链表，将temp后移
            temp = temp.next;
        }
        //当退出while循环时，temp就指向了链表的最后
        //将最后这个节点的next 指向新的 节点
        temp.next = heroNode;
    }

    //第二种方式在添加英雄时，根据排名将英雄插入到指定位置（如果有这个排名，则添加失败，并给出提示）
    //需要按照编号的顺序添加
    //1.首先找到新添加的节点的位置，是通过辅助变量（指针），通过遍历来搞定
    //2. 新的节点 next = temp.next
    //3. 将temp.next = 新的节点
    public void addByOrder(HeroNode heroNode){
        //因为头节点不能动，因此我们仍然通过一个辅助指针（变量）来帮助找到添加的位置
        HeroNode temp = head;

        //因为单链表，因为我们找到 temp是位于添加位置的前一个节点，否则插入不了
         //flag 标识添加的编号是否存在，默认为false
        boolean flag = false;
        //当temp.next==null 时说明temp已经在链表的最后
        while(null!= temp.next){

            //位置找到时，就在temp的后面 插入
            if(temp.next.no >heroNode.no){
//                temp.next = heroNode;
                break;
            }
            //说明希望添加的heroNode的编号已然存在
            if(temp.next.no == heroNode.no){
                System.err.println("请想插入的英雄编号已经存在，请修改英雄编号");
                flag = true;
                break;
            }
            //后移，遍历当前链表
            temp= temp.next;

        }
        //flag为true 则说明 编号存在不能添加
        if(!flag){
            heroNode.next = temp.next;
            temp.next = heroNode;
        }

    }

    // 修改节点的信息，根据no编号来修改，即no编号不能改
    //说明
    //1. 根据newHeroNode 的no来修改即可
    public void myupdate(HeroNode newHeroNode){
        //判断是否为空
        if(head.next==null){
            System.err.println("链表为空");
            return;
        }
        HeroNode temp = head;
        boolean hasChange = false;
         //当temp.next ==null 时说明temp已经在链表的最后
        while(null!= temp.next){
            //说明希望添加的heroNode的编号已然存在
            if(temp.next.no == newHeroNode.no){
                //将编号相同的新节点的next节点指向 迭代节点的下个节点
                newHeroNode.next = temp.next.next ;
                // 用编号相同的新节点 替换掉 迭代节点
                temp.next =newHeroNode;
                hasChange = true;
                break;
            }
            //后移，遍历当前链表
            temp = temp.next;

        }
        if(!hasChange){
            System.err.printf("没有找到编号%d的节点，不能i需改\n",newHeroNode.no);
        }
    }

    //从单链表中删除一个节点的思路
    //思路： head不能动， 因此我们需要一个temp辅助节点 找到待删除节点的前一个节点
    //2. 说明我们在比较时，是temp.next.no 和需要删除的节点的no比较
    public void delete(int waitDelNodeNo){
        //判断是否为空
        if(head.next==null){
            System.err.println("链表为空");
            return;
        }
        boolean hasFound = false;
        HeroNode temp = head;
        while(null!= temp.next){
            if(temp.next.no== waitDelNodeNo){
                temp.next = temp.next.next;
                hasFound = true;
                break;
            }
            temp = temp.next;
        }
        if(!hasFound){
            System.err.printf("没有找到编号%d的节点，不能删除\n",waitDelNodeNo);
        }
    }

    public HeroNode getHead() {
        return head;
    }

    /**
     * 尚硅谷update方法
     * @param newHeroNode
     */
    public void sggupdate(HeroNode newHeroNode){
        //判断是否为空
        if(head.next==null){
            System.err.println("链表为空");
            return;
        }
        //找到需要修改的节点，根据no编号
        HeroNode temp = head.next;
        boolean flag = false; //表示是否找到该节点
        while (true){
            if(temp ==null ){
                break;// 已经遍历完链表
            }
            if(temp.no == newHeroNode.no){
                // 找到
                flag = true;
                break;
            }
            temp = temp.next;
        }
        //根据flag 判断是否找到要修改的系欸但那
        if(flag){
            temp.name = newHeroNode.name;
            temp.nickName = newHeroNode.nickName;
        }else{
            System.out.printf("没有找到编号%d的节点，不能i需改\n",newHeroNode.no);
        }
    }
    //显示链表【遍历】
    public void list(){
        //判断链表是否为空
        if(head.next==null){
            System.err.println("链表为空");
            return;
        }
        //因为头节点不能动，因此需要一个辅助变量来遍历
        HeroNode temp = head;
        while(temp.next!=null){
            System.out.println(temp);
            temp = temp.next;
        }
        System.out.println(temp);

    }

    //获取到单链表的节点的个数（如果是带头节点的链表，需求不统计头节点）
    public static int getLength(HeroNode head) {
        int nodeLen = 0;
        HeroNode temp = head;
        while(temp.next!=null){
            ++nodeLen;
            temp=  temp.next;
        }
        return nodeLen;
    }

    //查找单链表中的倒数第k个节点【新浪面试题】
    //思路：1.编写一个方法，接收head节点，同时接收一个index
    //2.index表示 是倒数第index个节点
    //3. 先把链表从头到尾遍历，得到链表的总的长度getLength
    //4. 得到size后，我们从链表的第一个开始遍历（size-index）个，就可以得到
    //5. 如果找到了，则返回该界定啊，否则返回null
    public static HeroNode findLastIndexNode(HeroNode head,int index){
        //判断如果链表为空，返回null
        if(head.next==null){
            return  null;
        }
        int nodeLength = getLength(head);

        if(index<0|| index>nodeLength){
            return null;
        }
        HeroNode temp = head;
         int iterIndex = 0;
        while(temp.next!=null){
            if(iterIndex== nodeLength - index){
                return temp.next;
            }
            temp = temp.next;
            iterIndex++;
        }
        return null;
    }
    // 将单链表进行反转【腾讯面试题】
    //思路!!!!!
    //1. 先定义1个节点reverseHead = new HeroNode();
    //2.从头到尾遍历原来的链表，每遍历一个节点，就将其取出，并放在新的链表reverseHead的最前端
    //3. 原来链表的head.next = reverseHead.next;
    public static void reverseList(HeroNode head){
         //如果当前链表为空，或只有一个节点，无需反转
        if(head.next==null||head.next.next==null){
            return ;
        }
        //定义一个辅助的指针（变量），帮助我们遍历原来的链表
        HeroNode cur = head.next;
        HeroNode next = null; //指向当前节点【cur】 的下一个节点
        HeroNode reverseHead = new HeroNode(0,"tempReverseHead","临时反转的头节点");
        // 遍历原来的链表，每遍历一个节点，就将其取出，并放在新的链表reversHead的最前端
        while(cur!=null){
            //这4行代码 每一行 都很关键！！！！
            next = cur.next;//先暂时 保存当前节点的下一个节点，因为后面需要使用
            cur.next = reverseHead.next;  //将cur的下一个节点指向新的链表的最前端
            reverseHead.next = cur;   //将cur连接到新的链表上
            //让cur后移
            cur =  next;
         }
        //将head.next 指向 reverseHead.next ,实现单链表的反转
        head.next = reverseHead.next;
    }

    // 逆序打印单链表【百度面试题】
    // 思路：
    //1. 上面的题的要求就是逆序打印单链表.
    //2. 方式1： 现讲单链表进行反转操作，然后再遍历即可，这样的做的问题是会破坏原来的单链表的结构，不建议。
    //3. 方式2: 可以利用栈这个数据结构，将各个节点压入栈中，然后利用栈的先进后厨的特点，就实现了逆序打印的效果。
    public static void reversePrint(HeroNode head){
        if(head.next == null){
            return ;// 空链表 不能打印
        }
        //创建一个栈，将各个节点入栈
        Stack<HeroNode> stack = new Stack<>();
        HeroNode cur = head.next;
        //将链表的所有节点压入栈中
        while(cur!=null){
            stack.add(cur);
            cur = cur.next;
        }
        while(!stack.empty()){
            HeroNode node = stack.pop();
            System.err.println(node);
        }
    }


}


//定义HeroNode，每个HeroNode 对象就是一个节点
class HeroNode {
    public int no ;
    public String name;
    public String nickName;
    public HeroNode next; // 指向下一个节点

    public HeroNode(int no, String name, String nickName) {
        this.no = no;
        this.name = name;
        this.nickName = nickName;
    }


    //为了显示方柏霓，重写tostring

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                '}';
    }
}
