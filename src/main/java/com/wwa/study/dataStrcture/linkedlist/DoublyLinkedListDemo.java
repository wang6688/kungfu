package com.wwa.study.dataStrcture.linkedlist;

/**
 * @param
 * @author clark
 * @date 2021/3/25
 * @desc 分析双向链表的遍历，添加，修改，删除的操作思路===》 代码实现
 * 1） 遍历 方法和单链表一样，只是可以向前，也可以向后查找
 * 2） 添加(默认添加到双向链表的最后）
 * （1）先找到双向链表的最后这个节点
 * （2）temp.next = new HeroNode
 *  (3) newHeroNode.pre = temp;
 * 3) 修改思路和原理的单向链表一样。
 * 4）删除
 * （1）因为是双向链表，因此我们可以实现自我删除某个节点
 *  （2） 直接找到要删除的这个节点，比如temp
 *  （3） temp.pre.next = temp.next
 *   (4) temp.next.pre = temp.per;
 * @return
 */
public class DoublyLinkedListDemo {

    public static void main(String[] args) throws InterruptedException {
        HeroDoublyNode hero1 = new HeroDoublyNode(1, "宋将", "及时雨");
        HeroDoublyNode hero2 = new HeroDoublyNode(2, "卢俊义", "玉麒麟");
        HeroDoublyNode hero3 = new HeroDoublyNode(3, "吴用", "智多星");
        HeroDoublyNode hero4 = new HeroDoublyNode(4, "林冲", "豹子头");
        //创建一个双向链表
        DoublyLinkedList doublyLinkedList = new DoublyLinkedList();
        //1. 普通无序加入
//        doublyLinkedList.add(hero1);
//        doublyLinkedList.add(hero2);
//        doublyLinkedList.add(hero3);
//        doublyLinkedList.add(hero4);
//
//        doublyLinkedList.list();
        //2. 加入按照编号的顺序
        doublyLinkedList.addByOrder(hero1);
        doublyLinkedList.addByOrder(hero4);
        doublyLinkedList.addByOrder(hero3);
        doublyLinkedList.addByOrder(hero2);
        doublyLinkedList.list();
        //修改
        HeroDoublyNode updateNode = new HeroDoublyNode(4, "公孙胜", "入云龙");
        doublyLinkedList.sggupdate(updateNode);
        System.out.println("修改后的链表的情况");
        Thread.sleep(100);
        doublyLinkedList.list();
        //删除
        System.out.println("删除后的链表的情况");
        doublyLinkedList.delete(3);
        Thread.sleep(1000);

        doublyLinkedList.list();
    }


}

//创建一个双向链表的bean
class HeroDoublyNode {
    public int no ;
    public String name;
    public String nickName;
    public HeroDoublyNode next; // 指向下一个节点
    public HeroDoublyNode pre; // 指向前一个节点

    public HeroDoublyNode(int no, String name, String nickName) {
        this.no = no;
        this.name = name;
        this.nickName = nickName;
    }


    //为了显示方柏霓，重写tostring

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                '}';
    }

}
class DoublyLinkedList{
    //先初始化一个头节点，头节点不要动，不存放具体的数据
    private HeroDoublyNode head = new HeroDoublyNode(0,"","");
    //返回头节点
    public HeroDoublyNode getHead(){
        return head;
    }
    //遍历双向链表的方法
    public void list(){
        //判断链表是否为空
        if(head.next==null){
            System.err.println("链表为空");
            return;
        }
        //因为头节点不能动，因此需要一个辅助变量来遍历
        HeroDoublyNode temp = head;
        while(temp.next!=null){
            System.out.println(temp);
            temp = temp.next;
        }
        System.out.println(temp);
    }
    //添加一个节点到双向链表的最后
    public void add(HeroDoublyNode heroNode){
        //因为head节点不能动，因此我们需要一个辅助遍历temp
        HeroDoublyNode temp =head;
        //遍历链表，找到最后
        while(null != temp.next){
            //如果没有找到子链表，将temp后移
            temp = temp.next;
        }
        //当退出while循环时，temp就指向了链表的最后
        //将最后这个节点的next 指向新的 节点
        //形成一个双向链表
        temp.next = heroNode;
        heroNode.pre = temp;
    }

    //添加一个节点到双向链表的最后
    public void addByOrder(HeroDoublyNode heroNode){
        boolean flag = false;
        //因为head节点不能动，因此我们需要一个辅助遍历temp
        HeroDoublyNode temp =head;
        //遍历链表，找到最后
        while(null != temp.next){
            //如果没有找到子链表，将temp后移
            //位置找到时，就在temp的后面 插入
//            if( heroNode.no< temp.next.no){
            if(  temp.next.no >heroNode.no){

                heroNode.next = temp.next;
                heroNode.pre = temp.pre;
                break;
            }
            //说明希望添加的heroNode的编号已然存在
            if(temp.next.no == heroNode.no){
                System.err.println("请想插入的英雄编号已经存在，请修改英雄编号");
                flag = true;
                break;
            }
            temp = temp.next;
        }
        //当退出while循环时，temp就指向了链表的最后
        //将最后这个节点的next 指向新的 节点
        //形成一个双向链表
        //flag为true 则说明 编号存在不能添加
        if(!flag){
             temp.next = heroNode;
             heroNode.pre = temp;
        }
    }

    /**
     * 尚硅谷update方法:修改双向链表的方法基本与单向链表一致
     * @param newHeroNode
     */
    public void sggupdate(HeroDoublyNode newHeroNode){
        //判断是否为空
        if(head.next==null){
            System.err.println("链表为空");
            return;
        }
        //找到需要修改的节点，根据no编号
        HeroDoublyNode temp = head.next;
        boolean flag = false; //表示是否找到该节点
        while (true){
            if(temp ==null ){
                break;// 已经遍历完链表
            }
            if(temp.no == newHeroNode.no){
                // 找到
                flag = true;
                break;
            }
            temp = temp.next;
        }
        //根据flag 判断是否找到要修改的系欸但那
        if(flag){
            temp.name = newHeroNode.name;
            temp.nickName = newHeroNode.nickName;
        }else{
            System.out.printf("没有找到编号%d的节点，不能i需改\n",newHeroNode.no);
        }
    }
//    public void myupdate(HeroDoublyNode newHeroNode){
//        //判断是否为空
//        if(head.next==null){
//            System.err.println("链表为空");
//            return;
//        }
//        HeroDoublyNode temp = head;
//        boolean hasChange = false;
//        //当temp.next ==null 时说明temp已经在链表的最后
//        while(null!= temp.next){
//            //说明希望添加的heroNode的编号已然存在
//            if(temp.next.no == newHeroNode.no){
//                //将编号相同的新节点的next节点指向 迭代节点的下个节点
//                newHeroNode.next = temp.next.next ;
//                // 用编号相同的新节点 替换掉 迭代节点
//                newHeroNode.pre = temp.next;
//
//                 hasChange = true;
//                break;
//            }
//            //后移，遍历当前链表
//            temp = temp.next;
//        }
//        if(!hasChange){
//            System.err.printf("没有找到编号%d的节点，不能i需改\n",newHeroNode.no);
//        }
//    }


    //从双向链表中删除一个节点
    //1.对于双向链表，我们可以直接找到要删除的这个节点
    //2. 找到 后 ，自我删除即可。
    public void delete(int waitDelNodeNo){
        //判断是否为空
        if(head.next==null){
            System.err.println("链表为空");
            return;
        }
        boolean hasFound = false;
        //辅助变量（指针）
        HeroDoublyNode temp = head.next;
        while(null!= temp){
            if(temp.no== waitDelNodeNo){
                //拆掉 待删除节点,使待删除节点的前一个节点的next 直接指向待删除节点的后一个节点
                temp.pre.next =temp.next;
                if(temp.next!=null){
                    //如果是最后一个节点就不再需要执行这行代码
                    //使待删除节点的后一个节点的pre 指向 待删除节点的前一个节点
                    temp.next.pre = temp.pre;
                }
                hasFound = true;
                break;
            }
            temp = temp.next;
        }
        if(!hasFound){
            System.err.printf("没有找到编号%d的节点，不能删除\n",waitDelNodeNo);
        }
    }
}
