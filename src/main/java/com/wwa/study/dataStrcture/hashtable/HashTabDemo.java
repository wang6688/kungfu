package com.wwa.study.dataStrcture.hashtable;

import java.util.Scanner;

/**
 * 哈希表（散列） -Google 上机题
 * 看一个实际需求，google公司的一个上机题：
 * 有一个公司，当有新的员工来报道时，要求将该员工的信息加入（id,性别，你那零，住址...) ，当输入该员工的id时，要求查找到该员工的所有信息。
 * 要求： 不适用数据库，尽量节省内存，速度越快越好=》 哈希表（散列）
 * 添加时，保证按照 id从低到高插入【课后思考： 如果id不是从低到高插入时，但要求各调链表仍是从低到高，怎么解决？】
 * 1) 使用链表 来实现哈希表，该链表不带表头【即： 链表的第一个节点就存放雇员信息】
 * 2） 思路分析 并画出示意图
 * 3） 代码实现【增删改查（显示所有员工，按id查询】
 *
 * @date Created in 2021/6/17
 * @modified By：
 */
public class HashTabDemo {
    public static void main(String[] args) {
        HashTab hashTab = new HashTab(7);
        //写一个简单的菜单
        String key = "";
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("add : 添加雇员");
            System.out.println("list : 显示雇员");
            System.out.println("find : 查找雇员");
            System.out.println("del : 删除雇员");
            System.out.println("exit : 退出系统");
            key = scanner.next();
            switch (key) {
                case "add":
                    System.out.println("输入id");
                    int id = scanner.nextInt();
                    System.out.println("输入名字");
                    String name = scanner.next();
                    // 创建雇员
                    Emp emp = new Emp(id, name);
                    hashTab.add(emp);
                    break;
                case "list":
                    System.out.println("输出 哈希表");
                    hashTab.list();
                    break;
                case "find":
                    System.out.println("请输入要查找的雇员id");
                    id = scanner.nextInt();
                    hashTab.findEmpById(id);
                    break;
                case "del":
                    System.out.println("请输入要删除的雇员id");
                    id = scanner.nextInt();
                    hashTab.delEmpById(id);
                    break;
                case "exit":
                    scanner.close();
                    System.exit(0);
            }
        }
    }
}

// 创建 HashTab  管理多条链表
class HashTab {

    private EmpLinkedList[] empLinkedListArray;
    private int size;

    //构造器
    public HashTab(int size) {
        this.size = size;
        // 初始化 empLinkedListArray
        empLinkedListArray = new EmpLinkedList[size];
        //? 留一个 坑 , 这是 不要忘了  ，需要分别初始化每个链表
        for (int i = 0; i < empLinkedListArray.length; i++) {
            empLinkedListArray[i] = new EmpLinkedList();
        }
    }

    // 添加雇员
    public void add(Emp emp) {
        // 根据员工的id， 得到该员工应该添加到哪条链表
        int empLinkedListNo = hashFun(emp.id);
        // 将 emp 添加到 对应的链表中
        empLinkedListArray[empLinkedListNo].add(emp);
    }

    // 遍历所有的链表,遍历hashtab
    public void list() {
        for (int i = 0; i < size; i++) {
            empLinkedListArray[i].list(i);
        }
    }

    // 根据输入 的id，查找雇员
    public void findEmpById(int id) {
        // 使用 散列函数确定到哪条链表查找
        int empLinkedListNo = hashFun(id);
        Emp emp = empLinkedListArray[empLinkedListNo].findEmpById(id);
        if (emp != null) {
            // 找到
            System.out.printf(" 在第%d条链表中找到 雇员id = %d\n", (empLinkedListNo + 1), id);
        } else {
            System.out.println(" 在 哈希表中，没有找到该雇员");
        }
    }

    // 根据输入 的id，删除该雇员
    public void delEmpById(int id) {
        // 使用 散列函数确定到哪条链表查找
        int empLinkedListNo = hashFun(id);
        boolean isDeleted = empLinkedListArray[empLinkedListNo].delEmpById(id);
        if (isDeleted) {
            // 找到
            System.out.printf(" 在第%d条链表中找到并删除雇员id = %d 成功\n", (empLinkedListNo + 1), id);
        } else {
            System.out.println(" 在 哈希表中，没有找到该雇员，删除失败");
        }
    }

    // 编写散列函数，使用简单取模法
    public int hashFun(int id) {
        return id % size;
    }

}

//表示一个雇员
class Emp {
    public int id;
    public String name;
    public Emp next;  //默认为null

    public Emp(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }
}

//创建EmpLinkedList , 表示链表
class EmpLinkedList {
    // 头指针，指向第一个Emp，因此 我们这个链表的head 是直接指向第一个 Emp
    private Emp head; // 默认 null;


    // 添加雇员到链表
    //说明
    //1. 假定，当添加雇员时，id 是自增长， 即id的分配总是从小到大。
    //  因此 我们将该雇员直接加入到 本链表的最后即可。
    public void add(Emp emp) {
        // 如果是 添加 第一个雇员
        if (head == null) {
            head = emp;
            return;
        }
        // 如果不是第一个雇员，则使用一个辅助的指针， 帮助定位到最后
        Emp curEmp = head;
        // 循环结束则说明到了链表最后
        while (curEmp.next != null) {
            curEmp = curEmp.next;  // 后移
        }
        // 退出时直接将 emp 加入链表
        curEmp.next = emp;
    }

    //遍历链表的雇员信息
    public void list(int no) {
        if (head == null) {  // 说明链表为空
            System.out.println("第" + (no + 1) + "条链表为空");
            return;
        }
        System.out.printf("当前%d条链表的信息为", no + 1);
        Emp curEmp = head; // 辅助指针
        // 循环结束时，则说明 curEmp 已经时最后节点
        while (true) {

            System.out.printf(" =>id =%d name =%s\t", curEmp.id, curEmp.name);
            if (curEmp.next == null) { //说明 curEmp 已经时最后节点
                break;
            }
            curEmp = curEmp.next;  // 后移，遍历

        }
        System.out.println();
    }

    // 根据id 查找雇员
    // 如果查找到，就返回Emp，如果没有找到，就返回null
    public Emp findEmpById(int id) {
        //判断链表是否为空
        if (head == null) {
            System.out.println("链表为空");
            return null;
        }
        // 辅助指针
        Emp curEmp = head;
        while (true) {
            if (curEmp.id == id) {
                // 找到
                break;  // 这时 curEmp 就指向要查找的雇员
            }
            //退出
            if (curEmp.next == null) {  // 说明当前链表 没有找到要查找的元素
                return null;
            }
            curEmp = curEmp.next;  // 以后
        }
        return curEmp;
    }

    // 根据id删除雇员
    // 如果查找到，就返回Emp删除成功，如果没有找到，就返回null
    public boolean delEmpById(int waitDelCurId) {
        boolean hasFound = false;
        //判断链表是否为空
        if (head == null) {
            System.out.println("链表为空");
            return false;
        } else {
            if (head.id == waitDelCurId) {
                head = null;
                return true;
            }
        }
        Emp temp = head;
        while (true) {
            if (temp.next.id == waitDelCurId) {
                temp.next = temp.next.next;
                hasFound = true;
                break;
            }
            temp = temp.next;
        }

        return hasFound;
    }
}