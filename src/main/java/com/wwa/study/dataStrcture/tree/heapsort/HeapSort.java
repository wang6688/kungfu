package com.wwa.study.dataStrcture.tree.heapsort;


import java.util.Arrays;

/**
  堆排序
  很重要！！！！
 堆排序，不是很容易理解，建议通过debug 以及画二叉树大顶堆图的 方式加强理解
 堆排序的速度非常快，其时间复杂度是 O(n log n) 线性对数阶乘
 * @date Created in 2021/6/21
  */
public class HeapSort {

    public static void main(String[] args) {
        // 要求 将数组进行升序排序
        int arr[] = {4,6,8,5,9};
      heapSort(arr);

    }


    //编写 一个堆排序的方法
    public static void heapSort(int arr[]){
        System.out.println("堆排序！！");
        int temp =0;
        //分步完成
//        adjustHeap(arr,1, arr.length);
//        System.out.printf("第2次调整 大顶堆结构后，数据结构是：%s\n", Arrays.toString(arr));  //[4, 9, 8, 5, 6]
//        adjustHeap(arr,0, arr.length);
//        System.out.printf("第2次调整 大顶堆结构后，数据结构是：%s\n", Arrays.toString(arr));   //[9, 6, 8, 5, 4]
        //最终代码
        // 1.将无序序列 构建成一个堆，而根据升序降序 需求 选择 大顶堆 或小顶堆。
        for (int i = arr.length/2-1; i >=0  ; i--) {
            adjustHeap(arr,i,arr.length);
        }
        // 2. 将堆顶元素与末尾元素交换，将最大元素 ”沉“ 到数组末端；
        //3. 重新调整结构，使其满足堆定义，然后继续交换 堆顶元素 与当前末尾元素，反复执行 调整+ 交换步骤，直到整个序列有序。
        for (int j = arr.length-1; j >0; j--) {
            // 交换
            temp = arr[j];
            arr[j] = arr[0];
            arr[0] = temp;
            adjustHeap(arr,0,j);

        }
        System.out.println("数组="+Arrays.toString(arr));

    }

    /**
     *
     *
     * 将一个数组（二叉树），调整成一个大顶堆
     * 功能： 完成  将 以 i 对应的非叶子节点的树 调整成大顶堆。
     * 举例：int arr[] = {4，6，8，5，9}；
     *                  4
     *              6       8
     *           5    9
     *
     * => i = 1 => adjustHeap => 得到{4,9,8,5,6}
     *                  4
     *              9       8
     *           5    6
     *
     * 如果我们再次调用 adjustHeap 传入的是 i = 0 => 得到 {4,9,8,5,6} =>{9,6,8,5,4}
     *                  9
     *              4       8
     *          5     6
     *
     *
     *                  9
     *              6       8
     *          5     4
     *
     * @param arr 待调整的数组
     * @param   nonLeafNodeIndex 表示 非叶子节点在数组中的索引
     * @param length 表示对多少个元素继续调整，length在逐渐减少
     */
    public static void adjustHeap(int[] arr,int nonLeafNodeIndex,int length) {
        int temp = arr[nonLeafNodeIndex];  // 先取出 当前 非叶子节点 元素的值，保存在临时变量
        // 开始调整
        // 说明
        //1. sonUnderNonLeafNodeIndex = nonLeafNodeIndex*2+1 ; sonUnderNonLeafNodeIndex 是nonLeafNodeIndex节点的左子节点  ；sonUnderNonLeafNodeIndex=sonUnderNonLeafNodeIndex*2+1  是将sonUnderNonLeafNodeIndex指向 nonLeafNodeIndex的孙级左子节点的索引
        for(int sonUnderNonLeafNodeIndex = nonLeafNodeIndex*2+1; sonUnderNonLeafNodeIndex<length; sonUnderNonLeafNodeIndex=sonUnderNonLeafNodeIndex*2+1){
            //k+1 是指向的 非叶子节点i 的右子节点， k +1 <length 则说明未到 当前的非叶子节点i的右子节点 未到达 大顶堆数组的末端。
            if(sonUnderNonLeafNodeIndex +1 <length && arr[sonUnderNonLeafNodeIndex] <arr[sonUnderNonLeafNodeIndex+1]){  // 说明左子节点的值小于右子节点的值
                sonUnderNonLeafNodeIndex++;  // k 指向右子节点
            }
            if(arr[sonUnderNonLeafNodeIndex] >temp){   // 如果非叶子节点i的 左子节点或右子节点 元素的值 大于 非叶子节点i元素的值
                // 把较大的值赋给非叶子节点i
                arr[nonLeafNodeIndex] = arr[sonUnderNonLeafNodeIndex];
                nonLeafNodeIndex = sonUnderNonLeafNodeIndex; //!!! 将非叶子节点i  指向元素值较大的子节点k， 继续循环比较
            }else {
                break; // !!!
            }
        }

        // 当for 循环结束后，我们已经将nonLeafNodeIndex 为父节点的树的最大值，放在了 最顶(局部）
        arr[nonLeafNodeIndex] = temp; // 将temp 值 放到 调整后的位置。

    }
}
