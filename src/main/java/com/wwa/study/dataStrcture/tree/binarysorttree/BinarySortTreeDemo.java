package com.wwa.study.dataStrcture.tree.binarysorttree;


import javafx.scene.chart.ValueAxis;

/**
 * 看一个需求：给你一个数列（7，3，10，12，5，1,9），要求能够高效的完成对数据的查询和添加。
 * 解决方案分析
 *  使用数组
 *  1) 数组未排序，优点：直接在数组尾添加，速度快。 缺点： 查找速度慢。
 *  2）数组排序，优点： 可以使用二分查找，查找速度快，缺点：为了保证数组有序，在添加新数据时，找到插入位置后，后面的数据需整体移动，速度慢。
 *
 *  使用链式存储-链表
 *  不管链表是否有序查找速度都慢，添加数据速度比数组快，不需要数据整体移动。
 *
 *  使用二叉排序树。
 *
 */
public class BinarySortTreeDemo {

    public static void main(String[] args) {
        int[] arr = {7,3,10,12,5,1,9,2};
        BinarySortTree binarySortTree = new BinarySortTree();
        // 循环的添加节点 到二叉排序树
        for (int i = 0; i < arr.length; i++) {
            binarySortTree.add(new Node(arr[i]));
        }

        // 中序遍历二叉排序树
        System.out.println("中序遍历二叉排序树");
        // 结果刚好是升序排列的。
        binarySortTree.infixOrder();  //1,3,5,7,9,10,12


//         测试删除叶子节点
        binarySortTree.delNode(2);
        binarySortTree.delNode(5);
        binarySortTree.delNode(9);
        binarySortTree.delNode(12);
        // 测试 删除只有一颗子树的节点1
//        binarySortTree.delNode(1);
// 测试 删除有两颗子树的节点.(比如：7，3，10)
        binarySortTree.delNode(7);
        System.out.println("删除节点后");
        binarySortTree.infixOrder();
        System.out.println("测试修复 删除 bug问题");

        binarySortTree.delNode(3);
        System.out.println("当前二叉排序树的root节点shi :"+binarySortTree.getRoot());;
        binarySortTree.delNode(10);
//        binarySortTree.delNode(1);
        binarySortTree.infixOrder();

    }

}
// 创建二叉排序树
class BinarySortTree{
    private Node root;

    public Node getRoot() {
        return root;
    }

    // 查找 要删除的 节点
    public Node search(int value){
        if(root == null){
            return null;
        }else {
            return root.search(value);
        }
    }
    // 查找父节点
    public Node searchParent(int value){
        if (root == null){
            return null;
        }else{
            return root.searchParent(value);
        }
    }

    /** 1. 返回 以node 为根节点的二叉排序树的 最小节点的值
     * 2.删除 node 为根节点的二叉排序树的最小节点
     * @param node 传入的节点（当作二叉排序树的根节点）
     * @return 返回的以node 为根节点的二叉排序树的 最小节点的值
     */
    public int delRightTreeMin(Node node){
        Node target = node;
        // 循环的查找左节点，就会找到最小值
        while(target.left !=null){
            target = target.left;
        }
        // 这时候 target 就指向了最小节点
        // 删除最小节点
        delNode(target.value);
        return target.value;
    }
    //删除节点
    public void delNode(int value){
        if(root == null){
            return;
        }else{
            //1. 需要先去找到要删除的节点 targetNode
            Node targetNode = search(value);
            //如果没有找到要删除的节点
            if(targetNode == null){
                return ;
            }
            // 如果我们发现当前这颗二叉排序树 只有一个节点
            if(root.left == null && root.right == null){
                root = null;
                return ;
            }
            // 去找到targetNode的父节点
            Node parent = searchParent(value);
            // 如果要删除的节点是 叶子节点
            if(targetNode.left == null && targetNode.right == null){
                // 判断targetNode  是父节点的左子节点，还是右子节点
                if(parent.left!=null && parent.left.value == value){  // 是左子节点
                    parent.left = null;
                }else if(parent.right != null && parent.right.value == value ){ // 是右子节点
                    parent.right = null;
                }
            }else if(targetNode.left != null && targetNode.right !=null){  // 删除有两颗子树的节点
                int minVal = delRightTreeMin(targetNode.right);
                targetNode.value = minVal;

                //todo 作业： 从targetNode的 左子树找到最大的节点，然后按照前面的思路完成。
            }else{// 删除只有一颗子树的节点
                // 如果要删除的节点  有左子节点
                if(targetNode.left !=null){
                    if( parent== null){   // 在删除一颗子树时，要考虑它的父节点为空的情况
                        root = targetNode.left;
                        return;
                    }
                    // 如果targetNode 是 parent的左子节点
                    if(parent.left.value == value){
                        parent.left = targetNode.left;
                    }else{  // targetNode  是parent 的右子节点
                        parent.right = targetNode.left;
                    }
                }else{  // 如果要删除的节点有 右子节点
                    // 如果 targetNode 是 parent 的左子节点


                    if( parent== null){   // 在删除一颗子树时，要考虑它的父节点为空的情况
                        root = targetNode.left;
                        return;
                    }
                    if(parent.left.value == value){
                        parent.left = targetNode.right;
                    }else{ // 如果 targetNode 是 parent的右子节点
                        parent.right = targetNode.right;

                    }
                }
            }
        }
    }

    // 添加节点的方法
    public void add(Node node){
        if(root == null){
            root = node;  // 如果root为空，则直接让root指向node
        }else{
            root.add(node);
        }
    }

    //中序遍历
    public void infixOrder(){
        if(root !=null){
            root.infexOrder();
        }else{
            System.out.println("二叉排序树为空，不能遍历。");
        }
    }
}



// 创建Node 节点
class Node{
    int value;
    Node left;
    Node right;
    public Node(int value){
        this.value = value;
    }

    /**
     * @param value 希望删除的节点的值
     * @return 如果找到返回该节点，否则返回null
     */
    // 查找要删除的节点
    public Node search(int value){
        if(value == this.value){  // 找到就是该节点
            return this;
        }else if(value <this.value){  // 如果查找的值 小于当前节点，向左子树递归查找
            // 如果左子节点为空，则跳过
            if(this.left==null){
                return  null;
            }
           return this.left.search(value);
        }else{  // 如果查找的值不小于 当前节点，向右子树递归查找
            if(this.right == null){
                return null;
            }
            return this.right.search(value);
        }
     }
    // 查找要删除 节点的父节点
    public Node searchParent(int value){
        // 如果当前节点就是要删除的节点的父界定啊，就返回
        if((this.left!=null && this.left.value==value) || (this.right !=null && this.right.value == value)){
            return this;
        }else{
            // 如果查找的值小于 当前节点的值，并且当前节点的左子节点 不为空。
            if(value < this.value && this.left!=null){
                return this.left.searchParent(value);// 向左子树递归查找
            }else if(value>=this.value && this.right!=null){
                return this.right.searchParent(value);  // 向右子树递归查找
            }else {
                return null;   // 没有找到父节点
            }
        }
    }
    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }

    // 添加节点的方法,以递归的形式添加节点，注意需要满足二叉排序树的要求
    public void add(Node node){
        if(node == null){
            return ;
        }
        //判断传入的节点的值，和当前子树的根节点的值关系
        if(node.value <this.value){
            // 如果当前节点的 左子节点为null
            if(this.left == null){
                this.left = node;
            }else{
                // 递归的向左子树添加
                this.left.add(node);
            }
        }else{
            //添加的节点的值 > 当前节点的值
            if(this.right == null){
                this.right = node;
            }else{
                // 递归的向右子树添加。
                this.right.add(node);
            }
        }
    }

    //中序遍历
    public void infexOrder(){
        if(this.left!=null){
            this.left.infexOrder();
        }
        System.out.println(this);
        if(this.right!=null){
            this.right.infexOrder();
        }
    }

}
