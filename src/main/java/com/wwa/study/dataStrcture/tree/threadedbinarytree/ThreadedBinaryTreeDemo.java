package com.wwa.study.dataStrcture.tree.threadedbinarytree;

import lombok.Getter;
import lombok.Setter;

/**
 * 线索化二叉树
 *
 * 将数列{1,3,6,8,10,14}构建成一颗二叉树。
 *                     |1|
 *                |3|         |6|
 *           |8|     |10|   |14|
 * @description
 * @date Created in 2021/6/21
 * @modified By：
 */
public class ThreadedBinaryTreeDemo {
    public static void main(String[] args) {
        // 测试 中序线索二叉树的功能
        HeroNode root = new HeroNode(1,"tom");
        HeroNode node2 = new HeroNode(3,"jack");
        HeroNode node3 = new HeroNode(6,"smith");
        HeroNode node4 = new HeroNode(8,"mary");
        HeroNode node5 = new HeroNode(10,"king");
        HeroNode node6 = new HeroNode(14,"dim");

        //二叉树，后面会递归进行创建，现在简单处理-手动创建。
        root.setLeft(node2);
        root.setRight(node3);
        node2.setLeft(node4);
        node2.setRight(node5);
        node3.setLeft(node6);

        //测试线索化
        ThreadedBinaryTree threadedBinaryTree = new ThreadedBinaryTree();
        threadedBinaryTree.setRoot(root);
        threadedBinaryTree.threadedNodes();

        //测试 以节点10 为样例
        HeroNode leftNode = node5.getLeft();
        HeroNode rightNode = node5.getRight();

        System.out.println("节点10 的前驱节点是:"+leftNode); //3
        System.out.println("节点10 的后继节点是:"+rightNode);  //1

        // 当线索化二叉树 后，使用原来的遍历方法
//        threadedBinaryTree.infixOrder();   // 会有问题，
        System.out.println("使用线索化的方式遍历 线索化二叉树");
        threadedBinaryTree.threadedList();

        //todo 作业练习：
        // 参考中序线索化二叉树，实现 前序线索化二叉树  和 后序线索化二叉树的功能
    }
}

//创建 HeroNode 节点
@Getter
@Setter
class HeroNode {
    private int no;
    private String name;
    private HeroNode left; // 默认null
    private HeroNode right;  // 默认null
    // 如果leftType==0  表示指向的是左子树，如果==1 则表示指向 前驱节点
    private int leftType;
    // 如果rightType ==0 ,表示指向的是右子树，如果==1 表示指向 后继节点
    private int rightType;
    public HeroNode(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                '}';
    }

    /**
     * 递归删除节点
     * 1.如果删除的节点是叶子节点，则删除该节点
     * 2.如果删除的节点是 非叶子节点，则删除该子树
     *
     * @param no
     */
    public void delNode(int no) {
    /* 思路：
首先先处理：考虑如果树是空树root，如果只有一个root节点，则等价将二叉树置空。
然后进行下面步骤：
1.因为我们的二叉树是单向的，所以我们是判断当前节点的子节点是否需要删除节点，而不能去判断当前这个节点是不是需要删除节点。
2.如果当前节点的左子节点不为空，并且左子节点就是要删除节点，就将this.left=null; 并且就返回(结束递归删除)
3.如果当前节点的右子节点不为空，并且右子节点就是要删除节点，就讲 this.right=null; 并且 就返回(结束递归删除)
4.如果第2和第3步 没有删除节点，那么我们就需要向左子树进行递归删除。
5.如果第4步也没有删除节点，则应当向右子树进行递归删除。*/

        //2.如果当前节点的左子节点不为空，并且左子节点就是要删除节点，就将this.left=null; 并且就返回(结束递归删除)
        if (this.left != null && this.left.no == no) {
            this.left = null;
            return;
        }
        //3.如果当前节点的右子节点不为空，并且右子节点就是要删除节点，就讲 this.right=null; 并且 就返回(结束递归删除)
        if (this.right != null && this.right.no == no) {
            this.right = null;
            return;
        }
        //   4. 我们就需要向左子树进行递归删除
        if (this.left != null) {
            this.left.delNode(no);
        }
        //5.则应当向右子树进行递归删除
        if (this.right != null) {
            this.right.delNode(no);
        }

    }

    // 编写前序遍历的方法：先输出 (根节点/有叶子节点的子节点),再输出对应(根节点/有子节点)的 左测叶子节点，最后输出右侧的叶子节点
    public void preOrder() {
        //先输出父节点
        System.out.println(this);
        // 递归 向 左子树 前序遍历
        if (this.left != null) {
            this.left.preOrder();
        }
        // 递归 向 右子树 前序遍历
        if (this.right != null) {
            this.right.preOrder();
        }
    }

    // 中序(父节点在中间)遍历的方法：先输出左侧的叶子节点，再输出对应叶子节点的所属上级“子节点/根节点”, 最后输出右侧的叶子节点
    public void infixOrder() {
        //递归向 左子树遍历
        if (this.left != null) {
            this.left.infixOrder();
        }
        //输出父节点
        System.out.println(this);
        //递归向右子树遍历
        if (this.right != null) {
            this.right.infixOrder();
        }
    }

    // 后序遍历的方法: 先输出左侧的叶子节点，再输出右侧 的叶子节点，最后再输出对应的叶子节点的所属上级“子节点” 直到最终的根节点
    public void postOrder() {
        //递归向 左子树 后序遍历
        if (this.left != null) {
            this.left.postOrder();
        }
        //递归向 右子树 后序遍历
        if (this.right != null) {
            this.right.postOrder();
        }
        //输出父节点

        System.out.println(this);
    }

    /**
     * 前序查找思路： 1 先判断当前节点的no 是否等于 要查找的
     * 2 如果是相等，则返回当前节点
     * 3. 如果不等，则判断当前节点的左子节点是否为空，如果不为空，则递归前序查找。
     *
     * @param no 查找的编号
     * @return 如果找到就返回 该Node，如果没有找到 返回null
     */
    public HeroNode preOrderSearch(int no) {
        System.out.println("进入前序遍历");

        // 比较当前(父节点/有叶子节点的子节点)节点 是不是 要查找的节点
        if (this.no == no) {
            return this;
        }
        //1.判断 当前节点的左子节点是否为空，如果不为空，则递归前序查找
        //2. 如果 左递归前序查找，找到节点，则返回

        HeroNode resultNode = null;
        if (this.left != null) {
            resultNode = this.left.preOrderSearch(no);

        }
        if (resultNode != null) {
            // 说明在 左子树内找到
            return resultNode;
        }
        //1. 左递归前序查找，找到节点，则返回，否则继续判断，
        //2. 当前的节点的右子节点但是否为空，如果不空，则继续向右递归前序查找
        if (this.right != null) {
            resultNode = this.right.preOrderSearch(no);
        }
        return resultNode;
    }


    /**
     * 中序查找思路：1.判断当前节点的左子节点是否为空，如果不为空，则递归中序查找
     * 2. 如果找到，则返回，如果没有找到，就和当前节点比较，如果是则返回当前节点，否则继续进行右递归的中序查找
     * 3.如果右递归 中序查找，找到就返回，否则返回null。
     *
     * @param no
     * @return
     */
    // 中序遍历查找
    public HeroNode infixOrderSearch(int no) {
        // 判断当前节点的的左子节点那是否为空， 如果不为空， 则递归中序查找。

        HeroNode resultNode = null;
        if (this.left != null) {
            resultNode = this.left.infixOrderSearch(no);
        }
        if (resultNode != null) {
            return resultNode;
        }
        System.out.println("进入中序查找");
        //如果 找到，则返回，如果没有找到，就和当前节点比较，如果是 则返回当前节点
        if (this.no == no) {
            return this;
        }
        // 否则继续进行右递归的中序查找
        if (this.right != null) {
            resultNode = this.right.infixOrderSearch(no);
        }
        return resultNode;
    }

    /**
     * 后序查找思路： 1. 判断当前节点的左子节点是否为空，如果不为空，则递归后续查找
     * 2. 如果找到，就返回，如果没有找到，就判断当前节点的右子节点是否为空，如果不为空，则右递归进行后序查找，如果找到，就返回。
     * 3. 就和当前节点进行，比如， 如果是则返回，否则返回null。
     *
     * @param no
     * @return
     */
    //后序遍历查找
    public HeroNode postOrderSearch(int no) {
//        判断当前节点的左子节点是否为空，如果不为空，则递归后续查找
        HeroNode resultNode = null;
        if (this.left != null) {
            resultNode = this.left.postOrderSearch(no);
        }
        if (resultNode != null) {  //说明在左子树找到
            return resultNode;
        }
        // 如果左子树 没有找到， 则向右子树 递归进行后续遍历查找
        if (this.right != null) {
            resultNode = this.right.postOrderSearch(no);
        }
        if (resultNode != null) {
            return resultNode;
        }
        System.out.println("进入后序查找");

        //如果左右子树 都没有找到， 就比较当前节点是不是
        if (this.no == no) {
            return this;
        }
        return null;
    }
}



// 定义 ThreadedBinaryTree 二叉树,实现了线索化功能的二叉树。
class ThreadedBinaryTree{
    @Setter
    private HeroNode root;

    // 为了实现线索化，需要创建 要给指向当前节点的前驱节点的指针。
    //在递归进行线索化时，pre总是保留前一个节点。
    private HeroNode pre = null;

    /**
     * 重载 threadedNodes 方法
     */
    public void threadedNodes( ) {
        this.threadedNodes(root);
    }
        /**
         对 二叉树 进行中序线索化的方法
         * @param node 当前需要线索化的节点
         */
    public void threadedNodes(HeroNode node){
        // 如果 Node == null, 不能线索化
        if(node == null){
            return;
        }
        //(1) 先线索化 左子树
        threadedNodes(node.getLeft());
        //(2) 线索化当前节点[有难度]
            // 处理当前节点的前驱节点
                // 以节点8 来理解， 节点8 的.left = null, 节点8的leftType=1
            if(node.getLeft()==null){
                //让当前节点的左指针 指向 前驱节点
                node.setLeft(pre);
                // 修改当前节点的 做指针的类型，指向前驱节点
                node.setLeftType(1);
            }
            // 处理后继节点，有点不太好理解。
            if(pre!=null &&pre.getRight() == null){
                // 让前驱节点的右指针，指向当前节点。
                pre.setRight(node);
                // 修改前驱节点的右指针类型
                pre.setRightType(1);
            }
            //!!! 每处理一个节点后， 让当前节点是下一个节点的前驱节点
            pre = node;
        //(3) 再线索化 右子树
        threadedNodes(node.getRight());

    }

    public void threadedList(){
        //定义一个变量，存储当前遍历的节点，从root开始
        HeroNode node = root;
        while (node !=null){
            // 循环的找到 leftType ==1 的节点， 第一个找到的就是 节点8
            //后面随着遍历而变化，因为当leftType==1 时,说明该节点是按照线索化
            // 处理后的有效节点
            while(node.getLeftType()==0){
                node = node.getLeft();
            }
            //打印当前这个节点
            System.out.println(node);
            // 如果当前节点的右指针，指向的是后继节点，就一直输出
            while(node.getRightType() ==1){
                //  获取到 当前节点的后继节点
                node  = node.getRight();
                System.out.println(node);
            }
            //替换 这个遍历的节点
            node = node.getRight();
        }
    }
    //删除节点
    public void delNode(int no ){
        if(root != null){
            // 如果只有一个root节点，这里立即判断root 是不是 就是要删除的节点。
            if(root.getNo() == no){
                root = null;
            }else {
                // 递归删除
                root.delNode(no);
            }
        }else{
            System.out.println("空树，不能删除。 ");
        }
    }

    //前序遍历
    public void preOrder(){
        if(this.root != null){
            this.root.preOrder();
        }else{
            System.out.println(" 二叉树 为空，无法执行 前序遍历");
        }
    }

    // 中序遍历
    public void infixOrder(){
        if(this.root!=null){
            this.root.infixOrder();
        }else{
            System.out.println(" 二叉树 为空，无法执行 中序遍历");
        }
    }

    // 后序遍历
    public void postOrder(){
        if(this.root!=null){
            this.root.postOrder();
        }else{
            System.out.println(" 二叉树 为空，无法执行 后序遍历");
        }
    }


    //前序查找
    public HeroNode preOrderSearch(int no ){
        if(root !=null){
            return root.preOrderSearch(no);
        }
        return null;
    }
    //中序查找
    public HeroNode infixOrderSearch(int no ){
        if(root !=null){
            return root.infixOrderSearch(no);
        }
        return null;
    }
    //后序查找
    public HeroNode postOrderSearch(int no ){
        if(root !=null){
            return root.postOrderSearch(no);
        }
        return null;
    }
}
