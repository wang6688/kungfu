package com.wwa.study.dataStrcture.tree;

/**
 * 顺序存储二叉树的概念：
 * 基本说明
 * 从数据存储来看，数组存储方式和树的存储方式可以互相转换，即数组可以转换成树，树也可以转换成数组，看右面的示意图。
 *                 1
 *             2       3
 *         4     5  6     7
 * 要求：
 * 1) 上图的二叉树的节点，要求以数组的方式来存放 arr[1,2,3,4,5,6,7]
 * 2) 要求在遍历数组arr 时，仍然可以以前序遍历，中序遍历 和后序遍历的方式 完成节点的遍历
 * arr数组的前序遍历结果 为1,2,4,5,3,6,7
 *
 *
 * 顺序存储二叉树的特点：
 * 1）顺序二叉树通常只考虑完全二叉树
 * 2）第n个元素的左节点为 2* n+1
 * 3) 第n个元素的右子节点为 2* n+2
 * 4) 第n个元素的父节点为(n-1) /2
 * 5) n: 表示二叉树中的第几个元素（按0开始编号 如图所示【1，2，3，4，5，6，7】
 * @date Created in 2021/6/21
 * @modified By：
 */
public class ArrBinaryTreeDemo {

    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5,6,7};
        // 创建一个 ArrBinaryTree
        ArrBinaryTree arrBinaryTree = new ArrBinaryTree(arr);
        arrBinaryTree.preOrder();    // 1,2,4,5,3,6,7



        //todo 练习： 完成堆数组以 二叉树中序、后序遍历方式的代码
    }
}
// 编写一个ArraryBinaryTree，实现顺序存储二叉树遍历
class ArrBinaryTree{
    private int[] arr; // 存储数据节点的数组
    public ArrBinaryTree(int [] arr){
        this.arr = arr;
    }

    public void preOrder(int index){
        // 如果数组为空， 或者 arr.length = 0
        if(arr == null || arr.length == 0){
            System.out.println("数组为空， 不能按照二叉树的前序遍历");
        }
        // 输出当前这个元素
        System.out.println(arr[index]);
        // 向左递归遍历
        if((index * 2 +1) <arr.length) {
            preOrder(2*index + 1);
        }
        // 向右递归遍历
        if((index *2 +2) <arr.length){
            preOrder(2 * index +2);
        }
    }
    //重载preOder
    public  void preOrder(){
        preOrder(0);
    }

}


