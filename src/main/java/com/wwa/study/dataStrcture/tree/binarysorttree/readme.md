
**二叉排序树**
介绍：BST(Binary Sort(Search) Tree),对于二叉排序树的任何一个非叶子节点，要求左子节点的值比当前节点的值小，右子节点的值比当前节点的值大。
特别说明：如果有相同的值，可以将该节点放在左子节点或右子节点。
比如针对前面的数据（7,3,10,12,5,1,9),对应的二叉排序树为：
![](.readme_images/6c698797.png) 

**二叉排序树的创建和遍历**
一个数组创建 成对应的二叉排序树，并使用中序遍历二叉排序树，比如：数组为Array(7,3,10,12,5,1,9),创建成对应的二叉排序树为：
![](.readme_images/ec847eaa.png)

**二叉排序的删除**

二叉排序的删除 情况比较复杂，有下面3种情况需要考虑。
1) 删除叶子节点（比如：2，5，9，12）
思路： 
① 要先找到待删除的节点 targetNode
② 找到 targetNode 的父节点parent
③ 确定targetNode 是 parent的左子节点 还是右子节点
④ 根据前面的情况来对应删除
左子节点 parent.left = null;
右子节点 parent.right = null; 

2) 删除只有一颗子树的节点（比如：1）
思路：
① 要先找到待删除的节点 targetNode
② 找到 targetNode 的父节点parent
③ 确定targetNode的子节点 是左还是右?
④ targetNode 是 parent的左子节点还是右子节点。
⑤ 如果targetNode 有左子节点
    ⑤-1 如果targetNode 是 parent的左子节点，则 parent.left = targetNode.left;
    ⑤-2 如果 targetNode 是 parent 的右子节点，则 parent.right = targetNode.left;
⑥ 如果targetNode 有 右子节点。
    ⑥-1 如果 targetNode 是 parent 的左字节，则parent.left = targetNode.right;
    ⑥-2 如果 targetNode 是 parent 的右子节点 ，则 parent.right  = targetNode.right;
3) 删除有两颗子树的节点.(比如：7，3，10)
思路：
① 要先找到待删除的节点 targetNode
② 找到targetNode的父节点 parent
③ 从targetNode 的右(左)子树找到最小(大)的节点
④ 用一个临时变量，将最小(大)节点的值保存 temp =11
⑤ 删除该最小节点
⑥ targetNode.value =  temp;