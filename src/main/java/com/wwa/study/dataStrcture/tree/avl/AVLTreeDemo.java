package com.wwa.study.dataStrcture.tree.avl;

/**

 */
public class AVLTreeDemo {
    public static void main(String[] args) {
//        int [] arr = {4,3,6,5,7,8};  // 需要执行 左旋转的数组
//        int [] arr = {10,12,8,9,7,6};  // 需要执行 右旋转的数组
        int[] arr = {10,11,7,6,8,9};
        // 创建一个 AVLTree 对象
          AVLTree avlTree = new AVLTree();
          // 添加节点
        for (int i = 0; i<arr.length;i++){
            avlTree.add(new Node(arr[i]));
        }
        //遍历
        System.out.println("中序遍历");
        avlTree.infixOrder();

//        System.out.println("在没有平衡处理前~~~");
          System.out.println("经过平衡处理后~~~");

        System.out.println("树的高度="+avlTree.getRoot().height()+"\t~~其左子树高度为:"+avlTree.getRoot().leftHeight()+"\t~~其右子树高度为:"+avlTree.getRoot().rightHeight());  //4~1~3
        System.out.println("当前根节点的值是:"+avlTree.getRoot());
        System.out.println(" 根节点的左子节点是："+avlTree.getRoot().left+"~~根节点的右子节点是："+avlTree.getRoot().right);
    }
}


// 创建 二叉平衡树
class AVLTree{
    private Node root;



    public Node getRoot() {
        return root;
    }

    // 查找 要删除的 节点
    public Node search(int value){
        if(root == null){
            return null;
        }else {
            return root.search(value);
        }
    }
    // 查找父节点
    public Node searchParent(int value){
        if (root == null){
            return null;
        }else{
            return root.searchParent(value);
        }
    }

    /** 1. 返回 以node 为根节点的二叉排序树的 最小节点的值
     * 2.删除 node 为根节点的二叉排序树的最小节点
     * @param node 传入的节点（当作二叉排序树的根节点）
     * @return 返回的以node 为根节点的二叉排序树的 最小节点的值
     */
    public int delRightTreeMin(Node node){
        Node target = node;
        // 循环的查找左节点，就会找到最小值
        while(target.left !=null){
            target = target.left;
        }
        // 这时候 target 就指向了最小节点
        // 删除最小节点
        delNode(target.value);
        return target.value;
    }
    //删除节点
    public void delNode(int value){
        if(root == null){
            return;
        }else{
            //1. 需要先去找到要删除的节点 targetNode
            Node targetNode = search(value);
            //如果没有找到要删除的节点
            if(targetNode == null){
                return ;
            }
            // 如果我们发现当前这颗二叉排序树 只有一个节点
            if(root.left == null && root.right == null){
                root = null;
                return ;
            }
            // 去找到targetNode的父节点
            Node parent = searchParent(value);
            // 如果要删除的节点是 叶子节点
            if(targetNode.left == null && targetNode.right == null){
                // 判断targetNode  是父节点的左子节点，还是右子节点
                if(parent.left!=null && parent.left.value == value){  // 是左子节点
                    parent.left = null;
                }else if(parent.right != null && parent.right.value == value ){ // 是右子节点
                    parent.right = null;
                }
            }else if(targetNode.left != null && targetNode.right !=null){  // 删除有两颗子树的节点
                int minVal = delRightTreeMin(targetNode.right);
                targetNode.value = minVal;

                //todo 作业： 从targetNode的 左子树找到最大的节点，然后按照前面的思路完成。
            }else{// 删除只有一颗子树的节点
                // 如果要删除的节点  有左子节点
                if(targetNode.left !=null){
                    if( parent== null){   // 在删除一颗子树时，要考虑它的父节点为空的情况
                        root = targetNode.left;
                        return;
                    }
                    // 如果targetNode 是 parent的左子节点
                    if(parent.left.value == value){
                        parent.left = targetNode.left;
                    }else{  // targetNode  是parent 的右子节点
                        parent.right = targetNode.left;
                    }
                }else{  // 如果要删除的节点有 右子节点
                    // 如果 targetNode 是 parent 的左子节点


                    if( parent== null){   // 在删除一颗子树时，要考虑它的父节点为空的情况
                        root = targetNode.left;
                        return;
                    }
                    if(parent.left.value == value){
                        parent.left = targetNode.right;
                    }else{ // 如果 targetNode 是 parent的右子节点
                        parent.right = targetNode.right;

                    }
                }
            }
        }
    }

    // 添加节点的方法
    public void add(Node node){
        if(root == null){
            root = node;  // 如果root为空，则直接让root指向node
        }else{
            root.add(node);
        }
    }

    //中序遍历
    public void infixOrder(){
        if(root !=null){
            root.infexOrder();
        }else{
            System.out.println("二叉排序树为空，不能遍历。");
        }
    }
}



// 创建Node 节点
class Node{
    int value;
    Node left;
    Node right;
    public Node(int value){
        this.value = value;
    }

    /**
     * @return 返回左子树的高度
     */
    public int leftHeight(){
        if(left == null){
            return 0;
        }
        return left.height();
    }

    public int rightHeight(){
        if(right == null){
            return 0;
        }
        return right.height();
    }
    /**
     * 求得左子树/右子树的高度最后+1 是因为本身的节点也要算一层
     * @return 返回当前节点的高度，以该节点为根节点的树的高度。
     */
    public int height(){
        return Math.max(left==null?0:left.height(),right==null?0:right.height())+1;
    }

    /**
     * 左旋转思路：
     * 1.创建一个新的节点 newNode(以根节点值4进行创建）
     * 2. 把新节点的左子树设置为当前节点的左子树   newNode.left = left
     * 3.把新节点的右子树设置为当前节点的右子树的左子树   newNode.right = right.left;
     * 4. 把当前节点的值替换成右子节点的值 value = right.value;
     * 5. 把当前节点的右子树设置成右子树的右子树   right = right.right;
     * 6. 把当前节点的左子树 设置为新节点  left = newNode;
     */
    public void leftRotate(){
        // 创建新的节点，以当前根节点的值
        Node newNode = new Node(value);
        //把新的节点左子树 设置成当前节点的左子树
        newNode.left  = this.left;
        // 把新节点的右子树 设置成带你过去节点的右子树的左子树
        newNode.right = this.right.left;
        // 把当前节点的值 替换成 右子节点的值
        this.value = this.right.value;
        // 把 当前节点的右子树 设置成当前节点 右子树的右子树
        this.right = this.right.right;
        // 把当前节点的左子树（左子节点）设置成新的节点。
        this.left = newNode;
    }

    /**
     * 右旋转，怎么处理--进行右旋转。（就是 降低左子树的高度），这里是将9这个节点，通过右旋转，得到右子树。
     * 思路：
     * 1. 创建一个新的节点 newNode (以根节点10 这个值 进行创建)
     *2. 把新节点的右子树 设置了当前节点的右子树  newNode.right = right;
     * 3. 把新节点的左子树设置为当前节点的左子树的右子树   newNode.left = left.right;
     * 4.把当前节点的值 换为 左子节点的值   value = left.value;
     * 5. 把当前节点的左子树  设置成左子树的左子树  left = left.left;
     * 6. 把当前节点的右子树 设置为新节点。  right = newLeft
     */
    private void rightRotate(){
        Node newNode = new Node(value);
        newNode.right = this.right;
        newNode.left = this.left.right;
        this.value = this.left.value;
        this.left = this.left.left;
        this.right = newNode;
    }

    /**
     * @param value 希望删除的节点的值
     * @return 如果找到返回该节点，否则返回null
     */
    // 查找要删除的节点
    public Node search(int value){
        if(value == this.value){  // 找到就是该节点
            return this;
        }else if(value <this.value){  // 如果查找的值 小于当前节点，向左子树递归查找
            // 如果左子节点为空，则跳过
            if(this.left==null){
                return  null;
            }
            return this.left.search(value);
        }else{  // 如果查找的值不小于 当前节点，向右子树递归查找
            if(this.right == null){
                return null;
            }
            return this.right.search(value);
        }
    }
    // 查找要删除 节点的父节点
    public Node searchParent(int value){
        // 如果当前节点就是要删除的节点的父界定啊，就返回
        if((this.left!=null && this.left.value==value) || (this.right !=null && this.right.value == value)){
            return this;
        }else{
            // 如果查找的值小于 当前节点的值，并且当前节点的左子节点 不为空。
            if(value < this.value && this.left!=null){
                return this.left.searchParent(value);// 向左子树递归查找
            }else if(value>=this.value && this.right!=null){
                return this.right.searchParent(value);  // 向右子树递归查找
            }else {
                return null;   // 没有找到父节点
            }
        }
    }
    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }

    // 添加节点的方法,以递归的形式添加节点，注意需要满足二叉排序树的要求
    public void add(Node node){
        if(node == null){
            return ;
        }
        //判断传入的节点的值，和当前子树的根节点的值关系
        if(node.value <this.value){
            // 如果当前节点的 左子节点为null
            if(this.left == null){
                this.left = node;
            }else{
                // 递归的向左子树添加
                this.left.add(node);
            }
        }else{
            //添加的节点的值 > 当前节点的值
            if(this.right == null){
                this.right = node;
            }else{
                // 递归的向右子树添加。
                this.right.add(node);
            }
        }

        // 当添加完一个节点后，如果： （"右"子树的高度- "左"子树的高度)  >1 ，左旋转
        if(rightHeight() - leftHeight() >1){
            // 如果它的右子树的左子树的高度 大于它的右子树的右子树的高度
            if(right != null && right.leftHeight() > right.rightHeight()){
                // 先对右子节点 进行右旋转
                right.rightRotate();
                // 然后在对当前节点 进行左旋转
                leftRotate(); // 左旋转...
            }else{
                // 直接进行左旋转即可。
                leftRotate();  // 左旋转
            }
            return;  // 必须要！！！
        }
        // 当添加完一个节点后，如果（"左"子树的高度- "右"子树的高度）>1 ,右旋转
        if(leftHeight() -rightHeight() >1){
            // 如果它的左子树右子树高度大于它的左子树的高度
            if(left != null && left.rightHeight()>left.leftHeight()){
                // 先对当前节点的左节点（左子树） -> 左旋转
                left.leftRotate();
                // 在对当前节点 进行右旋转
                rightRotate();
            }else{
                //直接进行右旋转即可。
                rightRotate();
            }
        }
    }

    //中序遍历
    public void infexOrder(){
        if(this.left!=null){
            this.left.infexOrder();
        }
        System.out.println(this);
        if(this.right!=null){
            this.right.infexOrder();
        }
    }

}
