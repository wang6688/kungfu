package com.wwa.study.dataStrcture.tree;

import lombok.Getter;
import lombok.Setter;

/**
 * 二叉树的 前序，中序，和后序遍历
  * @date Created in 2021/6/18
 * @modified By：
 */
public class BinaryTreeDemo {
    public static void main(String[] args) {
        /**  二叉树结构图如下
        1：宋江
  2：吴用       3：卢俊义
                        4林冲      */

        //创建二叉树
        BinaryTree binaryTree = new BinaryTree();
        HeroNode root = new HeroNode(1, "宋将");
        HeroNode wuyong2 = new HeroNode(2, "吴用");
        HeroNode lujunyi3 = new HeroNode(3, "卢俊义");
        HeroNode linchong4 = new HeroNode(4, "林冲");

        // 先手动创建二叉树，后面学习使用 递归的方式 创建二叉树
        root.setLeft(wuyong2);
        root.setRight(lujunyi3);
        lujunyi3.setRight(linchong4);
        binaryTree.setRoot(root);
        //测试前序遍历
        System.out.println("前序遍历的");  //1宋将,2吴用,3卢俊义,4林冲"
        binaryTree.preOrder();
        //测试中序遍历
        System.out.println("中序遍历的");  //2吴用,1宋将,,3卢俊义,4林冲"
        binaryTree.infixOrder();
        //测试后序遍历
        System.out.println("后序遍历的");  //2吴用,4林冲,3卢俊义,1宋将"
        binaryTree.postOrder();

        //若再加一个叶子节点【5号关胜】 到 3号子节点卢均以的 左子节点，
        System.out.println("再加一个叶子节点【5号关胜】 到 3号子节点卢均以的 左子节点");
        HeroNode guansheng5 = new HeroNode(5, "关胜");
        lujunyi3.setLeft(guansheng5);
        System.out.println("则其前序遍历的");  //1宋将,2吴用,3卢俊义,5关胜，4林冲"
        binaryTree.preOrder();
        //测试中序遍历
        System.out.println("则其中序遍历的");  //2吴用,1宋将,5关胜,3卢俊义,4林冲"
        binaryTree.infixOrder();
        //测试后序遍历
        System.out.println("则其后序遍历的");  //2吴用,5关胜，4林冲,3卢俊义,1宋将"
        binaryTree.postOrder();

        int toFindNo = 5;
        //测试前序查找
        System.out.println("前序遍历查找方式~~~~~~");   //4次能找到
        HeroNode resNode = binaryTree.preOrderSearch(toFindNo);
        if(resNode!=null){
            System.out.printf("找到了，信息为no=%d name = %s",resNode.getNo(),resNode.getName());
        }else{
            System.out.printf("没有找到no=%d的英雄",toFindNo);
        }

        System.out.println("\n中序遍历查找方式~~~~~~");
//        toFindNo=15;
          resNode = binaryTree.infixOrderSearch(toFindNo);  //3次能找到
        if(resNode!=null){
            System.out.printf("找到了，信息为no=%d name = %s",resNode.getNo(),resNode.getName());
        }else{
            System.out.printf("没有找到no=%d的英雄",toFindNo);
        }

        System.out.println("\n后序遍历查找方式~~~~~~");
//        toFindNo=15;
        resNode = binaryTree.postOrderSearch(toFindNo);  //2次能找到
        if(resNode!=null){
            System.out.printf("找到了，信息为no=%d name = %s",resNode.getNo(),resNode.getName());
        }else{
            System.out.printf("没有找到no=%d的英雄",toFindNo);
        }

        //测试删除节点
        System.out.println("删除前，前序遍历情况：");
        binaryTree.preOrder();  //1,2,3,5,4
        binaryTree.delNode(5);
//        binaryTree.delNode(3);

        System.out.println("删除后，前序遍历情况：");
        binaryTree.preOrder();  //1,2,3,4


      /* todo //思考题（课后练习）
        1) 如果要删除的节点是 非叶子节点，现在我们不希望将该非叶子节点为根节点 的子树删除，需要指定规则，假如规定如下：
        2） 如果该非叶子节点A 只有一个子节点B，则子节点B替代节点A。
        3）如果该非叶子节点A 右左子节点B 和右子节点C，则让左子节点B 替代节点A。
        4） 请思考，如何完成 该删除功能。



        */

    }

}
// 定义 BinaryTree 二叉树
class BinaryTree{
    @Setter
    private HeroNode root;

    //删除节点
    public void delNode(int no ){
        if(root != null){
            // 如果只有一个root节点，这里立即判断root 是不是 就是要删除的节点。
            if(root.getNo() == no){
                root = null;
            }else {
                // 递归删除
                root.delNode(no);
            }
        }else{
            System.out.println("空树，不能删除。 ");
        }
    }

    //前序遍历
    public void preOrder(){
        if(this.root != null){
            this.root.preOrder();
        }else{
            System.out.println(" 二叉树 为空，无法执行 前序遍历");
        }
    }

    // 中序遍历
    public void infixOrder(){
        if(this.root!=null){
            this.root.infixOrder();
        }else{
            System.out.println(" 二叉树 为空，无法执行 中序遍历");
        }
    }

    // 后序遍历
    public void postOrder(){
        if(this.root!=null){
            this.root.postOrder();
        }else{
            System.out.println(" 二叉树 为空，无法执行 后序遍历");
        }
    }


    //前序查找
    public HeroNode preOrderSearch(int no ){
        if(root !=null){
            return root.preOrderSearch(no);
        }
        return null;
    }
    //中序查找
    public HeroNode infixOrderSearch(int no ){
        if(root !=null){
            return root.infixOrderSearch(no);
        }
        return null;
    }
    //后序查找
    public HeroNode postOrderSearch(int no ){
        if(root !=null){
            return root.postOrderSearch(no);
        }
        return null;
    }
}

//创建 HeroNode 节点
@Getter
@Setter
class HeroNode{
    private int no;
    private String name;
    private HeroNode left; // 默认null
    private HeroNode right;  // 默认null

    public HeroNode(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                '}';
    }

    /** 递归删除节点
     * 1.如果删除的节点是叶子节点，则删除该节点
     * 2.如果删除的节点是 非叶子节点，则删除该子树
     * @param no
     */
    public void delNode(int no ){
    /* 思路：
首先先处理：考虑如果树是空树root，如果只有一个root节点，则等价将二叉树置空。
然后进行下面步骤：
1.因为我们的二叉树是单向的，所以我们是判断当前节点的子节点是否需要删除节点，而不能去判断当前这个节点是不是需要删除节点。
2.如果当前节点的左子节点不为空，并且左子节点就是要删除节点，就将this.left=null; 并且就返回(结束递归删除)
3.如果当前节点的右子节点不为空，并且右子节点就是要删除节点，就讲 this.right=null; 并且 就返回(结束递归删除)
4.如果第2和第3步 没有删除节点，那么我们就需要向左子树进行递归删除。
5.如果第4步也没有删除节点，则应当向右子树进行递归删除。*/

        //2.如果当前节点的左子节点不为空，并且左子节点就是要删除节点，就将this.left=null; 并且就返回(结束递归删除)
        if(this.left!=null && this.left.no == no){
            this.left = null;
            return ;
        }
        //3.如果当前节点的右子节点不为空，并且右子节点就是要删除节点，就讲 this.right=null; 并且 就返回(结束递归删除)
        if(this.right!=null && this.right.no == no ){
            this.right = null;
            return ;
        }
    //   4. 我们就需要向左子树进行递归删除
        if(this.left!=null){
            this.left.delNode(no);
        }
        //5.则应当向右子树进行递归删除
        if(this.right!=null){
            this.right.delNode(no);
        }

    }

    // 编写前序遍历的方法：先输出 (根节点/有叶子节点的子节点),再输出对应(根节点/有子节点)的 左测叶子节点，最后输出右侧的叶子节点
    public void preOrder(){
        //先输出父节点
        System.out.println(this);
        // 递归 向 左子树 前序遍历
        if(this.left!=null){
            this.left.preOrder();
        }
        // 递归 向 右子树 前序遍历
        if(this.right!=null){
            this.right.preOrder();
        }
    }
    // 中序(父节点在中间)遍历的方法：先输出左侧的叶子节点，再输出对应叶子节点的所属上级“子节点/根节点”, 最后输出右侧的叶子节点
    public void infixOrder(){
        //递归向 左子树遍历
        if(this.left!=null){
            this.left.infixOrder();
        }
        //输出父节点
        System.out.println(this);
        //递归向右子树遍历
        if(this.right!=null){
            this.right.infixOrder();
        }
    }
    // 后序遍历的方法: 先输出左侧的叶子节点，再输出右侧 的叶子节点，最后再输出对应的叶子节点的所属上级“子节点” 直到最终的根节点
    public void postOrder(){
        //递归向 左子树 后序遍历
        if(this.left!=null) {
            this.left.postOrder();
        }
        //递归向 右子树 后序遍历
        if(this.right!=null){
            this.right.postOrder();
        }
        //输出父节点

        System.out.println(this);
    }

    /**
     *   前序查找思路： 1 先判断当前节点的no 是否等于 要查找的
     *         2 如果是相等，则返回当前节点
     *         3. 如果不等，则判断当前节点的左子节点是否为空，如果不为空，则递归前序查找。
     * @param no  查找的编号
     * @return 如果找到就返回 该Node，如果没有找到 返回null
     */
    public HeroNode preOrderSearch(int no ){
        System.out.println("进入前序遍历");

        // 比较当前(父节点/有叶子节点的子节点)节点 是不是 要查找的节点
        if(this.no == no){
            return this;
        }
        //1.判断 当前节点的左子节点是否为空，如果不为空，则递归前序查找
        //2. 如果 左递归前序查找，找到节点，则返回
        HeroNode resultNode = null;
        if(this.left!=null){
            resultNode = this.left.preOrderSearch(no);

        }
        if(resultNode!=null){
            // 说明在 左子树内找到
            return resultNode;
        }
        //1. 左递归前序查找，找到节点，则返回，否则继续判断，
        //2. 当前的节点的右子节点但是否为空，如果不空，则继续向右递归前序查找
        if(this.right!=null){
            resultNode = this.right.preOrderSearch(no);
        }
        return resultNode;
    }


    /**
     * 中序查找思路：1.判断当前节点的左子节点是否为空，如果不为空，则递归中序查找
     *                 2. 如果找到，则返回，如果没有找到，就和当前节点比较，如果是则返回当前节点，否则继续进行右递归的中序查找
     *                 3.如果右递归 中序查找，找到就返回，否则返回null。
     *
     * @param no
     * @return
     */
    // 中序遍历查找
    public HeroNode infixOrderSearch(int no){
        // 判断当前节点的的左子节点那是否为空， 如果不为空， 则递归中序查找。
        HeroNode resultNode = null;
        if(this.left !=null){
            resultNode = this.left.infixOrderSearch(no);
        }
        if(resultNode!=null){
            return resultNode;
        }
        System.out.println("进入中序查找");
        //如果 找到，则返回，如果没有找到，就和当前节点比较，如果是 则返回当前节点
        if(this.no  == no){
            return this;
        }
        // 否则继续进行右递归的中序查找
        if(this.right !=null){
            resultNode = this.right.infixOrderSearch(no);
        }
        return resultNode;
    }

    /**
     * 后序查找思路： 1. 判断当前节点的左子节点是否为空，如果不为空，则递归后续查找
     *                  2. 如果找到，就返回，如果没有找到，就判断当前节点的右子节点是否为空，如果不为空，则右递归进行后序查找，如果找到，就返回。
     *                  3. 就和当前节点进行，比如， 如果是则返回，否则返回null。
     * @param no
     * @return
     */
    //后序遍历查找
    public HeroNode postOrderSearch(int no ){
//        判断当前节点的左子节点是否为空，如果不为空，则递归后续查找
        HeroNode resultNode = null;
        if(this.left !=null){
            resultNode = this.left.postOrderSearch(no);
        }
        if(resultNode!=null){  //说明在左子树找到
            return resultNode;
        }
        // 如果左子树 没有找到， 则向右子树 递归进行后续遍历查找
        if(this.right !=null){
            resultNode = this.right.postOrderSearch(no);
        }
        if(resultNode!=null){
            return resultNode;
        }
        System.out.println("进入后序查找");

        //如果左右子树 都没有找到， 就比较当前节点是不是
        if(this.no  == no){
            return this;
        }
        return null;
    }
}