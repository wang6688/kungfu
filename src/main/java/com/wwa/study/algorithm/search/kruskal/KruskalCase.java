package com.wwa.study.algorithm.search.kruskal;

import cn.hutool.core.util.StrUtil;
import com.sun.javafx.geom.Edge;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.annotation.Resource;
import java.util.Arrays;

/**
  克鲁斯卡尔算法
 * @date Created in 2021/7/7

 */
public class KruskalCase {
    private  int edgeNum;   // 边的个数
    private char[] vertexs;     // 顶点数组
    private int[][] matrix;     //邻接矩阵

    // 使用INF 表示两个顶点不能连通
    private static final int INF = Integer.MAX_VALUE;

    public static void main(String[] args) {
        char[] vertexs = {'A','B','C','D','E','F','G'};
        // 克鲁斯卡尔算法的邻接矩阵
        int matrix[][] = {
                /*A* /*B* /*C* /*D* /*E* /*F*/ /*G*/
         /*A*/   {0, 12, INF, INF, INF,16,14},
          /*B*/  {12,0,10,INF,INF,7,INF},
          /*C*/  {INF,10,0,3,5,6,INF},
          /*D*/  {INF,INF,3,0,4,INF,INF},
          /*E*/  {INF,INF,5,4,0,2,8},
          /*F*/  {16,7,6,INF,2,0,9},
          /*G*/  {14, INF, INF, INF, 8, 9, 0}
        };

        // 创建KruskalCase 对象实例
        KruskalCase kruskalCase = new KruskalCase(vertexs,matrix);
        // 输出构建的
        kruskalCase.print();
        kruskalCase.kruskal();
//        EdgeData[] edges = kruskalCase.getEdges();
//        System.out.println("排序前："+Arrays.toString(edges));
//        kruskalCase.sortEdges(edges);
//        System.out.println("排序后："+Arrays.toString(edges));
    }
    // 构造器
    public KruskalCase(char [] vertexs,int [][] matrix){
        // 初始化 顶点树 和边的个数
        int vlen = vertexs.length;

        // 初始化顶点，复制拷贝的方式
        this.vertexs = new char[vlen];

        for (int i = 0; i < vertexs.length; i++){
            this.vertexs[i] = vertexs[i];

        }
        // 初始化边 ,使用的是复制拷贝的方式
        this.matrix = new int[vlen][vlen];
        for (int i = 0; i < vlen; i++) {
            for (int j = 0; j < vlen; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }

        // 统计边 的数量
        for (int i = 0; i < vlen; i++) {
            for (int j = 1; j < vlen; j++) {  // 这里初始化j=1 ，意为 自己跟自己就连的那条不存在的边就不统计了。
                if(this.matrix[i][j] != INF){
                    edgeNum++;
                }
            }
        }

    }

    // 打印邻接矩阵
    public void print(){
        System.out.println("邻接矩阵为:\n");
        System.out.printf("   %20s    \n", StrUtil.join(",\t",vertexs));

        for (int i = 0; i < vertexs.length; i++) {
            for (int j = 0; j < vertexs.length; j++) {
                System.out.printf("%12d\t",matrix[i][j]);
            }
            System.out.println();  // 换行处理
        }
    }

    public void kruskal(){
        int index = 0; // 表示最后结果数组的索引
        int[] ends = new int[edgeNum];  // 用于保存“已有最小生成树” 中每个顶点在最小生成树中的终点。
        // 创建结果数组，保存最后的最小生成树。
        EdgeData[] rets = new EdgeData[edgeNum];
        // 获取图 中 所有的 边的集合，一共由12条边
        EdgeData [] edges = getEdges();
        System.out.println("图的边的集合="+Arrays.toString(edges)+"共"+edges.length);  //12
        // 按照边的权值大小进行排序（从小到大）
        sortEdges(edges);

        // 遍历edges 数组，将边添加到 最小生成树中时， 判断是准备加入的边是否形成了回路，如果没有，就加入 rets,否则不能加入
        for (int i = 0; i < edgeNum; i++) {
            // 获取到第i条边的 第一个顶点(起点）
            int p1 = getPostion(edges[i].getStart());
            //  获取 到第i 条边的 第2个顶点
            int p2 = getPostion(edges[i].getEnd());
            // 获取p1  这个顶点在已有最小生成树中的终点
            int m = getEnd(ends,p1);
            // 获取 p2 这个顶点在已有最小生成树中的终点。
            int n = getEnd(ends,p2);
            // 是否构成回路
            if(m!=n ){  // 没有构成回路
                ends[m] = n;   // 设置 m 在“已有最小生成树”中的终点
                rets[index++] = edges[i]; //  有一条边加入到rets 数组
            }
        }
        // 统计并打印最小生成树 <E,F> <C,D> <D,E> <B,F> <E,G> <A,B>
        System.out.println("最小生成树为=" );
        for (int i = 0; i < index; i++) {
            System.out.println(rets[i]);
        }
    }

    /**
     * 功能： 对边进行排序，冒泡排序
     * @param edges 边的集合
     */
    private void sortEdges(EdgeData[] edges){
        for (int i = 0; i < edges.length - 1; i++) {
            for (int j = 0; j < edges.length - 1 - i; j++) {

                //fixme NullPointerException
                if(edges[j].getWeight()> edges[j+1].getWeight()){ // 交换
                    EdgeData tmp = edges[j];
                    edges[j] = edges[j+1];
                    edges[j+1] = tmp;

                }
            }
        }
    }

    /**
     * @param ch 顶点的值，如'A',‘B’
     * @return 返回ch顶点对应的下标，如果找不到，返回-1
     */
    private int getPostion(char ch){
        for (int i = 0; i < vertexs.length; i++) {
            if(vertexs[i] == ch){  //找到
                return i;
            }
        }
        //找不到
        return -1;
    }

    /**
     * 功能： 获取图中边，放到 EdgeData[] 数组中，后面需要遍历该数组
     * 是通过matrix 邻接矩阵来 获取
     *  EdgeData[['A','B',12],['B','F',7],.....] 形式
     * @return
     */
    private EdgeData[] getEdges(){
        int index = 0;
        EdgeData[] edges = new EdgeData[edgeNum];
        for (int i = 0; i < vertexs.length; i++) {
            for (int j = i+1; j < vertexs.length; j++) {
                if(matrix[i][j] != INF){
                    edges[index++] = new EdgeData(vertexs[i],vertexs[j],matrix[i][j]);
                }
            }
        }
        return edges;
    }

    /**
     * 这个方法很重要
     * 功能： 获取下标为 i的顶点的 终点（），用于后面判断 两个顶点的终点是否相同（避免形成回路）
     * @param ends : 数组就是记录了各个顶点对应的 终点是哪个，ends 数组是在遍历过程中，逐步形成。
     * @param i ： 表示传入的顶点对应的 下标
     * @return 返回的就是 下标为i 的这个顶点对应的 终点的下标
     */
    private int getEnd(int[] ends,int i ){
        while (ends[i] !=0){
            i = ends[i];
        }
        return i;
    }
}
// EdgeData 的对象实例表示一条边
@Data
@AllArgsConstructor
@ToString
class EdgeData{
    private char start;  // 边的一端
    private char end; // 边的令一端
    private int weight;     // 边的权值
}

