package com.wwa.study.algorithm.search.prim;

import cn.hutool.core.util.StrUtil;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Dictionary;
import java.util.concurrent.ForkJoinPool;

/**
 普利姆算法
 *
 */
public class PrimAlgorithm {
    public static void main(String[] args) {
        // 测试一下图是否创建ok
        char[]data = new char[]{'A','B','C','D','E','F','G'};
        int verxs = data.length;
        final int DISCONNECTTED = 9999;  // 用这个比较大的权值 来表示两个点不连通
        // 邻接矩阵的关系 使用二维数组表示
        int [][] weight = new int[][]{
                {DISCONNECTTED,5,7, DISCONNECTTED,DISCONNECTTED,DISCONNECTTED,2},//  A 行
                {5,DISCONNECTTED,DISCONNECTTED,9,DISCONNECTTED,DISCONNECTTED,3},
                {7,DISCONNECTTED,DISCONNECTTED,DISCONNECTTED,8,DISCONNECTTED,DISCONNECTTED},
                {DISCONNECTTED,9,DISCONNECTTED,DISCONNECTTED,DISCONNECTTED,4,DISCONNECTTED},
                {DISCONNECTTED,DISCONNECTTED,8,DISCONNECTTED,DISCONNECTTED,5,4},
                {DISCONNECTTED,DISCONNECTTED,DISCONNECTTED,4,5,DISCONNECTTED,6},
                {2,3,DISCONNECTTED,DISCONNECTTED,4,6,DISCONNECTTED}

        };

        // 创建 MGrpah 对象
        MGraph graph = new MGraph(verxs);
        // 创建一个 MinTree 对象
        MinTree minTree = new MinTree();
        minTree.createGraph(graph,verxs,data,weight);
        // 输出
        minTree.showGraph(graph);

        //测试 普利姆算法
        minTree.prim(graph,new String(data).indexOf("B"));
    }

}
// 创建最小生成树-> 村庄路线连通图
class MinTree{
    /**
     * 创建图的邻接矩阵
     * @param graph 图对象
     * @param verxs 图对应的顶点个数
     * @param data 图的各个顶点的值
     * @param weight 图的邻接矩阵
     */
    public void createGraph(MGraph graph,int verxs,char data[],int [][]weight){
        for (int i=0 ; i < verxs ; i++) {  // 顶点
            graph.data[i] = data[i];
            for (int j = 0; j < verxs; j++) {
                graph.weight[i][j] = weight[i][j];
            }
        }
    }

    /**
     * 展示邻接矩阵的图
     * @param graph
     */
    public void showGraph(MGraph graph){
        System.out.printf("   %s\n", StrUtil.join(",\t",graph.data));
        int idx = 0;
        for (int[] edge : graph.weight) {
            System.out.println(graph.data[idx++]+":"+Arrays.toString(edge));
        }
    }

    /**
     * prim 算法，得到最小生成树
     * @param graph 图
     * @param v 表示从图的 第几个顶点开始生成 'A'->0 ; 'B'->1 ...
     */
    public void prim(MGraph graph,int v){
        final int MAX_VALUE = 10000;
        // visited[] 标记节点（顶点） 是否被访问过
        int visited[] = new int[graph.verxs];
        // visited[] 默认初始化元素的值都是0，表示没有被访问过
        for (int i = 0; i < graph.verxs; i++) {
            visited[i] = 0;
        }
        // 把当前这个节点标记为已访问
        visited[v] = 1;
        // h1 和  h2 记录两个顶点的下标
        int h1 = -1;
        int h2= -1;
        // 顶点间连接线 的最短的边（权值最小）
        int minWeight = MAX_VALUE;  // 将minWeight 初始化成一个大数，后面在遍历过程中，会被替换。
        for (int edgeIndex = 1; edgeIndex < graph.verxs; edgeIndex++) {  // 因为有 graph.verxs 顶点，普利姆算法结束后，有 graph.verxs -1 条边
            // 这个是确定每一次生成的子图，和哪个节点距离最近
            for (int i = 0; i < graph.verxs; i++) {  // i 节点表示被访问过的节点
                for (int j = 0; j < graph.verxs; j++) {  // j节点表示还没有访问过的节点
                    if(visited[i] == 1 && visited[j] ==0 && graph.weight[i][j] < minWeight){
                        // 替换minWeight (寻找已经访问过的 节点和 未访问过的节点间的权值最小的边
                        minWeight = graph.weight[i][j];
                        h1 = i;
                        h2 = j;
                    }
                }

            }
            // 找到一条边的权值是最小的
            System.out.println("边<"+graph.data[h1]+","+graph.data[h2]+">  权值:"+minWeight);
            // 将当前这个节点标记为 已经访问呢
            visited[h2] =1 ;
            // minWeight 重新设置为 最大值 10000
            minWeight = MAX_VALUE;

        }


    }

}
class MGraph{
    int verxs ; // 表示图的 顶点个数
    char [] data ; // 存放节点数据
    int[][] weight; // 存放边，就是我们的邻接矩阵
    public MGraph(int verxs){
        this.verxs = verxs;
        data = new char[verxs];
        weight = new int [verxs][verxs];
    }

}
