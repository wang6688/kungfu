package com.wwa.study.algorithm.search.greedy;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
  贪心（贪婪）算法
 * @date Created in 2021/7/6
 * @modified By：
 */
public class GreedyAlgorithm {
    public static void main(String[] args) {
        //创建广播电台，并将各个电台放入到 broadcast2Cities
        Map<String, Set<String>> broadcast2Cities = new HashMap<String, Set<String>>(){{
             put("k1", Stream.of("北京", "上海", "天津").collect(Collectors.toSet()));
            put("k2", Stream.of("广州", "北京", "深圳").collect(Collectors.toSet()));
            put("k3", Stream.of("成都", "上海", "杭州").collect(Collectors.toSet()));
            put("k4", Stream.of( "上海", "天津").collect(Collectors.toSet()));
            put("k5", Stream.of("杭州", "大连").collect(Collectors.toSet()));
        }};

        // allAres 存放所有的地区
        Set<String> allAreas = Stream.of("北京", "上海", "天津", "广州", "深圳", "成都", "杭州", "大连").collect(Collectors.toSet());

        //创建ArrayList，存放选择的电台集合
        ArrayList<String> selects = new ArrayList<>();
        // 定义一个临时的集合，在遍历的过程中，存放遍历过程中的电台覆盖的地区和当前还没有覆盖的地区的交集
        HashSet<String> tempSet = new HashSet<>();

        // 定义一个maxKey，保存在依次遍历过程中，能够覆盖最大的地区对应的电台 的key
        // 如果maxKey  不为null， 则会加入到 selects
        String maxKey = null;
        while(allAreas.size() !=0){  // 如果allAreas 不为0 ，则表示还没有覆盖到所有的地区。
            //每 进行一次 while,需要
            maxKey = null;
            // 遍历 broadcast2cities, 取出对应的key
            for (String broadcastKey : broadcast2Cities.keySet()) {
                //每进行一次 for
                tempSet.clear();
                // 当前这个key 能够覆盖的地区
                Set<String> areas = broadcast2Cities.get(broadcastKey);
                tempSet.addAll(areas);
                // 求出 tempSet 和allAreas 集合的交集，交集会赋值给tempSet;
                tempSet.retainAll(allAreas);
                //如果当前这个集合包含的 未覆盖地区的数量，比maxKey 指向的集合地区还要多。
                // 就需要重置maxKey
                //tempSet.size()>broadcast2Cities.get(maxKey).size()) 体现出了贪心算法的特点，每次都选择最优的。
                if(tempSet.size()>0 && (maxKey==null || tempSet.size()>broadcast2Cities.get(maxKey).size())){
                    maxKey = broadcastKey;
                }
            }

            // maxKey != null ,就应该将 maxKey 加入 selects
            if(maxKey !=null){
                selects.add(maxKey);
                // 将maxKey 指向的广播电台 覆盖的地区，从allAreas 去掉
                allAreas.removeAll(broadcast2Cities.get(maxKey));
            }

        }
        System.out.println("得到的选择结果是："+selects);  //[k1,k2,k3,k5]
    }
}
