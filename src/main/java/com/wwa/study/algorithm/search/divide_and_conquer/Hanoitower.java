package com.wwa.study.algorithm.search.divide_and_conquer;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

/**
 *
 * @date Created in 2021/7/2
  */
public class Hanoitower {
    public static void main(String[] args) {
        hanoiTower(5,'A','B','C');
    }

    public static void hanoiTower(int num,char a ,char b ,char c ){
        // 如果只有一个盘
        if(num == 1){
            System.out.println("第1个盘从"+a+"->"+c);
            return;
        }else{
            // 如果我们有num>=2 的情况 ,我们总是可以看作是两个盘 ：1.最下边的一个盘 ；2 .上面的所有盘
            //1. 先把最上面的所有盘 A->B ,移动过程会使用到 c
            hanoiTower(num-1,a,c,b);
            //2. 把最下边的盘 A->c
            System.out.println("第"+num+"个盘从"+a+"->"+c);
            //3. 把B塔的所有盘从 B->C,移动过程使用到 a 塔
            hanoiTower(num-1,b,a,c);
        }
    }

}
