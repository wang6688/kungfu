package com.wwa.study.algorithm.search.binarysearch_non_recursion;

/**
 * 二分擦混找的 非递归实现
 * @date Created in 2021/7/2

 */
public class BinarySearchNonRecursion {

    public static void main(String[] args) {
        int arr[] = {1,3,8,10,11,67,100};
        int index = binarySearch(arr, 10);
        System.out.println("index = "+index);
    }

    /**
     * @param arr 待查找的数组，arr 是升序排序
     * @param target 需要查找的数
     * @return 返回对应 的下标，-1 表示没有找到
     */
    public static int binarySearch(int[] arr, int target){
        int left = 0;
        int right = arr.length-1;
        while(left <=right){  // 说明继续查找
            int mid = (left+right)/2;
            if(arr[mid] == target){
                return mid;
            }else if(arr[mid] >target){
                right = mid -1;  // 需要向左边查找
            }else{
                left = mid +1; // 需要向右边查找
            }

        }
        return -1;
    }
}
