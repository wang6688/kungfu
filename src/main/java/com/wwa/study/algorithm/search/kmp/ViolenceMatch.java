package com.wwa.study.algorithm.search.kmp;

/**
    暴力匹配算法
 * @date Created in 2021/7/5
 *
 */
public class ViolenceMatch {

    public static void main(String[] args) {
        String str1 = "硅硅谷  尚硅谷你尚硅 尚硅谷你尚硅谷你尚硅谷你好";
        String str2 = "尚硅谷你尚硅谷你";
        int index = voilenceMatch(str1, str2);
        System.out.println(index);
    }

    // 暴力匹配算法 实现
    public static int voilenceMatch(String originStr,String toFindTargetStr){
        char[] s1 = originStr.toCharArray();
        char[] s2 = toFindTargetStr.toCharArray();

        int s1Len = s1.length;
        int s2Len = s2.length;

        int i = 0; // i 索引指向s1
        int j = 0; // j 索引指向s2
        while(i <s1Len && j<s2Len){     // 保证匹配时，不越界
            if(s1[i] == s2[j]) {    // 匹配单字符ok
                i++;
                j++;
            }else{  //如果字符 没有匹配成功
                // 如果失配（即 str1[i]!= str2[j])，令 i=i-(j-1),j = 0。
                i = i-(j-1);
                j = 0;

            }

        }
        // 判断是否匹配成功
        if(j==s2Len){
            return i-j;
        }
            return -1;



    }


}
