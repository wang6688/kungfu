package com.wwa.study.algorithm.search.dynamic_programming;


/**
 * 背包问题
 * @date Created in 2021/7/2

 */
public class KnapsackProblem {
    public static void main(String[] args) {
        int [] weight ={1,4,3};   // 物品的 重量
        int[] goodsValue = {1500,3000,2000}; // 物品的价值， 这里 goodsValue[i] 就是前面讲的v[i]
        final int PACK_CAPACITY = 4;  // 背包的容量
       final int GOODS_TOTAL = goodsValue.length; // 物品的个数
        // 创建二维数组
        // v[i][j] 表示在前i个物品中能够装入容量为j的背包中的最大价值
        int [][] valueOfGoodsInPack = new int[GOODS_TOTAL+1][PACK_CAPACITY+1];


        // 为了记录放入商品的情况，我们定义一个二维数组
        boolean[][] path =  new boolean [GOODS_TOTAL+1][PACK_CAPACITY+1];
        // 初始化第一行 和第一列，这里在本程序中，可以不去处理，因为默认就是0
        for (int i = 0; i < valueOfGoodsInPack.length; i++) {
            valueOfGoodsInPack[i][0] = 0 ; // 将第一列设置为0
        }
        for (int i = 0; i < valueOfGoodsInPack[0].length; i++) {
            valueOfGoodsInPack[0][i] = 0; // 将第一行设置为0
        }

        // 根据前面得到的公式 来动态规划处理
        for (int goodsIndex = 1; goodsIndex < valueOfGoodsInPack.length; goodsIndex++) {  // 跳过第一行，因为packCapacityLevel是从1开始的
            for (int packCapacityLevel = 1; packCapacityLevel < valueOfGoodsInPack[0].length; packCapacityLevel++) {  //跳过第一列，因为j是从1开始的
                // 公式
                if(weight[goodsIndex-1] >packCapacityLevel){  // 因为我们程序 goodsIndex 是从1 开始的，因此原来公式中的 w[i] 修改称w[i-1]
                    valueOfGoodsInPack[goodsIndex][packCapacityLevel] = valueOfGoodsInPack[goodsIndex-1][packCapacityLevel];


                }else{
                    // 说明： 因为我们的i 是从1 开始的， 因此公式需要调整成 如下
//                    valueOfGoodsInPack[goodsIndex][packCapacityLevel] = Math.max(valueOfGoodsInPack[goodsIndex-1][packCapacityLevel], goodsValue[goodsIndex-1]+valueOfGoodsInPack[goodsIndex-1][packCapacityLevel-weight[goodsIndex-1]]);
                    // 为了记录商品存放到背包的情况，所以不能直接使用上面的公式，需要使用if-else 来体现公式。
                    if(valueOfGoodsInPack[goodsIndex-1][packCapacityLevel] <goodsValue[goodsIndex-1] + valueOfGoodsInPack[goodsIndex-1][packCapacityLevel-weight[goodsIndex-1]]){
                        valueOfGoodsInPack[goodsIndex][packCapacityLevel] = goodsValue[goodsIndex -1] + valueOfGoodsInPack[goodsIndex-1][packCapacityLevel-weight[goodsIndex-1]];
                        // 把当前的 情况记录到 path
                        path[goodsIndex][packCapacityLevel] = true;
                    }else{
                        valueOfGoodsInPack[goodsIndex][packCapacityLevel] = valueOfGoodsInPack[goodsIndex-1][packCapacityLevel];
                    }
                }



            }
        }

        // 输出一下  v 看看目前的情况
        System.out.println("物品名/背包容量等级");
        for (int i = 0; i < valueOfGoodsInPack.length; i++) {
            for (int j = 0; j < valueOfGoodsInPack[i].length; j++) {
                System.out.print("\t"+valueOfGoodsInPack[i][j] +" ");
            }
            System.out.println();
        }
        System.out.println("=============");
        // 输出最后我们是 放入的哪些商品
    /*    // 遍历path，这样输出会把所有的放入情况都得到，其实我们只需要最后的放入
        for (int i = 0; i < path.length; i++) {
            for (int j = 0; j < path[i].length; j++) {
                if(path[i][j] ){
                    System.out.printf("第 %d 个商品 放入到背包\n",i);
                }
            }
        }*/

        // 动脑筋
        int i = path.length -1;  // 行的最大下标
        int j = path[0].length -1 ;  // 列的最大下标
        while (i>0 && j>0){  // 从path的最后开始找
            if(path[i][j] ){
                System.out.printf("第 %d 个商品 放入到 背包\n",i);
                j -= weight[i-1];
            }
            i--;
        }
    }


}
