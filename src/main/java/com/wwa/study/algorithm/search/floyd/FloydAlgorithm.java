package com.wwa.study.algorithm.search.floyd;

import ch.qos.logback.core.pattern.FormatInfo;

import java.util.Arrays;

/**
   弗洛伊德算法
 * @date Created in 2021/7/9
  */
public class FloydAlgorithm {
    static char[] vertex ={'A','B','C','D','E','F','G'};

    public static void main(String[] args) {
        // 测试 图是否创建成功
        // 创建 邻接矩阵
        int [][] matrix = new int[vertex.length][vertex.length];
        final int N = 65535;
        matrix[0] = new int[]{0,5,7,N,N,N,2};
        matrix[1]= new int[]{5,0,N,9,N,N,3};
        matrix[2] = new int[]{7,N,0,N,8,N,N};
        matrix[3] = new int[]{N,9,N,0,N,4,N};
        matrix[4] = new int[]{N,N,8,N,0,5,4};
        matrix[5] = new int[]{N,N,N,4,5,0,6};
        matrix[6] =new int[]{2,3,N,N,4,6,0};

        // 创建Graph 对象
        Graph graph = new Graph(vertex.length, matrix, vertex);
        graph.folyd();
        graph.show();
    }
}
 class Graph{
    private char [] vertex;     // 存放顶点的数组
    private  int[][]  dis;  // 保存，从各个顶点出发到其他顶点的距离，最后的结果，也是保留在该数组
    private int[][] pre;    // 保存到目标节点的前驱顶点。

     /** 构造器
      * @param length 大小
      * @param matrix 邻接矩阵
      * @param vertex 顶点数组
      */
     public Graph(int length,int[][] matrix,char[] vertex){
         this.vertex = vertex;
         this.dis = matrix;
         this.pre = new int[length][length];
         // 对pre数组 初始化，注意存放的是 前驱顶点的下标
         for (int i = 0; i < length; i++) {
             Arrays.fill(pre[i],i);
         }


     }

     // 显示pre 数组和 dis数组
    public void show(){
         // 为了显示 便于阅读，优化输出
        for (int k = 0; k < dis.length; k++) {
            // 先将 pre 数组输出的一行
            for (int i = 0; i < dis.length; i++) {
                System.out.print(pre[k][i] +" ");
            }
            System.out.println();
            // 输出dis 数组的 一行数据
            for (int i = 0; i < dis.length; i++) {
                System.out.print("（"+vertex[k]+"到"+vertex[i]+"的最短路径是"+dis[k][i] +" ）");

            }
            System.out.println("\n");

        }

    }

     /**
      * 弗洛伊德算法,比较容易理解，而且容易实现
      * 复杂度 为n的3次放
      */
    public void folyd(){
         int len = 0;   // 变量保存距离
        // 对中间顶点遍历，middleVertexIdx 就是中间顶点的下标[A,B,C,D,E,F,G]
        for (int middleVertexIdx = 0; middleVertexIdx < dis.length; middleVertexIdx++) {
            // 从 i顶点出发[A,B,C,D,E,F,G]
            for (int beginVertexIdx = 0; beginVertexIdx < dis.length; beginVertexIdx++) {
                // 到达 终点[A,B,C,D,E,F,G]
                for (int endVertexIdx = 0; endVertexIdx < dis.length; endVertexIdx++) {
                    len = dis[beginVertexIdx][middleVertexIdx] +dis[middleVertexIdx][endVertexIdx]; //=> 求出 从开始顶点出发，经过中间顶点，到达结束顶点的距离
                    if(len< dis[beginVertexIdx][endVertexIdx]){  // 如果len（经过其他中间顶点后的距离） 小于 从 开始顶点到结束顶点的距离
                        dis[beginVertexIdx][endVertexIdx] = len;  // 更新距离
                        pre[beginVertexIdx][endVertexIdx] = pre[middleVertexIdx][endVertexIdx];  // 更新前驱顶点，如 A->B的 连接线中，则前驱为A自身，其他顶点前驱情况同理
                    }
                }
            }
        }
    }
}
