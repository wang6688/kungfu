package com.wwa.study.algorithm.search.dijkstra;

import javafx.scene.input.TouchEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Dictionary;

/**
 *
 * @date Created in 2021/7/8
 */
public class DijkstraAlgorithm {
   static final int DIS_CONNECTED = 65535;

    public static void main(String[] args) {
        char[] vertex = {'A','B','C','D','E','F','G'};
        // 邻接矩阵
        int[][] matrix = new int[vertex.length][vertex.length];
        // 表示不可连接
        matrix[0] =  new int []{DIS_CONNECTED,5,7,DIS_CONNECTED,DIS_CONNECTED,DIS_CONNECTED,2};
        matrix[1] = new int[]{5,DIS_CONNECTED,DIS_CONNECTED,9,DIS_CONNECTED,DIS_CONNECTED,3};
        matrix[2] = new int[]{7,DIS_CONNECTED,DIS_CONNECTED,DIS_CONNECTED,8,DIS_CONNECTED,DIS_CONNECTED};
        matrix[3] = new int[]{DIS_CONNECTED,9,DIS_CONNECTED,DIS_CONNECTED,DIS_CONNECTED,4,DIS_CONNECTED};
        matrix[4] =new int[]{DIS_CONNECTED,DIS_CONNECTED,8,DIS_CONNECTED,DIS_CONNECTED,5,4};
        matrix[5] = new int[]{DIS_CONNECTED,DIS_CONNECTED,DIS_CONNECTED,4,5,DIS_CONNECTED,6};
        matrix[6] = new int[]{2,3,DIS_CONNECTED,DIS_CONNECTED,4,6,DIS_CONNECTED};

        // 创建Graph 对象
        Graph graph = new Graph(vertex,matrix);
        // 测试，看看图的连接矩阵是否ok
        graph.showGraph();
        // 测试迪杰斯特拉的算法
        graph.dsj(2);

        graph.showDijkstra();

    }


}
@Data
class Graph{

     private char[] vertex;  // 顶点数组
    private  int [][] matrix;   // 邻接矩阵
    public Graph(char[] vertex,int [][] matrix){
        this.vertex = vertex;
        this.matrix = matrix;
    }

    private  VisitedVertex vv;  // 已经访问的顶点的集合

    /// 显示结果
    public void showDijkstra(){
        vv.show();
    }
    // 显示图
    public void showGraph(){
       for (int[] link : matrix) {
           System.out.println(Arrays.toString(link));
       }
   }



    /**
     * 迪杰斯特拉 算法实现
     * @param index  表示出发点对应的下标
     */
    public void dsj(int index){
         vv = new VisitedVertex(vertex.length,index);
        update(index);  // 更新index顶点到周围顶点的距离 和前驱顶点
        for (int j = 1; j < vertex.length; j++) {
             index = vv.updateArr();
           update(index);     // 更新 index 顶点到周围顶点的距离和前驱顶点

        }
    }

    // 更新index 下标顶点到周围顶点的距离和周围顶点的 前驱顶点
    private void update(int index){
        int len = 0;
        // 根据遍历我们的邻接矩阵的 matrix[index] 行
        for (int i = 0; i < matrix[index].length; i++) {
            // len 含义是  ：出发顶点到index 顶点的 距离+ index 顶点到j顶点的距离的和
            len = vv.getDis(index) + matrix[index][i];
            // 如果i顶点没有被访问过，并且len 小于出发顶点到i顶点的距离，就需要更新
            if(!vv.in(i) && len <vv.getDis(i)){
                vv.updatePre(i,index);  // 更新i顶点的前驱为 index顶点
                vv.updateDis(i,len);    // 更新出发顶点到i顶点的距离
            }
        }
    }
}
// 已访问顶点集合
class VisitedVertex{
    // 记录 各个顶点是否 访问过1=访问过；0= 未访问，会动态更新
    public int[] already_arr;
    // 每个小标对应的 值为前一个顶点下标，会动态更新
    public int[] pre_visited;
    // 记录出发顶点到 其他所有顶点的距离，比如G为出发顶点，就会记录G到其他顶点的距离，会动态更新，求的最短距离就会存放到dis
    public int[] dis;

    public VisitedVertex(int length,int index){
        this.already_arr = new int[length];
        this.pre_visited = new int [length];
        this.dis = new int[length];
        // 初始化 dis 数组
        Arrays.fill(dis, DijkstraAlgorithm.DIS_CONNECTED);
        this.already_arr[index] = 1;  // 设置出发顶点被访问过
        this.dis[index] = 0;    // 设置 出发顶点的访问距离为0
    }


    /**
     * 功能：  判断index 顶点是否被访问过
     * @param index
     * @return 如果访问过，就返回ture，否则返回false
     */
    public boolean in(int index){
        return  already_arr[index] ==1;

    }

    /**
     * 功能： 更新出发顶点到index的距离
     * @param index
     * @param len
     */
    public void updateDis(int index,int len){
        dis[index] = len;
    }

    /**
     * 功能： 更新pre这个顶点的前驱为 index 顶点
     * @param pre
     * @param index
     */
    public void updatePre(int pre,int index){
        pre_visited[pre] = index;
    }

    /**
     * 功能： 返回出发顶点到 index 顶点的距离
     * @param index
     * @return
     */
    public int getDis(int index){
        return dis[index];
    }

    /**
     * 继续选择并返回新的 访问顶点，比如这里的G 完后，就是A点作为新的访问顶点（注意不是出发顶点）
     * @return
     */
    public int updateArr(){
        int min = DijkstraAlgorithm.DIS_CONNECTED,index = 0;
        for (int i = 0; i < already_arr.length; i++) {
            if(already_arr[i] == 0 && dis[i] <min){
                min = dis[i];
                index = i;
            }
        }
        // 更新 index  顶点被访问过
        return index;
    }

    /**
     *     // 显示最后的结果
     *     将三个数组的情况输出
     */
    public void show(){
        System.out.println("===============================");
        //输出alreadl_arr
        for (int i : already_arr) {
            System.out.printf(i+"  ");
        }
        System.out.println();

        for (int i : pre_visited) {
            System.out.printf(i+"  ");
        }
        System.out.println();
        for (int i : dis) {
            System.out.printf(i+"  ");
        }
        System.out.println();


        // 为了好看最后的最短距离，美化以下
        char[] vertex ={'A','B','C','D','E','F','G'};
        int count = 0;
        for (int i : dis) {
            if(i!=65535){
                System.out.printf(vertex[count] +"("+i+")");
            }else{
                System.out.printf("DIS_CONNECT  ");
            }
            count++;
        }
    }
}