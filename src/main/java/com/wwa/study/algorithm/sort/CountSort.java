package com.wwa.study.algorithm.sort;

import java.util.Arrays;
import java.util.Random;

/**
 * 计数排序 ,应用场景：量大但是数据范围值较小的情况下， 如(1.某大型企业数万名员工按年两排序，2.如何快速得知高考名次【腾讯面试】)
 * 算法思想：桶思想的一种 首先 按数据的范围值 分配一个数组(enumElementArray)，每个数组的单元格存放其数值 出现的次数，每出现1次就在对应下标的数组元素中+1
 *  最后再 初始化一个 排好序的新数组（orderedNewAry长度与原始数据数组长度一致）依次从enumElementArray数组中 按元素索引去取出数据放入 orderedNewAry 数组中,
 *  enumElementArray其下标索引存放的是几 就 放入 几次到 orderedNewAry 数组中，从而完成排序
 * @author clark
 * @date 2021/3/8
 * @desc  总结：  计数排序是 非比较排序， 适用于特定问题，也就是 对源数据有要求，其时间复杂度为 2(N+K),空间复杂度为(N+K)
 * @return
 */
public class CountSort {

    public static void main(String[] args) {
//        int [] rangeDataAry = {0,1,2,3,40,0,21,34,21,32,44,32,43,23,34,23,23,53,63,53,44,23,45,23,64,34,54,64,23,43,23,64,54,43,44,23,44,23,23,22,54,43,32,33,44,22,21,19,23,40,34,54,34};
        int []msbAry = {2,4,2,3,7,1,1,0,0,5,6,9,8,5,7,4,0,9};
        int[] orderedNewAry = msbSort(msbAry);
//        int[] orderedNewAry = mySort(msbAry);
        System.err.println("排好序的是"+ Arrays.toString(orderedNewAry));
//        test();

    }

    /**
     * @param employeeAgeAry
     * @return fixme  该算法不稳定
     */
    public static int [] mySort(int[] employeeAgeAry) {
        int []enumElementArray = new int[100];
        for (int index = 0; index < employeeAgeAry.length; index++) {
            int age  = employeeAgeAry[index];
            enumElementArray[age] += 1;
        }
        int [] orderedNewAry = new int[employeeAgeAry.length];
        int initIdx = 0;

        for (int enumEleIndex = 0; enumEleIndex < enumElementArray.length; enumEleIndex++) {
            int enumEleCount = enumElementArray[enumEleIndex];
            for(int k= 0;k<enumEleCount;k++){
                orderedNewAry[initIdx++] = enumEleIndex;
            }
        }
        return orderedNewAry;
    }

    public static int []  msbSort(int[] rangeDataAry) {
        int [] result = new int[rangeDataAry.length];
        int[] count = new int[10];
        for (int i = 0; i < rangeDataAry.length ; i++) {
            count[rangeDataAry[i]]++;
        }
        //fixme 此种写法的 算法是不稳定的
//        for (int i = 0,j=0;i<count.length;i++){
//            while (count[i]-- >0) {result[j++]=i;}
//        }
        // 稳定的排序改进后 ，其思路为 count数组 记录的是 枚举元素索引和对应的偏移量
        //例如： 原数组 是{0,1,0,2,2,0,1,1,1,1}, 则其count数组中的元素为 {index0=3,index1=8(index0+5个1),index2=10(index1+两个2)}
        for (int i = 1; i < count.length; i++) {
            count [i] = count[i]+ count[i-1];
        }
        //往 结果数组放入 时  直接取原数组最后的元素 然后按其枚举元素值在count数组中的对应位置 直接按 真实原下标放入对应的 下标即可，这样就还是原来的位置序，是稳定的
        for (int i = rangeDataAry.length-1; i >=0 ; i--) {
            result[--count[rangeDataAry[i]]] = rangeDataAry[i];
        }
        return result;
    }


    public static void test( ) {
        Random random = new Random();
        int[] arr = new int [10000];
        for (int i = 0; i <arr.length ; i++) {
            arr[i] = random.nextInt(100);
        }
        int[] result = mySort(arr);
        Arrays.sort(arr);
        boolean same = true;
        for (int i = 0; i < arr.length; i++) {
            if(result[i]!=arr[i]) same = false;
        }
        assert  same ;

    }
}
