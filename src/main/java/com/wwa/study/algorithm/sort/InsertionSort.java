package com.wwa.study.algorithm.sort;

/**
 * @param （3）
 * @author clark
 * @date 2021/2/19
 * @desc 插入排序： 对于基本有序的数组最好用-它是稳定的。
 * 思路： 先定位到数组中下标为1 的元素，另其与前面相邻的元素进行比较，若比前面相邻的元素小，则交换它们俩的位置，单次比较/交换完成，后面的 将数组偏移++ ，然后依次执行前面的过程
 *
 *  其 平均时间复杂度为 n² ，最坏时间复杂度为 n²，最好时间复杂度也是n，空间复杂度为1
 *
 *  ！！！！优劣分析： 样本小且 基本有序的时候效率比较高 ，插入排序 基本要比冒泡排序快一倍，比选择排序快一点，也是稳定的，需要简单排序时用插入排序即可，掌握
 * @return
 */
public class InsertionSort {

    public static void main(String[] args) {
        int[] ary = {9, 6, 1, 3, 5};
//        mySort(ary);
            msbSort(ary);
//            System.err.println("第"+j+"种的数 依次为:");
        for (int d : ary) {
            System.err.print(d + ",");
        }
        System.out.println();
    }


    public static void mySort(int[] ary) {
        if (null == ary || ary.length < 1) {
            return;
        }
//        int offset = 1;
        int temp = 0;
        for (int offset = 1; offset < ary.length; offset++) {
            for (int before = offset - 1; before >= 0; before--) {
                //如果后面的数 小于 前面的数 ，则将后面的数 向前移动一个位置
                //否则 后面的数大于前面的数 ，本轮比较结束，向后偏移一个位置继续比较即可。
                if (ary[offset] < ary[before]) {
                    temp = ary[before];
                    ary[before] = ary[offset];
                    ary[offset] = temp;
                    offset--;
                } else {
                    break;
                }

            }
        }
    }

    public static  void msbSort(int[] ary){
        int temp = 0;

        for (int i = 0; i < ary.length; i++) {
            for (int j = i; j >0; j--) {
                if (ary[j] < ary[j-1]) {
                    temp = ary[j-1];
                    ary[j-1] = ary[j];
                    ary[j] = temp;
                 }
            }
        }
    }

}
