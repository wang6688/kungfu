package com.wwa.study.algorithm.sort;

/**
 * @name 希尔排序 :改进的插入排序 (4)
 * @author clark
 * @date 2021/2/19
 * @desc 希尔排序： ，在排序之前 需要先指定一个起始位置量(0)和一个 间隔偏移量(比如：4,建议采用 knuth序列)，每次经过这个偏移量找到对应的数 将这些数字作为一组，把这一组数用插入排序 排列好顺序
 * 当 按照间隔第一次取完数并排好序后，将其实位置量向后偏移1位 再按照间隔偏移量取一组数，再把这一组用插入排序拍好顺序
 * 以此类推上述过程（起始位置量 需 小于  间隔偏移量），经过此番操作后 大致的 数据 时相对有序的了，
 * 但还要 缩小间隔偏移（比如：2）再执行上述过程，经过几轮操作后，
 * 最终 再将 间隔偏移量 缩小为1 ，再执行一次 插入排序  才算大功告成
 *
 * 优劣分析： 在 间隔偏移量 比较大的时候 移动的次数比较少，在间隔偏移量比较小（=1）的时候，移动的距离又比较小，
 *         希尔排序 比普通插入排序效率 要高，但由于其采取的策略是跳着排 所以不稳定，不太重要
 *         平均情况下大家普遍任务 其最好的时间复杂度为 n的1.3次方 ，根据采取的序列不同，其复杂度略有差异
 * @return
 */
public class ShellSort {
    public static void main(String[] args) {
        int [] arr = {9,6,11,3,5,12,8,7,10,15,14,4,1,13,2};
        msbSort(arr);
        for (int d : arr) {
            System.err.print(d + ",");
        }
        System.out.println();
    }


    public static void msbSort(int[] ary) {
        for (int gap = ary.length >>1 ; gap >0; gap/=2 ) {
            for (int i = gap; i < ary.length; i++) {
                for (int j = i; j > gap -1; j-=gap ) {
                    if(ary[j] <ary[j-gap]){
                        int temp = ary[j];
                        ary[j] = ary[j-gap];
                        ary[j-gap] = temp;
                    }
                }
            }
        }
    }
}
