package com.wwa.study.algorithm.sort;

import java.security.SecureRandom;
import java.util.Arrays;

/**
 * @param
 * @author clark
 * @date 2021/2/18
 * @desc 对数器
 * @return
 */
public class DataChecker {
    public static int[] generateRandomArray( ) {
        SecureRandom r = new SecureRandom();
        int [] arr = new int[10000];
        for (int i = 0; i < arr.length; i++) {
            arr[i]= r.nextInt(10000);
        }
        return arr;
    }

    public static void check() {
        boolean isSame = true;
        for (int times = 0; times < 1000; times++) {
            int [] arr = generateRandomArray();
            int []arr2= new int [arr.length];
            System.arraycopy(arr,0,arr2,0,arr.length);
            Arrays.sort(arr);
//        SelectionSort.mySort(arr2);
//        BubbleSort.mySort(arr2);
//        InsertionSort.mySort(arr2);
//        ShellSort.msbSort(arr2);
//            MergeSort.mySort(arr2,0,arr2.length-1);
            QuickSort.mySort(arr2,0,arr2.length-1);
            for (int i = 0; i < arr2.length; i++) {
                if(arr[i] !=arr2[i]) {
                    isSame = false;
                    break;
                 }
            }
            if(!isSame){
                break;
            }


        }
        System.out.println(isSame?"right":"wrong");
    }

    public static void main(String[] args) {
        check();
    }
}
