package com.wwa.study.algorithm.sort;

/**
 * @author clark
 * @name 归并排序 (5)
 * @date 2021/2/20
 * @desc TIM SORT(改进的归并排序)：:Java 对象排序专用
 * 思路：  假设一个给定的大数组(如{1,4,7,8,3,6,9}) 若将其从中间切分为两个逻辑上的子数组(a和b)，且两个子数组在其内部是有序的
 * 先需按照 大数组的长度 创建一个同样长度的 新数组 c，然后 取子数组a和子数组b的第一个元素进行比较，若a下标对应的元素小于b下标的元素
 * 则将a对应下标的元素 拷贝到新数组c中，然后a数组下标向后偏移1位 取其元素再和b数组下标指向的元素进行比较，若a数组下标对应的元素 大于b 下标对应的元素
 * 则将b 对应下标的元素拷贝到新数组c中，然后b数组下标向后偏移1位 取其元素再和a数组下标指向的元素进行比较，依次类推此过程，
 * 直到 a数组和b数组同时达到界限为止 。
 * 其 平均的时间复杂度 为 O(N * logN)  ;空间复杂度为 O(N)
 * 实际使用中较常用，在 java/python中 对于对象的排序用的都是归并排序，而对象排序一般要求稳定
 * 回顾： 归并排序其实就是一个递归的过程，分两块，每一块（分两块）排好后再两两归并，直到结束
 * @return
 */
public class MergeSort {

    public static void main(String[] args) {
        // 待排序的大数组
        int[] arr = {1, 4, 7, 8, 3, 6, 9};
//        int[] arr = {1, 3, 5, 7, 23, 45, 47, 50, 2, 4, 8, 9, 12, 16, 30};

//        int[] arrangeAry = mySort(arr);
      mySort(arr,0,arr.length-1);
        for (int d : arr) {
            System.err.print(d + ",");
        }
        System.out.println();
    }

    public static  void mySort(int[] ary,int left,int right) {
        //当数组粒度小到无法再进行拆分时（左侧值和右侧值相等，则递归条件结束）
        if(left==right) return;
        //将完整的 大的无序数组 从中间进行拆开，分别对左右两边的数组 进行排序和归并
//        int middle = (left+right)/2;
        int middle = left+(right-left)/2;
//
////        //对左侧 数组进行排序
        mySort(ary,left,middle);
////        //对右侧数组进行排序
        mySort(ary,middle+1,right);
//        //将左右两侧的数组 进行归并操作
        int[] merge = merge(ary, left, middle+1, right);
//        for (int d : merge) {
//            System.err.print(d + ",");
//        }
//        System.out.println();
    }

    /**
     * @Desc 思路： 假设一个给定的大数组(如{1,4,7,8,3,6,9}) 若将其从中间切分为两个逻辑上的子数组(a和b)，且两个子数组在内部是有序的
     * 先需按照 大数组的长度 创建一个同样长度的 新数组 c，然后 取子数组a和子数组b的第一个元素进行比较，若a下标对应的元素小于b下标的元素
     * 则将a对应下标的元素 拷贝到新数组c中，然后a数组下标向后偏移1位 取其元素再和b数组下标指向的元素进行比较，若a数组下标对应的元素 大于b 下标对应的元素
     * 则将b 对应下标的元素拷贝到新数组c中，然后b数组下标向后偏移1位 取其元素再和a数组下标指向的元素进行比较，依次类推此过程，
     * 直到 若比较排序过程中 a或b数组达到界限为止，之后继续 判断 若a数组得到界限且b数组未达到界限 则最后将b数组中的其余元素拷贝到数组c中，数组b反之亦然
     * @param arySection 数组片段
     * @param leftPointer 左侧下标指针
     * @param rightPointer  右侧下标指针
     * @param rightBorder   右侧边界
     */
    public static int[] merge(int[] arySection,int leftPointer,int rightPointer,int rightBorder) {
        //定义新数组 接收 排好序后的数据
        int[] newAry = new int[rightBorder-leftPointer+1];
        //定义3个指针下标，分别指向 大数组的中间位置， a数组的起始为止 ，和b数组的起始位置
        int midIndex = rightPointer-1;
        //指向 逻辑子数组a的起始位置
        int aIndex = leftPointer;
        //指向逻辑子数组b的起始位置
        int bIndex = rightPointer;
        //指向新数组的 起始位置
        int cIndex = 0;
        // 当 子数组a和b 都没有到达界限时  进行相应的比较  并将对应数组的元素拷贝到新数组c中
        while (aIndex <= midIndex && bIndex <= rightBorder) {
            if (arySection[aIndex] <= arySection[bIndex]) {
                newAry[cIndex++] = arySection[aIndex++];
            } else {
                newAry[cIndex++] = arySection[bIndex++];
            }
        }
        //若子数组b中的值全部到遍历完拷贝到新数组c中了，但子数组a中还产生了遗留，则再将子数组a中遗留的值全部依次拷贝到数组c中
        while (aIndex <= midIndex){
            newAry[cIndex++] = arySection[aIndex++];
        }
        //若子数组a中的值全部到遍历完拷贝到新数组c中了，但子数组b中还产生了遗留，则再将子数组b中遗留的值全部依次拷贝到数组c中
        while (bIndex <= rightBorder){
            newAry[cIndex++] = arySection[bIndex++];
        }

//        将新数组中 排好序的元素替换掉 原数组中未排序的元素
        for (int newIndex = 0; newIndex < newAry.length; newIndex++) {
            arySection[leftPointer+newIndex] =newAry[newIndex];
        }
        return newAry;
    }
}
