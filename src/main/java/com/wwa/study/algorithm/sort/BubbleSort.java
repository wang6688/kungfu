package com.wwa.study.algorithm.sort;

/**
 * @param  （2）
 * @author clark
 * @date 2021/2/18
 * @desc 冒泡排序：
 * 注： 冒泡排序 是 稳定的 ，其 平均时间复杂度为 n² ，最坏时间复杂度为 n²，最好时间复杂度也是n，空间复杂度为1
 * 将 数组中的 数值 按相邻顺序依次两两进行比较，若发现 其后面的数比自己小 则相邻的这两个数交换位置，直到最后一个为止 。
 * 第二次 排序时 按照上面的套路继续进行即可，只不过需要把 数组最后的那个已经 挑出来的数 剔除掉不参与本次比较即可
 * 依此类推
 *
 * 优劣分析： 基本不用，太慢， 因为其需要两两比较，两两交换，比较和交换都非常多，他是最慢的一种
 * @return
 */
public class BubbleSort {
    public static void main(String[] args) {
        //定义待排序的 数组
        int []ary = {3,8,4,5,2,6,9,12,43,53,22};   //不计入算法时间
//        mySort(ary);
        msbSort(ary);
        for (int d : ary) {
            System.err.print(d+",");
        }
        System.out.println();

    }


    public static void mySort(int[] ary) {

//        依此类推
        int temp = 0;
        int compFlag = 0;
        for (int j = 1; j < ary.length-1; j++) {  //外层循环初始值设为1 是因为 内层循环其数组的索引初始值为0，在比较时与后面一个相邻的数比较故在次直接设为1让其取后面相邻的数即可，循环条件设为length-1 是为初始值从1开始故第0个索引值就不用再参与外层循环的计算了。
            //  第二次 排序时 按照上面的套路继续进行即可，只不过需要把 数组最后的那个已经 挑出来的数 剔除掉不参与本次比较即可
            for (int i = 0; i < ary.length-j; i++) { //循环条件 设为length-j 意为将 外层循环 比较完的数据 进行剔除
                compFlag++;
                // 将 数组中的 数值 按相邻顺序依次两两进行比较，若发现 其后面的数比自己小 则相邻的这两个数交换位置，直到最后一个为止 。
                if(ary[i] > ary[i+1]){
                    temp = ary[i];
                    ary[i] = ary[i+1];
                    ary[i+1] = temp;
                }
            }
//            System.err.println("第"+j+"种的数 依次为:");
//            for (int d : ary) {
//                System.err.print(d+",");
//            }
//            System.out.println();
        }
        System.out.println("内层循环其次数分别为"+compFlag);



    }

    public static void msbSort(int[] ary) {
        int compFlag = 0;

        for (int i = ary.length-1; i >0; i--) {
            for (int j = 0; j < i; j++) {
                compFlag++;
                if(ary[j]>ary[j+1]) {
                    int temp = ary[j];
                    ary[j] = ary[j+1];
                    ary[j+1] = temp;
                }
            }
//              第二次 排序时 按照上面的套路继续进行即可，只不过需要把 数组最后的那个已经 挑出来的数 剔除掉不参与本次比较即可
//            System.err.println("第"+j+"种的数 依次为:");

        }

        System.out.println("内层循环其次数分别为"+compFlag);
    }
}
