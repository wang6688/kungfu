package com.wwa.study.algorithm.sort;

import java.util.Arrays;

/**
 * 基数排序 ： 非比较排序，桶思想的一种，本质上是一种多关键字排序
 * 算法思想：  首先按数组中 所有数字的 个位数进行排序（个位相同的数儿放到同一个桶里），
 *              之后再按照十位数 进行排序（十位数相同的数儿放到同一个桶里），
 *              最后按照 百位数 进行排序（百位相同的数儿放到同一个桶里）。
 *  例如:      {421,240,115,532,305,430,124}
 *第一次按个位排 {240,430,421,532,124,115,305}
 * 第二次按十位排{305,115,421,124,430,532,240}
 *第三次按百位排{115,124,240,305,421,430,532}
 *
 * 空间复杂度 O(n), 时间复杂度 O(n*k),稳定排序
 * @author clark
 * @date 2021/3/9
 * @desc
 * @return
 */
public class RadixSort {

    public static void main(String[] args) {
        int[] ary =  {421,240,115,532,305,430,124};
        //todo 第一步 应该先求出 最高位数
        msbSort(ary);
        System.out.println(Arrays.toString(ary));
    }

    public static  int [] msbSort(int[] arr) {
        int []result = new int [arr.length];
        int []count = new int[10];

        for (int i = 0; i < 3; i++) {
            int division = (int)Math.pow(10, i);
            System.out.println(division);
            for (int j = 0; j < arr.length; j++) {
                int num = arr[j] / division % 10;
                System.out.printf(num +" ");
                count[num]++;

            }
            System.out.println();
            System.out.println(Arrays.toString(count));
            for (int m = 1; m < count.length; m++) {
                count[m]= count[m]+ count[m-1];

            }
            System.out.println(Arrays.toString(count));
            for (int n = arr.length-1; n >=0 ; n--) {
                int num = arr[n] / division % 10;
                result[--count[num]] = arr[n];
            }
            System.arraycopy(result,0,arr,0,arr.length);
            Arrays.fill(count,0);
        }
        return result;

    }


//    public static  int [] mySort(int[] ary){
//
//    }
}
