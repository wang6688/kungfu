package com.wwa.study.algorithm.sort;

/**
 * @param （1）
 * @author clark
 * @date 2021/2/18
 * @desc 算法的稳定性：  若 待排序数组中的两个数值相等 但位置不相等，经过排序后若其两个数的位置发生了变化 则称其 不稳定
 * 选择排序的思路：
 *   1.临时记录下数组中起始位置的数 ，拿该数依次与数组中的所有数据进行比对，找到该数组中最小的数字 记录其数组下标
 *   2.根据下标从数组中 取出该数字 然后放入 数组的起始位置 进行两个数的交换 ，单次比对完成
 *   3.依次循环往复此过程
 *  注：  选择排序就是 不稳定的 ，其 平均时间复杂度为 n² ，最坏时间复杂度为 n²，最好时间复杂度也是n²，空间复杂度为1
 * 优劣分析： 基本不用，不稳 ，因为其并不比插入快
 * @return
 */
public class SelectionSort {
    public static void main(String[] args) {
        //定义待排序的 数组
        int []ary = {3,8,4,5,2,6,9,12,43,53,22};   //不计入算法时间
        mySort(ary);
//        msbSort(ary);
    }

    public static void mySort(int[] ary){

        // 3.依次循环往复此过程
        for (int j = 0; j < ary.length-1; j++) {
            // 1.临时记录下数组中起始位置的数 ，拿该数依次与数组中的所有数据进行比对，找到该数组中最小的数字 记录其数组下标
            int temp = ary[j];
            int minIndex = -1;
            for (int i = j+1; i < ary.length; i++) {
                if(ary[i]< temp){
                    temp = ary[i];
                    minIndex = i;
                }
            }
            if(minIndex<0) continue;
             // 2.根据下标从数组中 取出该数字 然后放入 数组的起始位置 进行两个数的交换 ，单次比对完成
            int swap =  ary[j];
            ary[j] = ary[minIndex];
            ary[minIndex] = swap;
            System.err.println("第"+j+"种的数 依次为:");
            for (int d : ary) {
                System.err.print(d+",");
            }
            System.out.println();
        }
    }

    public static void msbSort(int[] ary){

        // 3.依次循环往复此过程
        for (int j = 0; j < ary.length-1; j++) {   //由于 内层循环直接取的是往后偏移一个数进行比较，故外层循环可保证在循环时少循环1次
            // 1.临时记录下数组中起始位置的数 ，拿该数依次与数组中的所有数据进行比对，找出该数组中最小的数字 记录其数组下标
             int minIndex = j;
            for (int i = j+1; i < ary.length; i++) {  //由于外层循环已经作为基础数了，故在内层循环时 直接向后偏移1个位置进行比较即可
                if(ary[i]< ary[minIndex]){
                     minIndex = i;
                }
            }
             // 2.根据下标从数组中 取出该数字 然后放入 数组的起始位置 进行两个数的交换 ，单词比对完成
            int swap =  ary[j];
            ary[j] = ary[minIndex];
            ary[minIndex] = swap;
//            System.err.println("第"+j+"种的数 依次为:");
//            for (int d : ary) {
//                System.err.print(d+",");
//            }
//            System.out.println();
        }
    }

}
