package com.wwa.study.algorithm.sort;

/**
 * 快速排序 经典的叫做 单轴快排，改进的（JAVA工业排序）叫双轴快排
 *
 * @author clark
 * @date 2021/3/2
 * @desc 实现思路，假设 有一个数组，在数组中寻找一个值作为轴（例如指定数组最后一个下标对应的值），然后 将 该数组 分为两个区，
 * 其第1个区域  存放 <= 轴 的数值  作为该轴的左侧区域，第2个区域 存放 > 轴的数值  作为该轴的右侧区域
 * 然后 对 左侧区域和右侧区域分别调用递归进行排序
 * <p>
 * 经典快排 思路： 两边同时找，找到后同时做交换， 从左往右找时，一直找到第一个 比 轴值 大的数，   从右往左找时 一直找到第一个比 轴值 小的数，当这两个数都找到时 同时做交换
 * 最后 再将 轴的下标数据与  左下标指针最后停留的位置数据 进行交换即可。
 *
 * 注意： 快速排序的时间复杂度 是  O(N*log N) , 空间复杂度是 O(log N）
 * @return
 */
public class QuickSort {
    public static void main(String[] args) {
        int[] ary = {7, 3, 2,6, 8, 1, 9, 5, 4, 6,10,6};
        System.err.println("排序之前的数组元素为:");
        for (int i : ary) {
            System.out.print(i+",");
        }
        System.out.println();
        mySort(ary,0,ary.length-1);
        System.err.println("排序之后的数组元素为:");
        for (int i : ary) {
            System.out.print(i+",");
        }
        //
     }

    /**
     * 将 数组  按轴值 进行大致排列，将小于轴值的数 放到轴的左侧，大于轴的数放到轴值右侧，
     * 依次对轴左侧和右侧的 数据 递归往复上述过程（ 不停的拆分缩小范围 改变 轴值 ）
     * 直到左侧下标和右侧下标重合时代表 该部分数据排序执行结束
     * @param arr
     * @param leftBound
     * @param rightBound
     */
    public static void mySort(int[] arr,int leftBound, int rightBound) {
        //如果在排序过程中 左侧下标与右侧下标产生了重合则 排序结束
        if(leftBound>= rightBound)return;
        //将 数组 按轴进行 大致排列，将小于轴值的数 放到 轴下标左侧，大于 轴值的数放在 轴右侧，返回中间轴值的 下标
        int axisIndex = partition(arr, leftBound, rightBound);
        //对 中间轴左侧的数据 进行 排序,
        mySort(arr,leftBound,axisIndex-1);
        //对 中间轴右侧的数据 进行 排序
        mySort(arr,axisIndex+1,rightBound);
    }

    /**
     * @param arr
     * @param leftBound
     * @param rightBound
     * @return 轴值所在下标
     */
    public static int partition(int[] arr,int leftBound, int rightBound) {
        // 用数组最后一个下标的值 作为 轴 值
        int axisIndex = rightBound;
        int axisVal = arr[axisIndex];

        int leftIndex = leftBound;
        //右侧数组下标从 轴下标左侧开始;
        int rightIndex = axisIndex -1 ;
        //左侧下标不断向右侧偏移，右侧下标不断向左侧偏移
        //当 左侧下标 大于右侧下标时则程序停止
        while(leftIndex <= rightIndex){
            // 若 左侧下标的值 < 轴值 则 左侧下标向右偏移1，直到 左侧下标的值 >轴值 时 则记录该左侧下标, 准备后续将该下标的值 移到右侧区域
            // 加入 leftIndex <=rightIndex 用于处理 特殊情况：”轴下标左侧的数 全部都 < 轴值时（即轴值为数组中最大的数）“
//            while( leftIndex < axisIndex&& arr[leftIndex]<= axisVal){
            while( leftIndex <=rightIndex&& arr[leftIndex]<= axisVal){

                    leftIndex++;
            }
            //继续寻找右侧下标
            //若右侧下标的值 > 轴值 则 右侧下标 向左偏移1，直到右侧下标的值 <  轴值 时则记录该右侧下标
            //leftIndex <=rightIndex 用于处理特殊情况： ”轴下标左侧 的数全部都 > 轴值 时（即 轴值为数组中 最小的数） “
//            while (rightIndex>0&& arr[rightIndex] > axisVal){
            while (leftIndex <=rightIndex&& arr[rightIndex] > axisVal){

                    rightIndex--;
            }
            //将本次停留 的左侧下标的值 和 右侧下标的值 做交换
            //只有在左侧下标和右侧下标 未发生重合的情况下才进行交换。（发生重合时则意味着 左右两侧的数都是已经经过交换或无需再进行交换的（基本相对有序）的数据了）
            if(leftIndex<rightIndex){
                swap(arr,leftIndex,rightIndex);
                //因 左侧下标值和右侧下标值已经进行了交换，则 需使左侧下标向后右偏移1位，右侧下标向左偏移1位
                leftIndex++;rightIndex--;
            }
        }
        //最后再将 轴的下标对应的值 与 最终停留下来的左侧下标的值 做交换
        //即将该 数组 排列成了以  轴值 为中心 左侧和右侧 部分 大致分别时有序的
        swap(arr,leftIndex,axisIndex);
        //返回 轴值所在 下标
        return leftIndex;
    }

    private static  void swap(int[] ary,int index1,int index2){
        int temp = ary[index1];
        ary[index1] = ary[index2];
        ary[index2] = temp;
    }
}
