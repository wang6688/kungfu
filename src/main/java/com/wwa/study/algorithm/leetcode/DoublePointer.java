package com.wwa.study.algorithm.leetcode;

import com.wwa.study.interview.classLoad.Father;
import org.junit.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 双指针-- 有序数组的平方
 * @date Created in 2021/7/13

 */
public class DoublePointer {




    /**
     *
     * 显然，如果数组 \textit{nums}nums 中的所有数都是非负数，那么将每个数平方后，数组仍然保持升序；如果数组 \textit{nums}nums 中的所有数都是负数，那么将每个数平方后，数组会保持降序。
     * 这样一来，如果我们能够找到数组 \textit{nums}nums 中负数与非负数的分界线，那么就可以用类似「归并排序」的方法了。具体地，我们设 \textit{neg}neg 为数组 \textit{nums}nums 中负数与非负数的分界线，也就是说，\textit{nums}[0]nums[0] 到 \textit{nums}[\textit{neg}]nums[neg] 均为负数，而 \textit{nums}[\textit{neg}+1]nums[neg+1] 到 \textit{nums}[n-1]nums[n−1] 均为非负数。当我们将数组 \textit{nums}nums 中的数平方后，那么 \textit{nums}[0]nums[0] 到 \textit{nums}[\textit{neg}]nums[neg] 单调递减，\textit{nums}[\textit{neg}+1]nums[neg+1] 到 \textit{nums}[n-1]nums[n−1] 单调递增。
     *同样地，我们可以使用两个指针分别指向位置 00 和 n-1n−1，每次比较两个指针对应的数，选择较大的那个逆序放入答案并移动指针。这种方法无需处理某一指针移动至边界的情况，读者可以仔细思考其精髓所在。
     *
     * 作者：LeetCode-Solution
     * 链接：https://leetcode-cn.com/problems/squares-of-a-sorted-array/solution/you-xu-shu-zu-de-ping-fang-by-leetcode-solution/
     * @param nums
     * @return
     */
    public static int[] sortedSquares(int[] nums) {
        int n = nums.length;
        int [] ans = new int[n];
        for(int i = 0,j=n-1,pos = n-1;i <=j;){
            if(nums[i] *nums[i] >nums[j]*nums[j]){
                ans[pos] = nums[i] * nums[i];
                ++i;
            }else{
                ans[pos] = nums[j] * nums[j];
                --j;
            }
            --pos;
        }
        return ans;
    }
    @Test
    public   void testSortedSquares() {
        int []nums = {-4,-1,0,3,10};
        int[] ints = sortedSquares(nums);
        System.out.println(Arrays.toString(ints));
    }
    /**
     * 思路如下：
     * 首先对整个数组实行翻转，这样子原数组中需要翻转的子数组，就会跑到数组最前面。
     * 这时候，从 kk 处分隔数组，左右两数组，各自进行翻转即可。
     * @param nums
     * @param k
     * @return
     */
    public static int []    rotate(int [] nums,int k){
        int delimit = k %nums.length ;
        reverse(nums,0,nums.length-1);
        reverse(nums,0,delimit-1);
        reverse(nums,delimit,nums.length-1);
        return nums;
    }

    private static void reverse(int []ary,int start,int end){
        int temp = 0;

        while(start < end){
            temp = ary[start];
            ary[start] = ary[end];
            ary[end] =temp;
            start++;
            end--;
        }
    }
    @Test
    public   void testRotate( ) {
        int []ary ={1,2,3,4,5,6,7};  //{1,2,3,4,5,6,7}  ; {-1,-100,3,99}
        int[] rotate = rotate(ary, 3);
        System.out.println(Arrays.toString(rotate));
    }


     private  void moveZeroes(int[] nums) {
        if(nums.length<2){
            return ;
        }
        // 设定一个 快指针 不断的向后遍历数组中数字，设定一个慢指针跟随快指针进行遍历，但若当快指针指向的元素值为0时，
        // 则快指针继续向后遍历寻找到下一个非0的数字，将快指针指向的非0值 填充到慢指针与快指针的距离元素。
        // 填充完后，慢指针继续尾随快指针共同向后移动，将快指针指向的元素填充到慢指针位置的元素，直到快指针到达数组尾部，最后将慢指针到快指针的距离元素填充为0
        int slowIndex = 0;
        for (int fastIndex =0; fastIndex<nums.length;fastIndex++ ){
            if(nums[fastIndex]!=0){
                nums[slowIndex ] = nums[fastIndex];
                slowIndex++;
            }
        }
         for (;slowIndex< nums.length;slowIndex++){
            nums[slowIndex] = 0;
        }
    }
    @Test
    public void testMoveZeroes(){
        int [] ary = new int[]{0, 0,1, 0, 3, 12};
        ary = new int[]{1,0};
        moveZeroes(ary);
        System.out.println(Arrays.toString(ary));
    }

    /**
     * 使用双指针，一个指针指向值较小的元素，一个指针指向值较大的元素。指向较小元素的指针从头向尾遍历，指向较大元素的指针从尾向头遍历。
     * 如果两个指针指向元素的和 sum == target，那么得到要求的结果；
     * 如果 sum > target，移动较大的元素，使 sum 变小一些；
     * 如果 sum < target，移动较小的元素，使 sum 变大一些。
     * 数组中的元素最多遍历一次，时间复杂度为 O(N)。只使用了两个额外变量，空间复杂度为 O(1)。
     * @param numbers
     * @param target
     * @return
     */
    private int[] twoSum(int[] numbers, int target) {
        for(int left = 0,  right= numbers.length-1;left<right;){

            if(numbers[left+1]+numbers[right] == target){
                return new int[]{left+1,right};
            }else if(numbers[left+1]+ numbers[right] >target){
                right--;
            }else{
                left++;
            }
        }
        return new int[]{-1,-1};
    }
    @Test
    public void testTwoSum(){
        int [] ary = new int[]{-1,2,7,11,15};
        int target = 9;
//        ary = new int[]{-1,2,3,4};
//        target = 6;
        int[] ints = twoSum(ary, target);
        System.out.println(Arrays.toString(ints));
    }


    /**
     * @param c
     * @return
     */
    public boolean judgeSquareSum(int c) {
        char[] metaAry ={'a','e','i','o','u'};
         int lg = (int)Math.sqrt(c);
        int left = 0;
        while(left<=lg){
            if((left*left + lg*lg )== c){
                return true;
            }else if((left*left + lg*lg )< c){
                left++;
            }else{
                lg--;
            }
        }
        return false;
    }
    @Test
    public void testJudgeSquareSum(){
        boolean b = judgeSquareSum(2);
        System.out.println(b);
        String str= "";
        str.toCharArray();
    }

    public String reverseVowels(String s) {
        int left= 0 ;
        int right = s.length()-1;

        while(left<right){
            while(left<right&&!isMeta(s.charAt(left))){
                left++;
            }
            while(left<right&&!isMeta(s.charAt(right))){
                right--;
            }
            s = swap(s,left,right);
            left++;
            right--;


        }
        return s;
    }
    @Test
    public void testReverseVowels(){
        String hello = reverseVowels("hello");
        System.out.println(hello);
        Set metas = Stream.of('a','e','i','o','u','A','E','I','O','U').collect(Collectors.toSet());

    }
    private boolean isMeta(char c){
        boolean isMeta = false;
        char[] metaAry ={'a','e','i','o','u'};
        for(int i = 0;i<metaAry.length;i++){
            if(c== metaAry[i]){
                isMeta = true;
                break;
            }
        }
        return isMeta;
    }

    private String swap(String str,int idx1,int idx2){
        char [] cAry = str.toCharArray();
        char temp = cAry[idx1];
        cAry[idx1] = cAry[idx2] ;
        cAry[idx2] = temp;
        return new String(cAry);
    }

    private  void merge(int[] nums1, int m, int[] nums2, int n) {
        int index1 = m-1;
        int index2 = n-1;
        int finalIndex = m+n -1;
        while(index2>=0){
            if(nums1[index1]<nums2[index2]){
                nums1[finalIndex--]= nums2[index2--];
            }else{
                nums1[finalIndex--]= nums1[index1--];
            }
        }
        while (index1>=0){
            nums1[finalIndex--] = nums1[index1--];
        }
        while (index2>=0){
            nums1[finalIndex--] = nums1[index2--];
        }
    }
    @Test
    public void testMerge(){
        int []  nums1 = {1,2,3,0,0,0};
        int []nums2 = {2,5,6};
        merge(nums1,3,nums2,3);
        System.out.println(Arrays.toString(nums1));
    }


    @Test
    public void testUse(){
        System.out.println("abc".compareTo("adf"));
    }
}

