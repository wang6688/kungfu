package com.wwa.study.algorithm.leetcode;

/**
 * 回文数-简单
 * @author wangweian@chinafocus.net
 * @description
 * @date Created in 2021/7/12
 * @modified By：
 */
public class Palindrome {

    public static void main(String[] args) {
        boolean palindrome = isPalindrome(0);
        System.out.println(palindrome);

    }
    public static boolean isPalindromeByStr(int x) {
        int numLength =  String.valueOf(x).length();

        for( int leftIdx = 0, rightIdx = numLength-1; leftIdx <rightIdx;leftIdx++,rightIdx--){
            if(String.valueOf(x).charAt(leftIdx)!= String.valueOf(x).charAt(rightIdx)){
                return false;
            }
        }
        return true;
    }

    public static boolean isPalindrome(int num) {
        int lastDigit = num%10;
        //判断 边界，当 x 为负数时不是回文数。同样的，如果数字的最后一位是0，为了使该数字为回文，则其第一位数字也应该是0，只有0满足这一属性
        if(num<0 || (num !=0 && lastDigit==0)){
            return false;
        }
        // 将数值 按 前后两部分 来看待，对后半部分的值进行反转，当前半部分的值于后半部分的值相等时则判定为 是回文数
        int reverseSecondPart = 0;
        while(num > reverseSecondPart){
            lastDigit = num%10;
            num/=10;
            reverseSecondPart = reverseSecondPart*10+ lastDigit;
        }
        // 当数字长度为奇数时，我们可以通过reverseSecondPart/10 去除处于中位的数字。
        // 例如，当输入为12321 时，在while循环的末尾，我们可以得到 num=12,reverseSecondPart = 123,
        // 由于 处于中位的数字 不影响回文（它总是于自己相等），所以我们可以简单的将其去除。
        return num == reverseSecondPart || num == reverseSecondPart/10;
    }
}
