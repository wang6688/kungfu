package com.wwa.study.algorithm.leetcode;

import java.util.concurrent.ThreadLocalRandom;

/**
  二分查找
 * @date Created in 2021/7/12
 *
 */
public class BinarySearch {

    /**
     *

     */
    public static void main(String[] args) {
       int[] nums = {-1,0,3,5,9,12};
               int target = 9;
        int search = search(nums, target);
        System.out.println(search);

        int lastBadVersion = firstBadVersion(988);
        System.out.println(lastBadVersion);
    }

    /**二分查找
     *  给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target  ，写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1。
     *      二分查找是一种基于比较目标值和数组中间元素的教科书式算法。
     *      如果目标值等于中间元素，则找到目标值。
     *      如果目标值较小，继续在左侧搜索。
     *      如果目标值较大，则继续在右侧搜索。
     *      * 链接：https://leetcode-cn.com/problems/binary-search
     * @param nums
     * @param target
     * @return
     */
    public static int search(int[] nums, int target) {
        // 定位到中间下标，从中间开始查找，若target < 中间下标对应的 值  则以中间下标为终点，向左侧查找继续寻找,再次确定中间下标。 若target > 中间下标对应的值，那么以中间下标作为起点，向右侧继续寻找。
        int left = 0;
        int right = nums.length-1;
        int middle = (left+right)/2;
        while(left <= right){
            if(target < nums[middle]){
//                因为 mid 已经搜索过，应该从搜索区间中去除。
                right = middle-1;
            }else if(target > nums[middle]){
                //  因为 mid 已经搜索过，应该从搜索区间中去除。
                left = middle+1;
            }else {
                return  middle;
            }
            // 折半查找的关键
//            middle = (left+right)/2;  这种写法是着眼于 整个数组的全局中来(如left=4,right=8,middle=(4+8)/2=6) 定位到 前半部分（较小的数）或后半部分（较大的数中），但当数组中元素过多时可能会导致 (left+right)超出整数范围
            middle = left+(right-left)/2; // 由于上一行代码的写法 可能会导致 left+right超出整数范围，故采用这种方法（先以左侧下标作为中间轴，然后只计算出右半部分的中间值，这样便使middle 指向了整个数组中4分之3的位置）
        }
        return -1;
    }


    /**
     * 第一个错误的版本
     * 你是产品经理，目前正在带领一个团队开发新的产品。不幸的是，你的产品的最新版本没有通过质量检测。由于每个版本都是基于之前的版本开发的，所以错误的版本之后的所有版本都是错的。
     * 假设你有 n 个版本 [1, 2, ..., n]，你想找出导致之后所有版本出错的第一个错误的版本。
     * 你可以通过调用 bool isBadVersion(version) 接口来判断版本号 version 是否在单元测试中出错。实现一个函数来查找第一个错误的版本。你应该尽量减少对调用 API 的次数。

     * 因为题目要求尽量减少调用检查接口的次数，所以不能对每个版本都调用检查接口，而是应该将调用检查接口的次数降到最低。
     * 注意到一个性质：当一个版本为正确版本，则该版本之前的所有版本均为正确版本；当一个版本为错误版本，则该版本之后的所有版本均为错误版本。我们可以利用这个性质进行二分查找。
     * 具体地，将左右边界分别初始化为 11 和 nn，其中 nn 是给定的版本数量。设定左右边界之后，每次我们都依据左右边界找到其中间的版本，检查其是否为正确版本。如果该版本为正确版本，那么第一个错误的版本必然位于该版本的右侧，我们缩紧左边界；否则第一个错误的版本必然位于该版本及该版本的左侧，我们缩紧右边界。
     * 这样我们每判断一次都可以缩紧一次边界，而每次缩紧时两边界距离将变为原来的一半，因此我们至多只需要缩紧 O(\log n)O(logn) 次。
     * 链接：https://leetcode-cn.com/problems/first-bad-version/solution/di-yi-ge-cuo-wu-de-ban-ben-by-leetcode-s-pf8h/
     * @param n
     * @return
     */
    public static int firstBadVersion(int n){
        int low = 1,mid = 0, high = n;
        while(low < high){  // 循环直至区间左右端点相同
            mid = low+(high-low)/2;  // 防止计算时溢出
            if(isBadVersion(mid)){
                high =mid ;  // 答案在区间 [left, mid] 中
            }else{
                low = mid +1;  // 答案在区间 [mid+1, right] 中
            }
        }
        // 此时有 left == right，区间缩为一个点，即为答案
        return low;
    }

    /**
     * 搜索插入位置
     * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
     * 你可以假设数组中无重复元素。
     * 示例 1:
     * 输入: [1,3,5,6], 5
     * 输出: 2
     * 示例 2:
     * 输入: [1,3,5,6], 2
     * 输出: 1
     * @param nums
     * @param target
     * @return
     */
    public int  searchInsert(int[] nums,int target){
        int left = 0,right = nums.length-1;
        int middle = 0;
        while(left <= right){
            middle = left + (right-left)/2;
            if(target == nums[middle]){
                return middle;
            }else if(target < nums[middle]){
                right = middle -1 ;
            }else{
                left = middle+1;
            }
        }
        return left;
    }

    /**
     * @param version 版本号
     * @return 返回 当前版本号是否错误版本
     */
    private static boolean isBadVersion(int version){
        return ThreadLocalRandom.current().nextBoolean();
    }
}
