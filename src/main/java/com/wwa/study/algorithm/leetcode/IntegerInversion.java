package com.wwa.study.algorithm.leetcode;

import com.sun.javafx.application.PlatformImpl;

import java.util.Stack;

/**
 整数反转
 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
 如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。
 假设环境不允许存储 64 位整数（有符号或无符号）。

 链接：https://leetcode-cn.com/problems/reverse-integer
  * @date Created in 2021/7/12
 * @modified By：
 */
public class IntegerInversion {
    public static void main(String[] args) {
        int reverse = reverse(-1534236);
        System.out.println(reverse);


    }

    public static int reverse(int intNum){
        // 存放反转后的结果
        int invertedResult = 0;
        while(intNum!=0){
            //因为x本身被int 限制，当x 为正数并且 位数和Integer.MAX_VALUE（2147483647）的位数相等时，首位最大只能为2，
            // 所以逆转后不会出现 invertedResult = Integer.MAX_VALUE/10 && lastDigit>2的情况，
            // 自然也不需要判断 invertedResult = 214748364 && lastDigit>7了。
            // 反之负数情况也一样，当x为负数并且位数和 Integer.MIN_VALUE(-2147483648)的位数相等时，首位最大也能为2，所以逆转后不会出现 invertedResult = Integer.MIN_VALUE/10 && lastDigit >2 的情况。自然也不需要判断 invertedResult =-214748364 && lastDigit >8的情况

            if(invertedResult < Integer.MIN_VALUE /10 || invertedResult > Integer.MAX_VALUE/10){
                return 0;
            }
            // 取出最后一位（带符号[-或+]的）数字
            int lastDigit = intNum %10;
            // 将最后一位数字从 原数值中移除
            intNum/=10;
            // 将 反转后的结果（带符号[-或+]） *10 以便达到进位效果，然后+ 原数值末尾的（带符号[-或+]）数字，达到反转的效果
            invertedResult = invertedResult *10 +lastDigit;

        }
        return invertedResult;
    }
}
