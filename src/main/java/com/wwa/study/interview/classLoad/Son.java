package com.wwa.study.interview.classLoad;

public class Son extends Father {

    private static int j = method();

    static {
        System.out.print("6\t");
    }
    Son(){
        System.out.print("7\t");
    }
    {
        System.out.print("8\t");
    }
    private int i = test();
    @Override
    public int test(){
        System.out.print("9\t");
        return 1;
    }

    public static int method(){
        System.out.print("10\t");
        return  1;
    }

//    public static void main(String[] args) {
//       Son s1 = new Son();
//        System.out.println();
//        Son s2 = new Son();
//    }
}
