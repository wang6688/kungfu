package com.wwa.study.interview.classLoad;

public class Father {
    private int i = test();
    static {
        System.out.print("1\t");
    }



    Father(){
        System.out.print("2\t");

    }
    {
        System.out.print("3\t");

    }
    private static int j = method();
    public int test(){
        System.out.print("4\t");
        return 1;
    }

    public static int method(){
        System.out.print("5\t");
        return 1;
    }
}
