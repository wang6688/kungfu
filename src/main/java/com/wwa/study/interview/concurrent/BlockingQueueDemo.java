package com.wwa.study.interview.concurrent;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

//ArrayBlockingQueue:是一个基于数组结构的有界阻塞队列，此队列按FIFO(先进先出)原则对元素进行排序。
//LinkedBlockingQueue 是一个基于链表结构的阻塞队列容量为Integer最大值，此队列按FIFO(先进先出)排序元素，吞吐量通常要高于ArrayBlockingQueue
public class BlockingQueueDemo {
    public static void main(String[] args) {

        BlockingQueue<String> strQueue =  new ArrayBlockingQueue<>(3);
        strQueue = new LinkedBlockingDeque<>();
    /*    System.out.println(strQueue.add("a"));;
        System.out.println(strQueue.add("b"));
        System.out.println(strQueue.add("c"));

       // System.out.println(strQueue.add("111"));  IllegalStateException
        System.out.println(strQueue.element());  //a

        System.out.println(strQueue.remove());
        System.out.println(strQueue.remove());
        System.out.println(strQueue.remove());
      //  System.out.println(strQueue.remove());  //NoSuchElementException*/

      /*  System.out.println(strQueue.offer("aaa"));
        System.out.println(strQueue.offer("bbb"));
        System.out.println(strQueue.offer("ccc"));
        System.out.println(strQueue.offer("111"));

        System.out.println(strQueue.peek()); //aaa
        System.out.println(strQueue.poll());
        System.out.println(strQueue.poll());
        System.out.println(strQueue.poll());
        System.out.println(strQueue.poll()); //null*/


        try {
            System.out.println(strQueue.offer("aaa",2, TimeUnit.SECONDS));
            System.out.println(strQueue.offer("bbb",2, TimeUnit.SECONDS));
            System.out.println(strQueue.offer("ccc",2, TimeUnit.SECONDS));
            System.out.println(strQueue.offer("111",2, TimeUnit.SECONDS));  //等待两秒再插入，false

            System.out.println(strQueue.poll(2, TimeUnit.SECONDS));
            System.out.println(strQueue.poll(2, TimeUnit.SECONDS));
            System.out.println(strQueue.poll(2, TimeUnit.SECONDS));
            System.out.println(strQueue.poll(2, TimeUnit.SECONDS));     //等待两秒再取出 null
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


       /* try {
            strQueue.put("aaaa");
            strQueue.put("bbbb");
            strQueue.put("cccc");
            System.out.println("===========================");
         //  strQueue.put("dddd");   //阻塞


            System.out.println(strQueue.take());
            System.out.println(strQueue.take());
            System.out.println(strQueue.take());
          //  System.out.println(strQueue.take());//阻塞
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/



    }
}
