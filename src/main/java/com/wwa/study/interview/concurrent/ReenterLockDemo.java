package com.wwa.study.interview.concurrent;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class  Phone implements Runnable{
    //case 1.synchronized 是典型的可重入锁线程在外层获取锁的时候，在内层方法 也会自动获取到锁
    public synchronized void sendSMS(){
        System.out.println("线程"+Thread.currentThread().getName()+"发送短信");
        sendEmail();
    }

    public synchronized void sendEmail() {
        System.out.println("线程"+Thread.currentThread().getName()+"发送邮件");
    }




    //----------------------------------------------
    //case 2 ReenterLock类 也是典型的可重入锁
    Lock lock = new ReentrantLock();
    @Override
    public void run() {
        appendLock();
    }

    public void appendLock(){
        //lock 与Unlock方法 要成对出现，
        // lock方法调用次数比unlock多会出现卡死死锁现象
        //unlock方法调用次数比lock多会出现 IllegalMonitorStateException 异常
        lock.lock();
        lock.lock();
        lock.lock();
        System.out.println("线程"+Thread.currentThread().getName()+"加锁了");
        clearLock();
        lock.unlock();
        lock.unlock();
        lock.unlock();
    }

    public void clearLock(){
        lock.lock();
        try{
            System.out.println("线程"+Thread.currentThread().getName()+"去除锁了");
        }finally {



            lock.unlock();
        }

    }
}

public class ReenterLockDemo {

    public static void main(String[] args) {
        Phone phone = new Phone();
        new Thread(()->{

           phone.sendSMS();
        },"Thread 1 ").start();
        new Thread(()->{

            phone.sendSMS();
        },"Thread 2 ").start();


        try {
            Thread.sleep(1000);
            System.out.println("=----------------------------------------");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Thread thread3 = new Thread(phone,"t3");
        Thread thread4 = new Thread(phone,"t4");
        thread3.start();
        thread4.start();

    }
}
