package com.wwa.study.interview.concurrent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class  MyCache{
    public volatile Map map = new HashMap<String,Object>();
  //  public ReadWriteLock readOrWriteLock = new ReentrantReadWriteLock();
    public Lock lock = new ReentrantLock();
    public void getVal(String key) {
       // readOrWriteLock.readLock().lock();
        System.out.println(Thread.currentThread().getName()+"开始获取值");
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"获取值完成"+map.get(key));
      //  readOrWriteLock.readLock().unlock();
    }

    public void putVal(String key,Object value){
     //   readOrWriteLock.writeLock().lock();
        lock.lock();
        System.out.println(Thread.currentThread().getName()+"开始放入值");
        map.put(key,value);
        System.out.println(Thread.currentThread().getName()+"放入值完成");
        lock.unlock();
      //  readOrWriteLock.writeLock().unlock();

    }
}


public class ReadWriteLockDemo {


    public static void main(String[] args) {
     MyCache cache = new MyCache();
        for (int i = 1;i<=10;i++){
            final int val = i;
            new Thread(()->{
                cache.putVal("t"+val,val);
            },String.valueOf(i)).start();
        }

        System.out.println("");
        for (int i = 1;i<=10;i++){
            final int val = i;
            new Thread(()->{
                cache.getVal("t"+val);
            },String.valueOf(i)).start();
        }
    }
}
