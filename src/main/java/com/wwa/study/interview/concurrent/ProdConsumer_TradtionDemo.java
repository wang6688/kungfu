package com.wwa.study.interview.concurrent;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class AirCondition{
        //资源来空调， 小红想要 减1度，小安想要 加1度
    private volatile int tempture = 0;
   private  Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();
    public void increament(){
        lock.lock();
        try {
        while (0!=tempture){
                condition.await();
        }
            tempture++;
            System.out.println(Thread.currentThread().getName()+"执行了空调+++操作"+tempture);
            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }

    }
    public void decreament(){
        lock.lock();
        try {
            while(1!=tempture){
                condition.await();
            }
            tempture--;
            System.out.println(Thread.currentThread().getName()+"执行了空调---操作"+tempture);
            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }

    }
}

public class ProdConsumer_TradtionDemo {

    public static void main(String[] args) throws InterruptedException {
        AirCondition airCondition = new AirCondition();
        new Thread(()->{
            for (int i =1;i<=5;i++)
            airCondition.decreament();
        },"小红").start();
        Thread.sleep(3000);
        new Thread(()->{
            for (int i =1;i<=5;i++)
            airCondition.increament();
        },"小安").start();

        new Thread(()->{
            for (int i =1;i<=5;i++)
                airCondition.increament();
        },"小么").start();

        new Thread(()->{
            for (int i =1;i<=5;i++)
                airCondition.decreament();
        },"小为").start();
    }
}
