package com.wwa.study.interview.concurrent;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReference;

public class AtomicDemo {
    public static void main(String[] args) {
//        atomicIntegerTest();
//        atomicIntegerArrayTest();
//        atomicReferenceTest();
        atomicIntegerFieldUpdaterTest();
    }

    public static void atomicIntegerTest() {
        int temvalue = 0;
        //使用AtomicInteger之后，不需要加锁，也可以实现线程安全。
        AtomicInteger i = new AtomicInteger(0);
        temvalue = i.getAndSet(3);
        System.out.println("temvalue:" + temvalue + ";  i:" + i);//temvalue:0;  i:3
        temvalue = i.getAndIncrement();
        System.out.println("temvalue:" + temvalue + ";  i:" + i);//temvalue:3;  i:4
        temvalue = i.getAndAdd(5);
        System.out.println("temvalue:" + temvalue + ";  i:" + i);//temvalue:4;  i:9
    }

    public static void atomicIntegerArrayTest() {
        int temvalue = 0;
        int[] nums = { 1, 2, 3, 4, 5, 6 };
        AtomicIntegerArray i = new AtomicIntegerArray(nums);
        for (int j = 0; j < nums.length; j++) {
            System.out.println(i.get(j));
        }
        temvalue = i.getAndSet(0, 2);
        System.out.println("temvalue:" + temvalue + ";  i:" + i);  //temvalue:1; i:2
        temvalue = i.getAndIncrement(0);
        System.out.println("temvalue:" + temvalue + ";  i:" + i); //temvalue:2; i:3
        temvalue = i.getAndAdd(0, 5);
        System.out.println("temvalue:" + temvalue + ";  i:" + i); //temvalue:3; i:8

        temvalue = i.getAndDecrement(3);
        System.out.println("temvalue:" + temvalue + ";  i:" + i); //temvalue:4; i:3
    }

    public static void atomicReferenceTest() {
        AtomicReference<Person> ar = new AtomicReference<Person>();
        Person person = new Person("SnailClimb", 22);
        ar.set(person);
        Person updatePerson = new Person("Daisy", 20);
        ar.compareAndSet(person, updatePerson);

        System.out.println(ar.get().getName());
        System.out.println(ar.get().getAge());
    }

    public static void atomicIntegerFieldUpdaterTest() {
        AtomicIntegerFieldUpdater<User> a = AtomicIntegerFieldUpdater.newUpdater(User.class, "age");

        User user = new User("Java", 22);
        System.out.println(a.getAndIncrement(user));// 22
        System.out.println(a.get(user));// 23
    }

}
@Data
@AllArgsConstructor
class Person{
    private String name;
    private int age;
}
@Data
@AllArgsConstructor
class User {
    private String name;
    public volatile int age;
}