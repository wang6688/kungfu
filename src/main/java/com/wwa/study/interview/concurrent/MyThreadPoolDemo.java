package com.wwa.study.interview.concurrent;

import java.util.concurrent.*;

public class MyThreadPoolDemo {

    public static void main(String[] args) {
        ExecutorService pool = null;
     //    pool = Executors.newScheduledThreadPool(4);
    //     pool = Executors.newWorkStealingPool(4);


       //  pool = Executors.newFixedThreadPool(6);//一池6个处理线程 ，执行长期的任务性能好很多。
   //     pool = Executors.newSingleThreadExecutor();//一池1个处理线程 一个任务一个任务执行的场景。
     //  pool = Executors.newCachedThreadPool();//一池N个处理线程，适用：执行很多短期异步的小程序或者负载较轻的服务器。

        pool = new ThreadPoolExecutor(2,
                5,  //最大线程池大小，配置指南，如 1、业务为cpu密集型则设为cpu核心数+1，2、业务为IO密集型的任务则配置为cpu核心数*2，可以参考公式 [CPU核心数/(1-阻塞系数(0.8~0.9之间))
                                    //IO密集型，即该任务需要大量的IO，即大量的阻塞。在单线程上运行IO密集型的任务会导致浪费大量的CPU运算能力浪费在等待。
                                    //所以在IO密集型的任务中使用多线程可以大大的加速程序运行，即使在单核CPU上，这种加速主要就是利用了被浪费掉的阻塞时间。
                1L,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
               Executors.defaultThreadFactory(),
             //  new  ThreadPoolExecutor.AbortPolicy()  //AbortPolicy 抛异常
               // new  ThreadPoolExecutor.CallerRunsPolicy()  //返回给调用者main线程去处理
               // new  ThreadPoolExecutor.DiscardOldestPolicy()  //丢弃队列中等待时间最久的任务
              new  ThreadPoolExecutor.DiscardPolicy()   //丢弃 新来的任务，拒绝提供服务
        );
        try {

            //模拟10个用户来银行办理业务，而该行只有 6个办理窗口，每个用户就是1个来自外部的请求线程
            for (int i =1;i<=15;i++){
                pool.execute(()-> {

                    System.out.println(Thread.currentThread().getName() + "为用户办理业务");
                    try {
                        Thread.sleep(30000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        finally {
            pool.shutdown();
           // pool.shutdownNow();
        }
    }
}
