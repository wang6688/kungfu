package com.wwa.study.interview.concurrent;
//自旋锁

import java.util.concurrent.atomic.AtomicInteger;

public class SpinLockDemo {
    public AtomicInteger number = new AtomicInteger();

    public void editVal(){
        System.out.println(Thread.currentThread().getName()+"进入 修改值的方法");
        while (!number.compareAndSet(0,66)){
       //     System.out.println(Thread.currentThread().getName()+"正在努力尝试修改number 的值为"+66);
        }
        System.out.println(Thread.currentThread().getName()+"成功修改number 的值为"+66);
    }

    public void clearVal(){
        System.out.println(Thread.currentThread().getName()+"进入 清空值的方法");
        number.compareAndSet(66,0);
        System.out.println(Thread.currentThread().getName()+"清空值完成");
    }


    public static void main(String[] args) throws InterruptedException {
        SpinLockDemo spinLock = new SpinLockDemo();
        new Thread(()->{
            spinLock.editVal();
            try {
                Thread.sleep(3000);

            spinLock.clearVal();                ;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        },"t1").start();
        System.out.println("===============线程分割");
        Thread.sleep(1000);
        new Thread(()->{
            spinLock.editVal();
            try {
                Thread.sleep(3000);

                spinLock.clearVal();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        },"T2").start();
    }
}
