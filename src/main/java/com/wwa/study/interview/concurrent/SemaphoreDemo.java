package com.wwa.study.interview.concurrent;

import java.util.concurrent.Semaphore;

public class SemaphoreDemo {

    public static void main(String[] args) {
        System.out.println("停车场场景。。。。");

        Semaphore semaphore = new Semaphore(3);

        for (int i =1;i<=3;i++){
            new Thread(()->{

                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+"车进入停车位");

                    Thread.sleep(3);
                    System.out.println(Thread.currentThread().getName()+"车离开停车位");

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {

                    semaphore.release();

                }

            },String.valueOf(i)).start(); ;
        }
    }
}
