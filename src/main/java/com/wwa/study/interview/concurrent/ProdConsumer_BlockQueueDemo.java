package com.wwa.study.interview.concurrent;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

class MyResource{
    private AtomicInteger number = new AtomicInteger();
    private  volatile boolean FLAG = true;
    private BlockingQueue<String> queue = null;
    public MyResource(BlockingQueue <String>queue) {
        System.out.println(queue.getClass().getName());
        this.queue = queue;
    }

    public void myProducer(){
        String result = null;
        boolean success ;
        while (FLAG){
            try {
                success = queue.offer("1",2, TimeUnit.SECONDS);

                if(success){
                    System.out.println(Thread.currentThread().getName()+"生产成功！");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("大老板叫停了。 ");

    }

    public void myConsumer(){
        String result = null;

        while (FLAG){
            try {
                result = queue.poll(2,TimeUnit.SECONDS);

                if(null==result||"".equalsIgnoreCase(result)){
                    System.out.println(Thread.currentThread().getName()+"队列里没有吃的了，消费失败！");
                    System.out.println("--------------------------");
                    FLAG = false;
                    return;
                }
                System.out.println(Thread.currentThread().getName()+"消费队列成功！");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void stop() {
        FLAG = false;
    }
}

public class ProdConsumer_BlockQueueDemo {

    public static void main(String[] args) {
        MyResource resource = new MyResource(new ArrayBlockingQueue<String>(10));

        new Thread(()->{
            System.out.println("生产线程启动---------");

            resource.myProducer();
        },"Producer").start();
        new Thread(()->{
            System.out.println("消费线程启动---------");

            resource.myConsumer();

        },"Consumer").start();

        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("生产3秒后，大老板叫停！");
        resource.stop();
    }
}
