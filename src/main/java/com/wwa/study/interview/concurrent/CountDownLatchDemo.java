package com.wwa.study.interview.concurrent;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {

    public static void main(String[] args) {
        System.out.println("放学了。。。。");
        CountDownLatch students = new CountDownLatch(10);

        for (int i =0;i<10;i++){
            new Thread(()->{
                System.out.println("学生"+Thread.currentThread().getName()+"离开教室");
                students.countDown();
            },String.valueOf(i)).start();
        }
        try {
            students.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("班长最后锁门");
    }
}
