package com.wwa.study.interview.concurrent;


public class DieLock {

    public static void main(String[] args) {
        String lock1 = "lock1";
        String lock2 = "lock2";

        new Thread(()->{
            synchronized (lock1){
                System.out.println(Thread.currentThread().getName()+"持有锁"+lock1+"其还想继续获得锁"+lock2);

                try {
                    Thread.sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock2){
                    System.out.println(Thread.currentThread().getName()+"持有锁"+lock2+"其还想继续获得锁"+lock1);
                }
            }
        },"T1").start();

        new Thread(()->{
            synchronized (lock2){
                System.out.println(Thread.currentThread().getName()+"持有锁"+lock2+"其还想继续获得锁"+lock1);

                try {
                    Thread.sleep(2000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock1){
                    System.out.println(Thread.currentThread().getName()+"持有锁"+lock1+"其还想继续获得锁"+lock2);
                }
            }
        },"T1").start();

    }
}
