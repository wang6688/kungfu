package com.wwa.study.interview.concurrent;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class TestList {
    public static void main(String[] args) {
      // List list = new ArrayList<String>();  //ConcurrentModificationException
       //  list = new Vector();  //1
       //  list = Collections.synchronizedList(new ArrayList<>());  //2
        Collections.synchronizedMap(new HashMap<>());


        List list = new  CopyOnWriteArrayList();
        for (int i = 0; i<30;i++){
           new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
               System.err.println(list);
            },"th"+i).start();

        }

    }
}
