package com.wwa.study.interview.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

class  MyThread implements Callable{

    @Override
    public Object call() throws Exception {
        System.out.println(Thread.currentThread().getName()+"调用call方法");
       return 1024;
    }
}

public class CallableDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<Object> myThreadFutureTask = new FutureTask<Object>(new MyThread());

        System.out.println(Runtime.getRuntime().availableProcessors());

        for (int i =1;i<=10;i++){
            Thread thread = new Thread(myThreadFutureTask,"T"+i);
            thread.start();
            System.out.println(myThreadFutureTask.get());
        }

    }
}
