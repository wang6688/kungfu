package com.wwa.study.interview.concurrent;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {

    public static void main(String[] args)  {
        CyclicBarrier countWait = new CyclicBarrier(4,()->{
            System.out.println("人齐了， 打麻将了。。。");
        });

        for (int i =1;i<=4;i++){
            new Thread(()->{
                System.out.println("我们在等人。。。");
                try {
                    countWait.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }

            },String.valueOf(i)).start();
        }
    }
}
