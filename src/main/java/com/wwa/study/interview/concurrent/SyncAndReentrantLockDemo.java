package com.wwa.study.interview.concurrent;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/***
 * synchronized 和Lock 的区别，用新的Lock有什么好处?你举例说说
 *
 * 1。原始构成， synchronzied 为系关键字，而Lock为调用concurrent包内的API
 *      synchronized 是关键字数与JVM层面。 monitorenter（底层是通过monitor对象来完成，其实wait/notify等方法也依赖于monitor对象只有在同步块或方法中才能调用wait/notify等方法。
 *                                         monitorexit
 *      Lock 是具体类(java.util.concourrent.Locks.Lock)是Api层面的锁
 * 2、使用方法不同， synchronized无需 手动释放锁，而Lock需要手动加锁和释放锁，并且必须成对出现。
 *      synchronzied 不需要用户手动去释放锁，当synchronzied代码执行完后系统会自动让线程释放对锁的占用。
 *      ReentrantLock则需要用户去手动释放锁 若没有主动释放锁，就有可能导致出现死锁现象。
 *      需要Lock()和unlock()方法配合try/finally语句块来完成。
 * 3、加锁是否公平，synchronized是非公平锁，而Lock默认是非公平锁但可通过参数制定是否为公平锁。
 *
 * 4、等待是否可中断 。synchronized 不可中断，执行过程中要么出现异常退出，要么正常结束。而lock可通过调用参数 来进行中断。
 *      synchronized 不可中断，除非抛出异常或者正常运行完成。
 *      ReentraLock可中断，1、设置超市方法tryLock(long timeout,TimeUnint unit)
 *                          2、lockInterruptibly()放代码快中，调用interrupt（）方法可中断。
 * 5、精确唤醒、锁绑定多个条件。synchronized 不支持，其只能随机唤醒一个线程或唤醒所有线程。而lock支持唤醒指定线程
 *   synchronized
 */
class  SharedResource {
    private int number = 1;
    private Lock lock = new ReentrantLock();
    BlockingQueue<Condition> queue = new ArrayBlockingQueue<>(3);
    BlockingQueue<Integer> intQueue = new ArrayBlockingQueue<>(3);
     public SharedResource(int number) {

            for(int i =1;i<=number;i++){
                try {
                    queue.put(lock.newCondition());
                    intQueue.put(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }



    }

//    private Condition condition1 = lock.newCondition();
//    private Condition condition2 = lock.newCondition();
//    private Condition condition3 = lock.newCondition();

    public void print(int count) {

        lock.lock();
        try {
            Integer intpeek = intQueue.peek();

            System.out.println(Thread.currentThread().getName()+":"+intpeek+"~~"+number);

            Condition condition = queue.take();
            //判断
        while(intpeek!=number) {
            condition.await();
        }
        intQueue.put(intpeek);
        queue.put(condition);
        //干活
        for (int i=1;i<=count;i++){
            System.out.println("线程"+Thread.currentThread().getName()+"打印数字"+i);
        }
        //通知

            if(intQueue.size()>0){
                number = intQueue.peek();
               // intQueue.put(intQueue.take());
                System.out.println("通知下一个线程"+number);
                queue.peek().signal();
            }else{
                lock.lockInterruptibly();
            }




        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
}

public class SyncAndReentrantLockDemo {
    public static void main(String[] args) {
        SharedResource resource = new SharedResource(3);
        new Thread(()->{
            resource.print(5);
        },"A").start();

        new Thread(()->{
            resource.print(10);
        },"B").start();

        new Thread(()->{
            resource.print(15);
        },"C").start();
    }


}
