package com.wwa.study.interview.concurrent;

import java.util.concurrent.SynchronousQueue;

//SynchronmousQueue: 一个不存储元素的阻塞队列，每个插入操作必须等到另一个线程调用移除操作，否则插入一直处于阻塞状态，吞吐量通常要高于
public class SynchronousQueueDemo {

    public static void main(String[] args) {
        SynchronousQueue<String> queue = new SynchronousQueue();


        new Thread(()->{
            try {
                for (int i= 1;i<=3;i++){

                    queue.put(""+i);
                    System.out.println(Thread.currentThread().getName()+"放入"+i);
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"AAA").start();

        new Thread(()->{
            try {
                for (int i= 1;i<=3;i++){
                    Thread.sleep(3000);

                    System.out.println("3秒后"+Thread.currentThread().getName()+"取出"+queue.take());

                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },String.valueOf("BBB")).start();
    }
}
