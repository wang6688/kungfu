package com.wwa.study.interview.io.netty.solve_sticky_pack;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;
//自定义的 解码器 继承 ByteToMessageDecoder 会在自定义Handler类之前执行
//在解码器 进行数据解码时，需要判断 缓存区(ByteBuf)的数据是否足够 ，否则接收到的结果会期望结果可能不一致(** 继承ReplayingDecoder<S> 则不需要进行判断 这个类自动检测 数据是否符合 泛型S **):/
public class MyByteToLongDecoder extends ByteToMessageDecoder {

    @Override
      /**
        * @Description:
        * @Param ctx 上下文对象
        * @Param in 入站的BuyteBuf
          @Param out List<集合> ，将解码后的数据传给下一个handler，下一个handler 接收的数据就为List中的数据，向下一个Handler执行的代码，如果List数据为多个，则传递多次。
        * @return:
        * @Author: 创造奇迹
        * @Date: 2021/7/10
        */
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        // 如果读取的数据等于 16，则会分两次 执行下一个Handler
        System.out.println("MyByteToLongDecoder 被调用");
        //因为 long 8个字节, 需要判断有8个字节，才能读取一个long
        if(in.readableBytes() >= 8) {
            out.add(in.readLong());
        }
    }
}
