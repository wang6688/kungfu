package com.wwa.study.interview.io.netty.dis_to_reconnect;

import com.wwa.study.interview.io.netty.heartbeat.CustomHeartbeatHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
    * @Description:  实现客户端的 断线重连
    * @Param:
    * @return:
    * @Author: 创造奇迹
    * @Date: 2021/7/10
    */
public class MyClientForAfterDisToReconnect {
       private NioEventLoopGroup workGroup = new NioEventLoopGroup(4);
       private Channel channel;
       private Bootstrap bootstrap;

       public static void main(String[] args) throws Exception {
           MyClientForAfterDisToReconnect client = new MyClientForAfterDisToReconnect();
           client.start();
           client.sendData();
       }

       public void sendData() throws Exception {
           Random random = new Random(System.currentTimeMillis());
           for (int i = 0; i < 10000; i++) {
               if (channel != null && channel.isActive()) {
                   String content = "client msg " + i;
                   ByteBuf buf = channel.alloc().buffer(5 + content.getBytes().length);
                   buf.writeInt(5 + content.getBytes().length);
                   buf.writeByte(CustomHeartbeatHandler.CUSTOM_MSG);
                   buf.writeBytes(content.getBytes());
                   channel.writeAndFlush(buf);
               }

               Thread.sleep(random.nextInt(20000));
           }
       }

       public void start() {
           try {
               bootstrap = new Bootstrap();
               bootstrap
                       .group(workGroup)
                       .channel(NioSocketChannel.class)
                       .handler(new ChannelInitializer<SocketChannel>() {
                           protected void initChannel(SocketChannel socketChannel) throws Exception {
                               ChannelPipeline p = socketChannel.pipeline();
                               p.addLast(new IdleStateHandler(0, 0, 5));
                               p.addLast(new LengthFieldBasedFrameDecoder(1024, 0, 4, -4, 0));
                               p.addLast(new MyClientHandlerForDisToReconnect(MyClientForAfterDisToReconnect.this));
                           }
                       });
               doConnect();

           } catch (Exception e) {
               throw new RuntimeException(e);
           }
       }
          /**
            * @Description:  我们抽象出 doConnect 方法, 它负责客户端和服务器的 TCP 连接的建立, 并且当 TCP 连接失败时, doConnect 会 通过 "channel().eventLoop().schedule" 来延时10s 后尝试重新连接.
            * @Param:
            * @return:
            * @Author: 创造奇迹
            * @Date: 2021/7/10
            */
       protected void doConnect() {
           if (channel != null && channel.isActive()) {
               return;
           }

           ChannelFuture future = bootstrap.connect("127.0.0.1", 12345);

           future.addListener(new ChannelFutureListener() {
               public void operationComplete(ChannelFuture futureListener) throws Exception {
                   if (futureListener.isSuccess()) {
                       channel = futureListener.channel();
                       System.out.println("Connect to server successfully!");
                   } else {
                       System.out.println("Failed to connect to server, try connect after 10s");

                       futureListener.channel().eventLoop().schedule(new Runnable() {
                           @Override
                           public void run() {
                               doConnect();
                           }
                       }, 10, TimeUnit.SECONDS);
                   }
               }
           });
       }

   }
