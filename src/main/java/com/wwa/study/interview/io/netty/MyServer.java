package com.wwa.study.interview.io.netty;

import com.wwa.study.interview.io.netty.solve_sticky_pack.MyByteToLongDecoder;
import com.wwa.study.interview.io.netty.solve_sticky_pack.MyLongToByteEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/***
 * 服务端启动类
 * */
public class MyServer {
    public static void main(String[] args) throws Exception {
        //创建两个线程组 boosGroup、workerGroup
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            //创建服务端的启动对象，设置参数
            ServerBootstrap bootstrap = new ServerBootstrap();
            //设置两个线程组boosGroup和workerGroup
            bootstrap.group(bossGroup, workerGroup)
                    //设置服务端通道实现类型
                    .channel(NioServerSocketChannel.class)
                    //设置线程队列得到连接个数
                    .option(ChannelOption.SO_BACKLOG, 128)
                    //设置保持活动连接状态
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    //使用匿名内部类的形式初始addListener化通道对象
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            // 入站的handler 进行解码 My
                            socketChannel.pipeline().addLast(new MyByteToLongDecoder());
                            // 出站的handler 进行编码
                            socketChannel.pipeline().addLast(new MyLongToByteEncoder());
                            //在pipeline加入ProtoBufDecoder
                            //指定对哪种对象进行解码
//                            使用protobuf编译器解析这个文件,命令protoc.exe --java_out=. Student.proto
//                            socketChannel.pipeline().addLast("decoder", new ProtobufDecoder(StudentPOJO.Student.getDefaultInstance()));
                            //给pipeline管道设置处理器
                            socketChannel.pipeline().addLast(new MyServerHandler());
//                            socketChannel.pipeline().addLast(new MyServerHandlerForProto());


                        }
                    });//给workerGroup的EventLoop对应的管道设置处理器
            System.out.println("java技术爱好者的服务端已经准备就绪...");
            //绑定端口号，启动服务端
            ChannelFuture channelFuture = bootstrap.bind(6666).sync();
            //对关闭通道进行监听IdleStateHandler
            channelFuture.channel().closeFuture().sync();
//            addListener(channelFuture);
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
    /**
     * ChannelFuture提供操作完成时一种异步通知的方式。一般在Socket编程中，等待响应结果都是同步阻塞的，而Netty则不会造成阻塞，因为ChannelFuture是采取类似观察者模式的形式进行获取结果
     * */
    private static void addListener(  ChannelFuture channelFuture){

        //添加监听器
        channelFuture.addListener(new ChannelFutureListener() {
            //使用匿名内部类，ChannelFutureListener接口
            //重写operationComplete方法
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                //判断是否操作成功
                if (future.isSuccess()) {
                    System.out.println("连接成功");
                } else {
                    System.out.println("连接失败");
                }
            }
        });
    }


}
