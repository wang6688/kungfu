package com.wwa.study.interview.reference;

import java.util.HashMap;
import java.util.WeakHashMap;

public class WeakHashMapDemo {

    public static void main(String[] args) {

       // myHashmap();
        myWeakHashmap();
    }

    private static void myWeakHashmap() {
        WeakHashMap<Integer,String> map = new WeakHashMap<Integer,String>();
        Integer key = new Integer(2);
        String value = "hashMap";
        map.put(key,value);
        System.out.println(map);
        key = null;
        System.out.println(map);
        System.gc();
        System.out.println(map+"\t"+map.size());

    }

    private static void myHashmap() {

        HashMap<Integer,String> map = new HashMap<>();
        Integer key = new Integer(1);
        String value = "hashMap";
        map.put(key,value);
        System.out.println(map);
        key = null;
        System.out.println(map);
        System.gc();
        System.out.println(map+"\t"+map.size());

    }
}
