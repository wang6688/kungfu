package com.wwa.study.interview.reference;

import java.lang.ref.SoftReference;

/**
 * 指一些还有用但并非必须的对象。系统在发生内存溢出异常前，会把这些对象列进回收范围之中进行二次回收。如果这次回收后还没有足够的内存才会抛出内存异常。
 *
 *
 *
 * 软引用 是一种相对强引用弱化了一些的引用，需要用java.lang.ref.SoftReference类来实现，可以让对象豁免一些垃圾收集。
 *
 * 对于只有软引用的对象来说，
 *
 * 当系统内存充足时它 不会被回收，
 *
 * 当系统内存不足时， 它会被回收。
 *
 *
 *
 * 软引用通常用在对内存敏感的程序中，比如告诉缓存就有用到软引用，内存够用的时候就保留，不够用就回收！
 * 使用 场景：
 * 1、 如果每次读取图片都从硬盘读取则会严重影响性能，
 * 2、如果一次性全部加载到内存中又可能造成内存溢出。
 *
 * 设计思路是： 用一个HashMap来保存图片的路径和相应图片对象关联的软引用之间的映射关系，在内存不足时，
 * JVM会自动回收这些缓存图片对象所占用的空间，从而有效的避免OOM的问题。
 * Map<String,SoftReference<Bitmap>> imageCache = new HashMap<String,SoftReference<Bitmap>>();
 * */
public class SoftReferenceDemo {
/**
 * 内存够用的时候就保留，不够用就回收！
 * */
    public static void softRef_Memory_Enough() {
        Object o1 = new Object();
        SoftReference<Object> softReference = new SoftReference<>(o1);
        System.out.println(o1);
        System.out.println(softReference.get());

        o1 = null;
        System.gc();

        System.out.println(o1);
        System.out.println(softReference.get());

    }

    /**
     * JVM配置 ，故意产生大对象 并配置小的内存，让他内存不够用了导致OOM，看软引用的回收情况。
     * -Xms5m -Xmx5m -XX:+PrintGCDetails
     * */
    public static void softRef_Memory_NotEnough() {
        Object o1 = new Object();
        SoftReference<Object> softReference = new SoftReference<>(o1);
        System.out.println(o1);
        System.out.println(softReference.get());

        o1 = null;
     //   System.gc(); 模拟OOM自动GC

        try{
            byte [] bytes = new byte[30*1024*1024];
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            System.out.println(o1);
            System.out.println(softReference.get());

        }

    }

    public static void main(String[] args) {
       softRef_Memory_Enough();
      //  softRef_Memory_NotEnough();
    }
}
