package com.wwa.study.interview.singleton;

import java.util.concurrent.*;

public class TestSingleton {

    public static void main(String[] args) {

        Callable<Singlegon> callSingle = new Callable<Singlegon>() {
            @Override
            public Singlegon call() throws Exception {
                return Singlegon.getInstance();
            }
        };

    //    FutureTask task1 = new FutureTask(callSingle);

        ExecutorService service = Executors.newFixedThreadPool(2);
        Future<Singlegon> submit1 = service.submit(callSingle);
        Future<Singlegon> submit2 = service.submit(callSingle);
        try {
            Singlegon singlegon1 = submit1.get();
            System.err.println("singlegon1: @"+ singlegon1);
            Singlegon singlegon2 = submit2.get();
            System.err.println("singlegon1: @"+ singlegon2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }finally {
            service.shutdownNow();
        }

    }
}
