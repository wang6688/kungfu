package com.wwa.study.interview.singleton;

public class BestSingleton {
    //在内部类内创建对象，内部类的加载机制是当有人调用时才会进行初始化 所以是线程安全的
    //在内部类被加载和初始化时，才创建INSTANCE实例对象
    //静态内部类不会自动随着外部类的加载和初始化而初始化，它是单独去加载和初始化的
    //因为是在内部类加载和初始化时创建的，因此是线程安全的。
    private BestSingleton(){}

    public static class Inner{
        private static final BestSingleton INSTANCE = new BestSingleton();
    }

    public static BestSingleton getInstance(){
        return Inner.INSTANCE;
    }
}
