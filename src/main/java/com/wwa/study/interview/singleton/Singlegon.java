package com.wwa.study.interview.singleton;

public class Singlegon {
    private volatile static    Singlegon instance = null;
    private   Singlegon(){}

    public static  Singlegon getInstance() {
        long beginTime = System.currentTimeMillis();
        if(null==instance){  //此处的if判断是为了提高获取对象的效率，使当instance 已有对象时不再进入线程抢锁的判断，提高效率
            synchronized (Singlegon.class){  //在此处加synconized关键字是为了给类加锁，使线程处于安全状态
                if(null==instance){   // 此处的if判断是为了 饿汉式的单例模式
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    instance = new Singlegon();
                }
            }
        }
        long endTime = System.currentTimeMillis();
        System.err.println("创建对象耗时:"+(endTime-beginTime));
        return instance;
    }
}
